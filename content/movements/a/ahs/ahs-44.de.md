---
title: "AHS 44"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AHS 44","August Hirsch","Schwenningen","Stiftanker"]
description: "in einfaches Stiftankerwerk ohne Steine  "
abstract: "Ein einfaches Stiftankerwerk ohne Steine"
preview_image: "ahs_44-mini.jpg"
image: "AHS_44.jpg"
movementlistkey: "ahs"
caliberkey: "44"
manufacturers: ["ahs"]
manufacturers_weight: 44
categories: ["movements","movements_a","movements_a_ahs"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum