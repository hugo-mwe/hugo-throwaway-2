---
title: "AHS"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "de"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "ahs"
description: ""
abstract: "(Alfred Hirsch, Schwenningen, Deutschland)"
---
(Alfred Hirsch, Schwenningen, Deutschland)
{{< movementlist "ahs" >}}

{{< movementgallery "ahs" >}}