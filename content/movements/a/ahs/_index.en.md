---
title: "AHS"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "en"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "ahs"
description: ""
abstract: "(Alfred Hirsch, Schwenningen, Germany)"
---
(Alfred Hirsch, Schwenningen, Germany)
{{< movementlist "ahs" >}}

{{< movementgallery "ahs" >}}