---
title: "AHS 53"
date: 2017-09-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AHS 53","AHS","53","Stiftanker","Finger","1980"]
description: "AHS 53 - Ein einfaches Stiftankerwerk der letzten Generation aus Deutschland"
abstract: "Eines der letzten Stiftankerwerke aus deutscher Produktion"
preview_image: "ahs_53-mini.jpg"
image: "AHS_53.jpg"
movementlistkey: "ahs"
caliberkey: "53"
manufacturers: ["ahs"]
manufacturers_weight: 53
categories: ["movements","movements_a","movements_a_ahs"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das hier vorgestellt Exemplar kam in gebrauchtem aber funktionsfähigen Zustand ins Labor und wurde nur minimal geölt und justiert.
donation: "Vielen Dank an [Günter G.](/supporters/guenter-g/) für die Spende der \"Domina\"-Uhr incl. funktionsfähigem Werk."
usagegallery: 
  - image: "a/ahs/AHS_HU_AHS_53.jpg"
    description: "AHS Herrenuhr (11/1980)"
  - image: "d/domina/Domina_DU_AHS_53.jpg"
    description: "Domina Damenuhr"
timegrapher_old: 
  rates:
    ZO: "+110"
    ZU: "+30"
    3O: "+30"
    6O: "+200"
    9O: "+120"
    12O: "-61"
  amplitudes:
    ZO: "328"
    ZU: "245"
    3O: "238"
    6O: "213"
    9O: "213"
    12O: "210"
  beaterrors:
    ZO: "1.5"
    ZU: "0.4"
    3O: "0.5"
    6O: "0.2"
    9O: "0.4"
    12O: "1.4"
---
Lorem Ipsum