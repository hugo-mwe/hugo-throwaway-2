---
title: "Amida 710"
date: 2012-12-31T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Roskopf movement","pillar movement","Amida 710","Amida","710","17 Jewels","Grenchen","swiss","switzerland","handwinding"]
description: "Amida 710 - A pretty modern and rare handwinding movement. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: "A pretty modern and rare swiss handwinding movement"
preview_image: "amida_710-mini.jpg"
image: "Amida_710.jpg"
movementlistkey: "amida"
caliberkey: "710"
manufacturers: ["amida"]
manufacturers_weight: 710
categories: ["movements","movements_a","movements_a_amida_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/amida/Amida_HAU_Amida_710.jpg"
    description: "Amida mens' watch"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Amida_710_ZO.jpg
    ZU: Zeitwaage_Amida_710_ZU.jpg
    3O: Zeitwaage_Amida_710_3O.jpg
    6O: Zeitwaage_Amida_710_6O.jpg
    9O: Zeitwaage_Amida_710_9O.jpg
    12O: Zeitwaage_Amida_710_12O.jpg
  values:
    ZO: "-30"
    ZU: "-10"
    3O: "-38"
    6O: "+6"
    9O: "+4"
    12O: "-15"
---
Lorem Ipsum