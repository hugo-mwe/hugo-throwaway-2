---
title: "AS 1220"
date: 2014-02-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1220","AS","Adolph Schild SA","17 Jewels","manual wind","center second"]
description: "AS 1220 - A 10 1/2 ligne manual wind movement with an odd minute display construction"
abstract: "A swiss manual wind movement with a very special shape of the main bridge and an odd construction of the center minute indication"
preview_image: "as_1220-mini.jpg"
image: "AS_1220.jpg"
movementlistkey: "as"
caliberkey: "1220"
manufacturers: ["as"]
manufacturers_weight: 1220
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Since the specimen shown here was in a very good condition, it was not intensivly cleaned, but just simple cleaned and oiled.
usagegallery: 
  - image: "f/felsus/Felsus_HAU_AS_1220.jpg"
    description: "Felsus mens' watch  (with wrong dial inscription)"
timegrapher_old: 
  description: |
    For a more or less unserviced movement, the timing rates are OK. Since the balance amplitude is very well (visually "measured"), it is likely, that with a better poising of the balance and an adjustment of the beat error, the rates could be much better.
  images:
    ZO: Zeitwaage_AS_1220_ZO.jpg
    ZU: Zeitwaage_AS_1220_ZU.jpg
    3O: Zeitwaage_AS_1220_3O.jpg
    6O: Zeitwaage_AS_1220_6O.jpg
    9O: Zeitwaage_AS_1220_9O.jpg
    12O: Zeitwaage_AS_1220_12O.jpg
  values:
    ZO: "+10"
    ZU: "+7"
    3O: "-13"
    6O: "+3"
    9O: "-25"
    12O: "-50"
---
Lorem Ipsum