---
title: "AS 976"
date: 2013-02-03T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 976","AS","Adolph Schild","976","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Damenuhr","Kaliber","Werk","17 Steine","Schweiz"]
description: "AS 976 - Ein weit verbreitetes und lange gebautes Formwerk"
abstract: "Ein weit verbreitetes, lange gebautes Formwerk für Damenuhren"
preview_image: "as_976-mini.jpg"
image: "AS_976.jpg"
movementlistkey: "as"
caliberkey: "976"
manufacturers: ["as"]
manufacturers_weight: 976
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Harald Hoeber](/supporters/de/) für die Spenden dieses Werks in der Ausführung mit dem Girocap-Lager und in der älteren Ausführung ohne Stoßsicherung!"
usagegallery: 
  - image: "e/exquisit/Exquisit_DAU.jpg"
    description: "Exquisit Damenuhr"
  - image: "b/bwc/BWC_Teenager.jpg"
    description: "BWC &quot;teenager&quot; Damenuhr"
  - image: "o/olma/Olma_DAU_Zifferblatt.jpg"
    description: "Olma Damenuhr"
  - image: "s/sigma/Sigma_DAU_Zifferblatt.jpg"
    description: "Sigma Damenuhr"
---
Lorem Ipsum