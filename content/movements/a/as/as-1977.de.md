---
title: "AS 1977"
date: 2009-06-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 1977","AS","1977","Adolph Schild","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Formwerk","Schweiz","17 Steine"]
description: "AS 1977"
abstract: ""
preview_image: "as_1977-mini.jpg"
image: "AS_1977.jpg"
movementlistkey: "as"
caliberkey: "1977"
manufacturers: ["as"]
manufacturers_weight: 1977
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bwc/BWC_DAU.jpg"
    description: "BWC Damenuhr"
---
Lorem Ipsum