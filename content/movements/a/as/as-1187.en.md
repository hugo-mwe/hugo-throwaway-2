---
title: "AS 1187"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1187","AS","1187","Adolph Schild","15 Jewels","watch","watches","wristwatch","wristwatches","caliber","swiss","switzerland","screw balance"]
description: "AS 1187"
abstract: ""
preview_image: "as_1187-mini.jpg"
image: "AS_1187.jpg"
movementlistkey: "as"
caliberkey: "1187"
manufacturers: ["as"]
manufacturers_weight: 1187
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: AS 1187](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&2&2uswk&AS_1187&)
usagegallery: 
  - image: "m/musette/Musette_Resist_HAU.jpg"
    description: "Musette Resist HAU"
---
Lorem Ipsum