---
title: "AS 1201"
date: 2015-11-14T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS","Schild","Grenchen","Adolph Schild","AS 1201","1200","11 1/2 Linien","kleine Sekunde","dezentrale Sekunde","17 Jewels","17 Rubis","17 Steine","Handaufzug","Schraubenunruh"]
description: "AS 1201 - Ein Handaufzugswerk mit kleiner Sekunde aus den späten 1940er Jahren"
abstract: "Ein Haufzugszugswerk mit dezentraler Sekundenanzeige aus den späten 1940er Jahren, bei dem das Ankerrad unter einem angetäuschten eigenen Kloben gelagert ist."
preview_image: "as_1201-mini.jpg"
image: "AS_1201.jpg"
movementlistkey: "as"
caliberkey: "1201"
manufacturers: ["as"]
manufacturers_weight: 1201
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das Werk kam lose und in gutem Zustand is Labor, mit Gangwerten die recht stark im Plus lagen, zudem war die Amplitude der Unruh recht niedrig. Auf einen Komplettservice wurde verzichtet, es wurde lediglich (mit Ausnahme des Unruhklobens) leicht geölt und justiert, so das Werk auf der Zeitwaage wieder eine relativ ordentliche Leistung bot.
links: |
  * [Ranfft Uhren: AS 1201 (Abbildung AS 1200!)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&AS_1201)
usagegallery: 
  - image: "e/erman/Erman_HAU_Zifferblatt_AS_1201.jpg"
    description: "Erman Herrenuhr  (ohne Gehäuse)"
timegrapher_old: 
  rates:
    ZO: "+90"
    ZU: "+4"
    3O: "+18"
    6O: "+30"
    9O: "+48"
    12O: "+29"
  amplitudes:
    ZO: "220"
    ZU: "317"
    3O: "249"
    6O: "255"
    9O: "235"
    12O: "228"
  beaterrors:
    ZO: "2.1"
    ZU: "1.3"
    3O: "1.6"
    6O: "2.0"
    9O: "2.4"
    12O: "2.0"
---
Lorem Ipsum