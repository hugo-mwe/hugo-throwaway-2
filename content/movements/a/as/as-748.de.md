---
title: "AS 748"
date: 2013-01-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 748","AS","Adolph Schild","Ebauches Trust","Ebauches","Trust","15 jewels","digitalUhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","15","Steine","Jewels","Handaufzug","Damenuhr"]
description: "AS 748 - ein extrem langgezogenes Werk mit digitaler Zeitanzeige, noch unter der Ebauches Trust auf den Markt gebracht"
abstract: "Ein besonders langgezogenes Werk mit digitaler Zeitanzeige, das noch unter dem Ebauches Trust-Dachverband auf den Markt kam"
preview_image: "as_748-mini.jpg"
image: "AS_748.jpg"
movementlistkey: "as"
caliberkey: "748"
manufacturers: ["as"]
manufacturers_weight: 748
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum