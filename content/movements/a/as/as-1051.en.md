---
title: "AS 1051"
date: 2013-11-03T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1051","AS","1051","Adolph Schild","15 Jewels","watch","watches","wristwatch","wristwatches","caliber","form movement","swiss","switzerland"]
description: "AS 1051"
abstract: "An older ladies' watch form movement"
preview_image: "as_1051-mini.jpg"
image: "AS_1051.jpg"
movementlistkey: "as"
caliberkey: "1051"
manufacturers: ["as"]
manufacturers_weight: 1051
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/labor/Labor_DAU.jpg"
    description: "Labor ladies' watch"
  - image: "i/irowa/Irowa_DAU.jpg"
    description: "Irowa ladies' watch"
---
Lorem Ipsum