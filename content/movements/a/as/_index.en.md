---
title: "AS"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "en"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "as"
description: ""
abstract: "(Adolph Schild SA, Grenchen, Switzerland)"
---
(Adolph Schild SA, Grenchen, Switzerland)
{{< movementlist "as" >}}

{{< movementgallery "as" >}}