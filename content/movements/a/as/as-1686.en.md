---
title: "AS 1686 (Standard 1686)"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Standard 1686","Standard","1686","AS 1686","AS","Schild","watch","watches","wristwatch","wristwatches","movement","caliber","17","jewels","17 jewels","17 Jewels","swiss","Switzerland"]
description: "AS 1686 / Standard 1686"
abstract: ""
preview_image: "as_1686-mini.jpg"
image: "AS_1686.jpg"
movementlistkey: "as"
caliberkey: "1686"
manufacturers: ["as"]
manufacturers_weight: 1686
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum