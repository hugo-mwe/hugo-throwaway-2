---
title: "Arogno"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "en"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "arogno"
abstract: "(Fabriques d'Ébauches Réunies d'Arogno SA, Arogno, Switzerland)"
description: ""
---
(Fabriques d'Ébauches Réunies d'Arogno SA, Arogno, Switzerland)
{{< movementlist "arogno" >}}

{{< movementgallery "arogno" >}}