---
title: "Arogno 2"
date: 2013-01-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Arogno 2","Arogno","2","10 Jewels","Arogno","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","10 Steine","Zylinder","Zylinderhemmung","Zylinderwerk"]
description: "Arogno 2 - Ein Zylinderwerk mit Kupplungsaufzug"
abstract: "Ein 10-steiniges Zylinderwerk mit Kupplungsaufzug"
preview_image: "arogno_2-mini.jpg"
image: "Arogno_2.jpg"
movementlistkey: "arogno"
caliberkey: "2"
manufacturers: ["arogno"]
manufacturers_weight: 2
categories: ["movements","movements_a","movements_a_arogno"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum