---
title: "Arogno 151"
date: 2017-01-14T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Arogno 151","Arogno","manual wind","decentral second","small seconds","sub seconds","pallet lever"]
description: "Arogno 151 - a manual wind movement from the late 1940ies"
abstract: "Arogno 151 - a well made manual wind movement from the late 1940ies."
preview_image: "arogno_151-mini.jpg"
image: "Arogno_151.jpg"
movementlistkey: "arogno"
caliberkey: "151"
manufacturers: ["arogno"]
manufacturers_weight: 151
categories: ["movements","movements_a","movements_a_arogno_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here was only slightly gummed and hence only got a fast cleaning and a simple service.
usagegallery: 
  - image: "t/tramont/Tramont_HAU_Arogno_151.jpg"
    description: "Tramont gents watch"
timegrapher_old: 
  rates:
    ZO: "+22"
    ZU: "+23"
    3O: "+15"
    6O: "+115"
    9O: "-138"
    12O: "-115"
  amplitudes:
    ZO: "203"
    ZU: "213"
    3O: "170"
    6O: "174"
    9O: "179"
    12O: "174"
  beaterrors:
    ZO: "6.4"
    ZU: "5.9"
    3O: "7.8"
    6O: "7.2"
    9O: "6.8"
    12O: "7.7"
---
Lorem Ipsum