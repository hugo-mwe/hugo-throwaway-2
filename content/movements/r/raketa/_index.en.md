---
title: "Raketa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "en"
keywords: ["Raketa","Russia","USSR","Petrodvoretz"]
categories: ["movements","movements_r"]
movementlistkey: "raketa"
description: "Movements from the russian factory Raketa, located in Petrodvoretz"
abstract: "Movements from the russian factory Raketa, located in Petrodvoretz"
---
(Pedrodvoretz watch factory, Petrodvoretz, Russia)
{{< movementlist "raketa" >}}

{{< movementgallery "raketa" >}}