---
title: "Raketa 2628.H"
date: 2011-01-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["four leg ring balance","Raketa 2628.H","Raketa","2628.H","17 Jewels","Petrodworez","Russia","USSR","St.Petersburg"]
description: "Raketa 2628.H"
abstract: ""
preview_image: "raketa_2628_h-mini.jpg"
image: "Raketa_2628_H.jpg"
movementlistkey: "raketa"
caliberkey: "2628.H"
manufacturers: ["raketa"]
manufacturers_weight: 2628
categories: ["movements","movements_r","movements_r_raketa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Raketa 2628.H](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Raketa_2628_H)
usagegallery: 
  - image: "r/raketa/Raketa_HAU_Zifferblatt_Raketa_2628_H.jpg"
    description: "Raketa gents watch  (dial only)"
---
Lorem Ipsum