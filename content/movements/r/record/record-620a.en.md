---
title: "Record 620A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Record Watch","Record","Geneve","Record 620A","620","17","jewels","17 jewels","17 Jewels","swiss","Switzerland","screw balance"]
description: "Record 620A"
abstract: ""
preview_image: "record_620a-mini.jpg"
image: "Record_620A.jpg"
movementlistkey: "record"
caliberkey: "620A"
manufacturers: ["record"]
manufacturers_weight: 620
categories: ["movements","movements_r","movements_r_record_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum