---
title: "Record 109"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Record 109","Record","109","15 Jewels","form movement","caliber","swiss","switzerland","ladies` watch"]
description: "Record 109"
abstract: ""
preview_image: "record_109-mini.jpg"
image: "Record_109.jpg"
movementlistkey: "record"
caliberkey: "109"
manufacturers: ["record"]
manufacturers_weight: 109
categories: ["movements","movements_r","movements_r_record_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/record/Record_DAU_2.jpg"
    description: "Record ladies' watch"
---
Lorem Ipsum