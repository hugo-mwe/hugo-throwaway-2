---
title: "Ronda 1017"
date: 2017-12-14T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 1017","Ronda","1017","Lausen","Datum","Zentralsekunde","Quickset","21 Jewels","21 Rubis","21 Steine","Stiftanker"]
description: "Ronda 1017 - Ein 21-steiniges Stiftankerwerk"
abstract: "Ein besseres schweizer Stiftankerwerk, bei mit Steinen, allerdings auch mit solchen ohne Funktion, nicht gegeizt wurde."
preview_image: "ronda_1017-mini.jpg"
image: "Ronda_1017.jpg"
movementlistkey: "ronda"
caliberkey: "1017"
manufacturers: ["ronda"]
manufacturers_weight: 1017
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum