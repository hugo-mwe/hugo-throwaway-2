---
title: "Ronda 1411"
date: 2013-12-29T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 1411","Ronda","1411","1413","Swiss","1 Jewel","Uhr","Uhren","Armbanduhr","Armbanduhren","Uhrwerk","1 Stein","Handaufzug","Schweiz","Plastik"]
description: "Ronda 1411 - ein spätes, einfaches Stiftankerwerk mit einem Stein und Plastiklager für die Unruh"
abstract: "Eein spätes, einfaches Stiftankerwerk mit einem Stein und Plastiklager für die Unruh"
preview_image: "ronda_1411-mini.jpg"
image: "Ronda_1411.jpg"
movementlistkey: "ronda"
caliberkey: "1411"
manufacturers: ["ronda"]
manufacturers_weight: 1411
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/parker/Parker_DAU_Ronda_1411.jpg"
    description: "Parker 2000 Damenuhr"
---
Lorem Ipsum