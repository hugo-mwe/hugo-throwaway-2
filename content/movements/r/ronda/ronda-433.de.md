---
title: "Ronda 433"
date: 2013-12-29T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda","Lausen","Swiss","Schweiz","Ronda 433","433","1413","17 Jewels","Anker","Plastiklager","Plastik"]
description: "Ronda 433 - Ein spätes, einfaches schweizer Ankerwerk mit Plastiklagern für die Unruh"
abstract: "Ein spätes, einfaches Steinankerwerk, dessen Unruhlager aus Plastik statt aus synthetischen Rubinen bestehen"
preview_image: "ronda_433-mini.jpg"
image: "Ronda_433.jpg"
movementlistkey: "ronda"
caliberkey: "433"
manufacturers: ["ronda"]
manufacturers_weight: 433
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>Das vorliegende Werk kam als New Old Stock, komplett verharzt nach geschätzten 30 Jahren Lagerung ins Labor und bekam einen Komplettservice. Dennoch war es auch anschließend nicht gangfähig, denn das Höhenspiel der Unruhbrücke war völlig falsch, die Unruh wurde fix in die Lager gepreßt.</p><p>Mit einem flachen Abstandsring zwischen Unruhkloben und -Pfeiler, sowie einer, mit Nagellack fixierten Schraube, konnte das Höhenspiel wieder in Ordnung gebracht werden, und das Ronda 433 läuft jetzt mit perfekter Amplitude, so wie am ersten Tag.</p>
timegrapher_old: 
  description: |
    Auf der Zeitwaage macht das Werk einen ordentlichen Eindruck, wenn auch die maximale Lagedifferenz von 50 Sekunden pro Tag deutlich zu viel für ein Palettenankerwerk ist.
  images:
    ZO: Zeitwaage_Ronda_433_ZO.jpg
    ZU: Zeitwaage_Ronda_433_ZU.jpg
    3O: Zeitwaage_Ronda_433_3O.jpg
    6O: Zeitwaage_Ronda_433_6O.jpg
    9O: Zeitwaage_Ronda_433_9O.jpg
    12O: Zeitwaage_Ronda_433_12O.jpg
  values:
    ZO: "+25"
    ZU: "+-0"
    3O: "+50"
    6O: "+45"
    9O: "+50"
    12O: "+25"
usagegallery: 
  - image: "p/parker/Parker_de_Luxe_HAU_Ronda_433.jpg"
    description: "Parker de Luxe Herrenuhr"
---
Lorem Ipsum