---
title: "Ronda 4135"
date: 2013-02-24T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 4135","Ronda","4135","Lausen","RAX","17 Jewels","17 Steine","Handaufzug","Datum","Schnellkorrektur"]
description: "Eines der letzten mechanischen Ronda-Werke, mit 8 3/4 Linien Durchmesser und einer Datumsindikation mit interessanter Schnellkorrektur.  Vorstellung mit Video, Makroaufnahmen, Zeitwaagen-Ausgabe und vielen Fotos."
abstract: "Eines der letzten mittelkleinen Handaufzugswerke von Ronda, ein echtes Ankerwerk mit 17 Steinen, Handaufzug und einer Datumsindikation mit interessanter Schnellkorrektur"
preview_image: "ronda_4135-mini.jpg"
image: "Ronda_4135.jpg"
movementlistkey: "ronda"
caliberkey: "4135"
manufacturers: ["ronda"]
manufacturers_weight: 4135
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam als NOS-Werk, vermutlich zwei Jahrzehnte lang gelagert, ins Labor. Es wurden keine Serviearbeiten daran vorgenommen, auch kein Ölwechsel.
links: |
  <p>* [Ermano Uhrwerke GmbH](http://www.ermano.de): Deutsche Ronda-Vertretung
  * [Watch-Wiki: Ronda 4135](http://watch-wiki.org/index.php?title=Ronda_4135)</p><p>Vielen Dank an die Ermano Uhrwerke GmbH für die Spende dieses Werks und die Überlassung technischer Unterlagen</p>
timegrapher_old: 
  description: |
    Auf der Zeitwaage gab dieses nicht weiter regulierte Werk trotz langer Lagerzeit ein gutes Bild ab, insbesondere wenn man bedenkt, daß es jahre- bis jahrzehntelang außer Betrieb eingelagert war.
  images:
    ZO: Zeitwaage_Ronda_4135_ZO.jpg
    ZU: Zeitwaage_Ronda_4135_ZU.jpg
    3O: Zeitwaage_Ronda_4135_3O.jpg
    6O: Zeitwaage_Ronda_4135_6O.jpg
    9O: Zeitwaage_Ronda_4135_9O.jpg
    12O: Zeitwaage_Ronda_4135_12O.jpg
  values:
    ZO: "+24"
    ZU: "+10"
    3O: "-15"
    6O: "-30"
    9O: "+-0"
    12O: "-15"
---
Lorem Ipsum