---
title: "Ronda 1215"
date: 2017-09-19T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 1215","Ronda","1215","Lausen","Datum","Zentralsekunde","Quickset","21 Jewels","21 Rubis","21 Steine","Stiftanker"]
description: "Ronda 1215 - Ein besseres Stiftankerwerk mit 21 Steinen"
abstract: "Ein besseres schweizer Stiftankerwerk, bei mit Steinen nicht gegeizt wurde."
preview_image: "ronda_1215-mini.jpg"
image: "Ronda_1215.jpg"
movementlistkey: "ronda"
caliberkey: "1215"
manufacturers: ["ronda"]
manufacturers_weight: 1215
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum