---
title: "Ronda 9033"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 9033","Ronda","9033","9033","17 Jewels","17 Rubis","Incabloc","Switzerland","17 jewels","17 rubis"]
description: "Ronda 9033"
abstract: ""
preview_image: "ronda_9033-mini.jpg"
image: "Ronda_9033.jpg"
movementlistkey: "ronda"
caliberkey: "9033"
manufacturers: ["ronda"]
manufacturers_weight: 9033
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum