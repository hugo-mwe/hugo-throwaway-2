---
title: "Ronda 5101"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 5101","Ronda","5101","17 Jewels","swiss","pin lever","form movement"]
description: "Ronda 5101"
abstract: ""
preview_image: "ronda_5101-mini.jpg"
image: "Ronda_5101.jpg"
movementlistkey: "ronda"
caliberkey: "5101"
manufacturers: ["ronda"]
manufacturers_weight: 5101
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Jeansuhr.jpg"
    description: "unmarked ladies' jeans watch"
---
Lorem Ipsum