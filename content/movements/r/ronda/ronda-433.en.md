---
title: "Ronda 433"
date: 2013-12-29T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda","Lausen","Swiss","Ronda 433","433","1413","17 Jewels","lever","pallet lever","plastic bearings"]
description: "Ronda 433 - A late, simple swiss pallet lever movement with plastic bearings for the balance staff"
abstract: "A late, simple pallet lever movement whose balance staff bearings are made of plastic instead of synthetic rubies."
preview_image: "ronda_433-mini.jpg"
image: "Ronda_433.jpg"
movementlistkey: "ronda"
caliberkey: "433"
manufacturers: ["ronda"]
manufacturers_weight: 433
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>The specimen shown here came as completely gummed NOS movement into the lab and got a full service. After that, it still did not run. Further investigation showed, that the height adjustment of the balance cock was completely wrong and the balance wheel was stucked into the plastic bearings.</p><p>With a tiny distance ring and a screw, which was fixed by nail polish, the height adjustment could be fixed and the balance now beats with perfect amplitude, like on the its first day after production.</p>
timegrapher_old: 
  description: |
    On the timegrapher, the movement shows rates, which are ok. Nevertheless, the maximum position difference of 50 secs per day is way too much for a pallet lever movement.
  images:
    ZO: Zeitwaage_Ronda_433_ZO.jpg
    ZU: Zeitwaage_Ronda_433_ZU.jpg
    3O: Zeitwaage_Ronda_433_3O.jpg
    6O: Zeitwaage_Ronda_433_6O.jpg
    9O: Zeitwaage_Ronda_433_9O.jpg
    12O: Zeitwaage_Ronda_433_12O.jpg
  values:
    ZO: "+25"
    ZU: "+-0"
    3O: "+50"
    6O: "+45"
    9O: "+50"
    12O: "+25"
usagegallery: 
  - image: "p/parker/Parker_de_Luxe_HAU_Ronda_433.jpg"
    description: "Parker de Luxe mens' watch"
---
Lorem Ipsum