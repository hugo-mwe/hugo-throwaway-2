---
title: "Ronda 1175"
date: 2016-11-18T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 1177","Ronda","1177","Lausen","Quarz","Quartz","Ronda-Quartz","date","center second"]
description: "Ronda 1177 - A very mature quartz movement from the second generation of Ronda quartz movements."
abstract: "A very mature quartz movement of the second generation of quartz movements by Ronda. Made around 1975."
preview_image: "ronda_1175-mini.jpg"
image: "Ronda_1175.jpg"
movementlistkey: "ronda"
caliberkey: "1175"
manufacturers: ["ronda"]
manufacturers_weight: 1175
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "r/ronda/Ronda_Quartz_HAU_Ronda_1177.jpg"
    description: "Ronda-Quartz gents watch"
---
Lorem Ipsum