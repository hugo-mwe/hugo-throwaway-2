---
title: "Ronda"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "en"
keywords: []
categories: ["movements","movements_r"]
movementlistkey: "ronda"
abstract: "(Ronda AG, Lausen, Switzerland)"
description: ""
---
(Ronda AG, Lausen, Switzerland)
{{< movementlist "ronda" >}}

{{< movementgallery "ronda" >}}