---
title: "Reconvilier"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "en"
keywords: ["Reconvilier","Switzerland","Swiss"]
categories: ["movements","movements_r"]
movementlistkey: "reconvilier"
description: "Movements of the swiss manufacturer Reconvilier"
abstract: "Movements of the swiss manufacturer Reconvilier"
---
(Societe Horlogere Reconvilier, Reconvilier, Switzerland)
{{< movementlist "reconvilier" >}}

{{< movementgallery "reconvilier" >}}