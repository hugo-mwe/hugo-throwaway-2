---
title: "Reconvilier"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "de"
keywords: ["Reconvilier","Schweiz","Swiss"]
categories: ["movements","movements_r"]
movementlistkey: "reconvilier"
abstract: "Uhrwerke des schweizer Herstellers Reconvilier"
description: "Uhrwerke des schweizer Herstellers Reconvilier"
---
(Societe Horlogere Reconvilier, Reconvilier, Schweiz)
{{< movementlist "reconvilier" >}}

{{< movementgallery "reconvilier" >}}