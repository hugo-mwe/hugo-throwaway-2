---
title: "Rego"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "de"
keywords: []
categories: ["movements","movements_r"]
movementlistkey: "rego"
abstract: "(Rego Watch, R. Lapanouse SA, Bubendorf, Holstein, Schweiz)"
description: ""
---
(Rego Watch, R. Lapanouse SA, Bubendorf, Holstein, Schweiz)
{{< movementlist "rego" >}}

{{< movementgallery "rego" >}}