---
title: "Rego"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "en"
keywords: []
categories: ["movements","movements_r"]
movementlistkey: "rego"
description: ""
abstract: "(Rego Watch, R.Lapanouse SA, Bubendorf, Holstein, Switzerland)"
---
(Rego Watch, R.Lapanouse SA, Bubendorf, Holstein, Switzerland)
{{< movementlist "rego" >}}

{{< movementgallery "rego" >}}