---
title: "Tschudin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "de"
keywords: ["Tschudin","Weil am Rhein","Pforzheim","Deutschland"]
categories: ["movements","movements_t"]
movementlistkey: "tschudin"
abstract: "Uhrwerke des deutschen Herstellers Tschudin aus Pforzheim bzw. Weil am Rhein"
description: "Uhrwerke des deutschen Herstellers Tschudin aus Pforzheim bzw. Weil am Rhein"
---
(Tschudin, Weil am Rhein / Pforzheim, Deutschland)
{{< movementlist "tschudin" >}}

{{< movementgallery "tschudin" >}}