---
title: "Tissot 709"
date: 2014-01-12T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 709","Tissot","709","handwind","manual wind","ladies' watch","Duofix","Incabloc"]
description: "Tissot 709 - a tiny handwound movement for ladies' watches"
abstract: "Ein tiny high-quality handwound movement for womens' watches"
preview_image: "tissot_709-mini.jpg"
image: "Tissot_709.jpg"
movementlistkey: "tissot"
caliberkey: "709"
manufacturers: ["tissot"]
manufacturers_weight: 70900
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_Zifferblatt_Tissot_709.jpg"
    description: "Tissot ladies' watch  (only dial)"
---
Lorem Ipsum