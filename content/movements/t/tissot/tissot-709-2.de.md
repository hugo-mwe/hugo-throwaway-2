---
title: "Tissot 709-2"
date: 2014-01-26T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tissot 709-2","Tissot 709","Tissot","709-2","Handaufzug","Damenuhr","Incabloc"]
description: "Tissot 709-2 - ein kleines, hochwertiges Handaufzugswerk für Damenuhren in seiner letzten Überarbeitung"
abstract: "Ein kleines, hochwertiges Handaufzugswerk für Damenuhren, hier in seiner letzten Überarbeitungsstufe"
preview_image: "tissot_709-2-mini.jpg"
image: "Tissot_709-2.jpg"
movementlistkey: "tissot"
caliberkey: "709-2"
manufacturers: ["tissot"]
manufacturers_weight: 70920
categories: ["movements","movements_t","movements_t_tissot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_Zifferblatt_Tissot_709-2.jpg"
    description: "Tissot Damenuhr (nur Zifferblatt)"
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum