---
title: "Tissot 2251"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 2251","Tissot","2251","Lanco","Astrolon","1 Jewels","mechanical","balance","manual wind","disposable","plastic"]
description: "Tissot 2251"
abstract: ""
preview_image: "tissot_2251-mini.jpg"
image: "Tissot_2251.jpg"
movementlistkey: "tissot"
caliberkey: "2251"
manufacturers: ["tissot"]
manufacturers_weight: 225100
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Tissot 2251](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Tissot_2251)
  * [The Tissot "Astrolon" Plastic Watch  (In-depth article about the "Astrolon" movement series)](http://members.iinet.net.au/~fotoplot/tissot/tissot.html)
usagegallery: 
  - image: "l/lanco/Lanco_Astrolon_HAU.jpg"
    description: "Lanco Astrolon gents watch"
---
Lorem Ipsum