---
title: "Tissot 2401"
date: 2009-08-29T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tissot 2401","Tissot","2401","17 Jewels","Tissot et Fils","Le Locle","Neuenburg","Schweiz","17 Steine","Formwerk","Schnellschwinger"]
description: "Tissot 2401"
abstract: ""
preview_image: "tissot_2401-mini.jpg"
image: "Tissot_2401.jpg"
movementlistkey: "tissot"
caliberkey: "2401"
manufacturers: ["tissot"]
manufacturers_weight: 240100
categories: ["movements","movements_t","movements_t_tissot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_2_Zifferblatt.jpg"
    description: "Tissot Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum