---
title: "Tissot 709-2"
date: 2014-01-26T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 709-2","Tissot 709","Tissot","709-2","handwind","manual wind","ladies' watch","Incabloc"]
description: "Tissot 709-2 - a tiny handwound movement for ladies' watches in its last iteration"
abstract: "Ein tiny high-quality handwound movement for womens' watches in its last iteration"
preview_image: "tissot_709-2-mini.jpg"
image: "Tissot_709-2.jpg"
movementlistkey: "tissot"
caliberkey: "709-2"
manufacturers: ["tissot"]
manufacturers_weight: 70920
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_Zifferblatt_Tissot_709-2.jpg"
    description: "Tissot ladies' watch  (only dial)"
donation: "Many thanks to [Klaus Brunnemer]() for the donation of that movement!"
---
Lorem Ipsum