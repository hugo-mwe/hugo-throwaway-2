---
title: "Timex M27"
date: 2013-07-03T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","M27","Model 27","Stiftanker","V-Conic","Tag","Wochentag"]
description: "Timex M27 - Die Maximalausstattung des Handaufzugswerks M24, zwar ohne Steine, dafür aber mit Tag- und Wochentagsindikation"
abstract: "Die Maximalausstattung des steinlosen Handaufzugskalibers M24, mit Tag- und Wochentagsindikation."
preview_image: "timex_m27-mini.jpg"
image: "Timex_M27.jpg"
movementlistkey: "timex"
caliberkey: "M27"
manufacturers: ["timex"]
manufacturers_weight: 27
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_26950-2723.jpg"
    description: "Timex Herrenuhr, Modell 26950, von 1973"
  - image: "t/timex/26450-02775.jpg"
    description: "Timex Herrenuhr, Modell 26450, von 1975"
---
Lorem Ipsum