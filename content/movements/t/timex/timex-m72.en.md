---
title: "Timex M72"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex","21 jewels","Timex 21","21","Timex M72","72","mechanical","balance","manual wind"]
description: "Timex M72"
abstract: ""
preview_image: "timex_m72-mini.jpg"
image: "Timex_M72.jpg"
movementlistkey: "timex"
caliberkey: "M72"
manufacturers: ["timex"]
manufacturers_weight: 72
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Article from the Timex Watch Forum (all about the 21 jewel Timex movements)](http://www.network54.com/Forum/message?forumid=102266&messageid=1083539182)
usagegallery: 
  - image: "t/timex/Timex_6524-7268.jpg"
    description: "Timex gents watch model 6524"
---
Lorem Ipsum