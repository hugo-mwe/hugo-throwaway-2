---
title: "Timex M105"
date: 2017-01-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M105","Timex","M105 mechanisch","Handaufzug","Datum","Zentralsekunde"]
description: "Timex M105"
abstract: ""
preview_image: "timex_m105-mini.jpg"
image: "Timex_M105.jpg"
movementlistkey: "timex"
caliberkey: "M105"
manufacturers: ["timex"]
manufacturers_weight: 105
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_25418-10580A.jpg"
    description: "Timex Herrenuhr Modell 25418"
  - image: "t/timex/Timex_27671-10578.jpg"
    description: "Timex Taucheruhr Modell 27671"
  - image: "t/timex/Timex_27729-10579.jpg"
    description: "Timex Herrenuhr Modell 27729"
donation: "Das taiwanesische M105 samt zugehöriger Uhr sind eine Spende von [R.Ludwig](/supporters/r-ludwig/). Ganz herzlichen Dank dafür!"
---
Lorem Ipsum