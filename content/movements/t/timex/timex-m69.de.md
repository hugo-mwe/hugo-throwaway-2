---
title: "Timex M69"
date: 2017-01-07T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","Timex M69","Electric","Lady","69","elektromechanisch","mechanisch","Unruh","Unruhe","Damen","Damenuhr","Batterie"]
description: "Timex M69"
abstract: "Das kleinste elektromagnetische Uhrwerk von Timex. Es besitzt sogar schon einen Plastik-Anker!"
preview_image: "timex_m69-mini.jpg"
image: "Timex_M69.jpg"
movementlistkey: "timex"
caliberkey: "M69"
manufacturers: ["timex"]
manufacturers_weight: 69
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_80160-06973.jpg"
    description: "Timex Electric Damenuhr Modell 80160"
  - image: "t/timex/Timex_80361-06976.jpg"
    description: "Timex Electric Damenuhr Modell 80361"
  - image: "t/timex/Timex_80460-6972.jpg"
    description: "Timex Electric Damenuhr Modell 80460"
  - image: "t/timex/Timex_802604.jpg"
    description: "Timex Electric Damenuhr Modell 802604"
donation: "Das jüngste Werk aus Taiwan (mit Plastikanker) samt zugehöriger Uhr sind eine Spende von [R.Ludwig](/supporters/r-ludwig/). Ganz herzlichen Dank dafür!"
---
Lorem Ipsum