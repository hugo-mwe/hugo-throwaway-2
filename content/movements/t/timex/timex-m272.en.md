---
title: "Timex M272"
date: 2018-02-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M272","Timex","M272","Quartz","flat","Portescap","Germany","Lavet-stepper","Lavet","Patents","DE3214683","US4376996A"]
description: "Timex M272 - An ultra flaz quartz movement, which has got a very unusual construction"
abstract: "A very unusual quartz movement construction from the early 1980ies, which was protected by a number of patents."
preview_image: "timex_m272-mini.jpg"
image: "Timex_M272.jpg"
movementlistkey: "timex"
caliberkey: "M272"
manufacturers: ["timex"]
manufacturers_weight: 272
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_40253-27283.jpg"
    description: "Timex ladies' watch, model 40253"
---
Lorem Ipsum