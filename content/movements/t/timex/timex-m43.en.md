---
title: "Timex M43"
date: 2016-01-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M43","Timex 43","Logotime","Quarz","Quartz","rocking motor","US4128992 A"]
description: "Timex M43 - An unusual second generation quartz movement"
abstract: "A very unusual quartz movement of the second generation with a rocking motor"
preview_image: "timex_m43-mini.jpg"
image: "Timex_M43.jpg"
movementlistkey: "timex"
caliberkey: "M43"
manufacturers: ["timex"]
manufacturers_weight: 43
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_56219-04379.jpg"
    description: "Timex gents watch 56219-04379"
  - image: "t/timex/Timex_54218-04379.jpg"
    description: "Timex ladies' watch 54218-04379"
labor: |
  The specimen shown here had corroded and lost battery contacts, which could rather easily be replaced. Since that, it switches loudly once per minute with a daily rate of less that -0.5 seconds per day. Unadjusted!
links: |
  * [US Patent US4128992 A - Rocking motor for low cost quartz watch](https://www.google.com/patents/US4128992) (registered 21.3.1977)
---
Lorem Ipsum