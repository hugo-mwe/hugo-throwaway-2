---
title: "Timex M33"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M33","Timex","M33 mechanical","automatic","selfwind","mens' watch"]
description: "Timex M33"
abstract: ""
preview_image: "timex_m33-mini.jpg"
image: "Timex_M33.jpg"
movementlistkey: "timex"
caliberkey: "M33"
manufacturers: ["timex"]
manufacturers_weight: 33
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_46860-3373.jpg"
    description: "Timex gents watch model 46860"
---
Lorem Ipsum