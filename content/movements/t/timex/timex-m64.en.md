---
title: "Timex M64"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M64","Timex","M64","Quartz","Quarz","Taiwan mechanical","electromechanic","mens' watch"]
description: "Timex M64"
abstract: ""
preview_image: "timex_m64-mini.jpg"
image: "Timex_M64.jpg"
movementlistkey: "timex"
caliberkey: "M64"
manufacturers: ["timex"]
manufacturers_weight: 64
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum