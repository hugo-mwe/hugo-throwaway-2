---
title: "Timex M66"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","Quarz","Quartz","Electric","Timex M66","M66","894","elektromechanisch","mechanisch","Unruh","Unruhe"]
description: "Timex M66"
abstract: ""
preview_image: "timex_m66-mini.jpg"
image: "Timex_M66.jpg"
movementlistkey: "timex"
caliberkey: "M66"
manufacturers: ["timex"]
manufacturers_weight: 66
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum