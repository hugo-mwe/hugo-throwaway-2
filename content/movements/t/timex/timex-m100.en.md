---
title: "Timex M100"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M100","Timex 100","Timex","M100","100 mechanical","manual wind","ladies' watch"]
description: "Timex M100"
abstract: ""
preview_image: "timex_m100-mini.jpg"
image: "Timex_M100.jpg"
movementlistkey: "timex"
caliberkey: "M100"
manufacturers: ["timex"]
manufacturers_weight: 100
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_10052-10075.jpg"
    description: "Timex ladies' watch model 10052"
  - image: "t/timex/Timex_10055_10078.jpg"
    description: "Timex ladies' watch model 10055"
  - image: "t/timex/Timex_20052-10078.jpg"
    description: "Timex ladies' watch model 20052"
---
Lorem Ipsum