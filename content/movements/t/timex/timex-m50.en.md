---
title: "Timex M50"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex","Timex M50","Electric","M50","246","Timex 246","electromechanical","mechanical","balance","battery","cell","electronic"]
description: "Timex M50"
abstract: ""
preview_image: "timex_m50-mini.jpg"
image: "Timex_M50.jpg"
movementlistkey: "timex"
caliberkey: "M50"
manufacturers: ["timex"]
manufacturers_weight: 50
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum