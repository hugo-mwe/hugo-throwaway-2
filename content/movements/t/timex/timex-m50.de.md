---
title: "Timex M50"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","Timex M50","Electric","M50","246","Timex 246","elektromechanisch","mechanisch","Unruh","Unruhe","Electronic","Batterie"]
description: "Timex M50"
abstract: ""
preview_image: "timex_m50-mini.jpg"
image: "Timex_M50.jpg"
movementlistkey: "timex"
caliberkey: "M50"
manufacturers: ["timex"]
manufacturers_weight: 50
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum