---
title: "Langendorf 6630"
date: 2011-02-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Langendorf 6630","Lanco 6630","Langendorf","Lanco","6630","17 Jewels","swiss","ladies` watch"]
description: "Langendorf 6630"
abstract: ""
preview_image: "langendorf_6630-mini.jpg"
image: "Langendorf_6630.jpg"
movementlistkey: "langendorf"
caliberkey: "600"
manufacturers: ["langendorf"]
manufacturers_weight: 600
categories: ["movements","movements_l","movements_l_langendorf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Langendorf 600](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Langendorf_600&)</p><p>This movement was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
usagegallery: 
  - image: "l/lanco/Lanco_DAU.jpg"
    description: "Lanco ladies' watch"
---
Lorem Ipsum