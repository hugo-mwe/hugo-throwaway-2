---
title: "Langendorf 6630"
date: 2011-02-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Langendorf 6630","Lanco 6630","Langendorf","Lanco","6630","17 Jewels","Swiss","17 Steine","Handaufzug","Damenuhr","Schweiz"]
description: "Langendorf 6630"
abstract: ""
preview_image: "langendorf_6630-mini.jpg"
image: "Langendorf_6630.jpg"
movementlistkey: "langendorf"
caliberkey: "600"
manufacturers: ["langendorf"]
manufacturers_weight: 600
categories: ["movements","movements_l","movements_l_langendorf"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Langendorf 600](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Langendorf_600&)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "l/lanco/Lanco_DAU.jpg"
    description: "Lanco Damenuhr"
---
Lorem Ipsum