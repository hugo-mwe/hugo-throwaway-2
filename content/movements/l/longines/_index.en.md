---
title: "Longines"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "en"
keywords: ["Longines","Saint-Imier","Switzerland","Swiss","movement","movements"]
categories: ["movements","movements_l"]
movementlistkey: "longines"
abstract: "Movements from the swiss manufacturer Longines from Saint-Imier"
description: "Movements from the swiss manufacturer Longines from Saint-Imier"
---
(Longines, Saint-Imier, Switzerland)
{{< movementlist "longines" >}}

{{< movementgallery "longines" >}}