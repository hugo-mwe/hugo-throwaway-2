---
title: "Luch"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "de"
keywords: ["Luch","Lutsch","Minsk","Weißrussland"]
categories: ["movements","movements_l"]
movementlistkey: "luch"
abstract: "Uhrwerke des weißrussischen Herstellers Luch (Lutsch) aus Minsk."
description: "Uhrwerke des weißrussischen Herstellers Luch (Lutsch) aus Minsk."
---
(Luch / Lutsch, Minsker Uhrenfabrik, Minsk, Weißrussland)
{{< movementlist "luch" >}}

{{< movementgallery "luch" >}}