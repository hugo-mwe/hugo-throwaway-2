---
title: "Lorsa P76"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa P76","Lorsa","P76","22 Jewels","Automatic","Automatik","22 Steine","Frankreich"]
description: "Lorsa P76"
abstract: ""
preview_image: "lorsa_p76-mini.jpg"
image: "Lorsa_P76.jpg"
movementlistkey: "lorsa"
caliberkey: "P76"
manufacturers: ["lorsa"]
manufacturers_weight: 760
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/ebf/EBF_Automatic.jpg"
    description: "EBF Automatic Herrenuhr"
---
Lorem Ipsum