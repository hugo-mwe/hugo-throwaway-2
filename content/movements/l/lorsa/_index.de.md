---
title: "Lorsa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "de"
keywords: ["Lorsa","Annemasse","Haute-Savoie","Frankreich","France"]
categories: ["movements","movements_l"]
movementlistkey: "lorsa"
abstract: "Uhrwerke des französischen Herstellers Lorsa aus Annemasse in Haute-Savoie."
description: "Uhrwerke des französischen Herstellers Lorsa aus Annemasse in Haute-Savoie."
---
(Lorsa, Annemasse, Haute-Savoie, Frankreich)
{{< movementlist "lorsa" >}}

{{< movementgallery "lorsa" >}}