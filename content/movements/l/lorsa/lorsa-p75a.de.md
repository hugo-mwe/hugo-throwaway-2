---
title: "Lorsa P75A"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa","Lorsa P75A","Lorsa 75","P75A","75","Frankreich"]
description: "Lorsa P75A"
abstract: ""
preview_image: "lorsa_p75a-mini.jpg"
image: "Lorsa_P75A.jpg"
movementlistkey: "lorsa"
caliberkey: "P75A"
manufacturers: ["lorsa"]
manufacturers_weight: 751
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum