---
title: "Lorsa 82"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa","Lorsa 82","82","Frankreich","17 Steine","17","Steine"]
description: "Lorsa 82"
abstract: ""
preview_image: "lorsa_82-mini.jpg"
image: "Lorsa_82.jpg"
movementlistkey: "lorsa"
caliberkey: "82"
manufacturers: ["lorsa"]
manufacturers_weight: 820
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum