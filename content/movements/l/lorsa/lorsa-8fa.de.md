---
title: "Lorsa 8FA"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa 8FA","Lorsa","8FA","Damenuhr","17","Steine","17 Steine","17 Rubis","Formwerk"]
description: "Lorsa 8FA"
abstract: ""
preview_image: "lorsa_8fa-mini.jpg"
image: "Lorsa_8FA.jpg"
movementlistkey: "lorsa"
caliberkey: "8FA"
manufacturers: ["lorsa"]
manufacturers_weight: 891
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/apolo/Apolo_DAU.jpg"
    description: "Apolo Damenuhr"
  - image: "t/tutima/Tutima_DAU.jpg"
    description: "Tutima Damenuhr"
---
Lorem Ipsum