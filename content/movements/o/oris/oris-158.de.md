---
title: "Oris 158"
date: 2010-03-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Pfeilerbauweise","axiale Stoßsicherung","Teil-Stoßsicherung","Oris 158","Oris","158"]
description: "Oris 158 - Ein einfaches Stiftankerwerk mit Teil-Stoßsicherung. Detaillierte Beschreibung mit Bildern, Makro-Aufnahmen, Video und Datenblatt."
abstract: ""
preview_image: "oris_158-mini.jpg"
image: "Oris_158.jpg"
movementlistkey: "oris"
caliberkey: "158"
manufacturers: ["oris"]
manufacturers_weight: 158
categories: ["movements","movements_o","movements_o_oris"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/siro/Siro_Jewelled_Oris_158.jpg"
    description: "Siro Jewelled Herrenuhr"
---
Lorem Ipsum