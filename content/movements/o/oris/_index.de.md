---
title: "Oris"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["Oris","Höstein","Basel","Schweiz","Swiss","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_o"]
movementlistkey: "oris"
description: "Uhrwerke des schweizer Herstellers Oris aus Höstein bei Basel"
abstract: "Uhrwerke des schweizer Herstellers Oris aus Höstein bei Basel"
---
(Oris, Höstein, Basel-Landschaft, Schweiz)
{{< movementlist "oris" >}}

{{< movementgallery "oris" >}}