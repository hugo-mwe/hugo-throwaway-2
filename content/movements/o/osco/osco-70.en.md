---
title: "Osco 70"
date: 2009-11-06T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco 70","Osco","70","17 Jewels","form movement","germany"]
description: "Osco 70"
abstract: ""
preview_image: "osco_70-mini.jpg"
image: "Osco_70.jpg"
movementlistkey: "osco"
caliberkey: "70"
manufacturers: ["osco"]
manufacturers_weight: 700
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/para/Para_Parat_DAU_Zifferblatt.jpg"
    description: "Para Parat ladies' watch  (dial only)"
---
Lorem Ipsum