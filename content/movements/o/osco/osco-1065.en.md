---
title: "Osco 1065"
date: 2015-10-09T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco 1065","Schlund","Osco","1065","Osco 1060","pallet lever","lever","25 Jewels","25 Rubis","Rufasuper"]
description: "Osco 1065 - The base caliber of the only selfwindig movement family from Osco (Otto Schlund)"
abstract: "The base movement of the single selfwinding caliber family by Osco"
preview_image: "osco_1065-mini.jpg"
image: "Osco_1065.jpg"
movementlistkey: "osco"
caliberkey: "1065"
manufacturers: ["osco"]
manufacturers_weight: 10650
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "s/stowa/Stowa_HAU_Osco_1065.jpg"
    description: "Stowa gents watch  (incomplete)"
labor: |
  The specimen shown here has got lots of problems, a scratched dial and missing hands, when it came into the lab. Among the problems, there's a missing ruby inset of the upper Rufasuper bearing, a floating ruby and a shortened axle of the center second arbour. Because of all these problems and the lack of spare parts, a repair was not possible.
---
Lorem Ipsum