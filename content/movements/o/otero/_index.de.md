---
title: "Otero"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["Otero","Uhrenrohwerke","Otto Epple","Epple","Königsbach","Deutschland","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_o"]
movementlistkey: "otero"
description: "Uhrwerke von Otero, der Uhrenrohwerke Otto Epple aus Königsbach in Deutschland"
abstract: "Uhrwerke von Otero, der Uhrenrohwerke Otto Epple aus Königsbach in Deutschland"
---
(Uhrenrohwerke Otto Epple, Königsbach, Deutschland)
{{< movementlist "otero" >}}

{{< movementgallery "otero" >}}