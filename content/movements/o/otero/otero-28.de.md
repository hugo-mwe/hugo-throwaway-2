---
title: "Otero 28"
date: 2018-01-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 28","Otero","Epple","17 Jewels","Monorex","17 Rubis","kleine Sekunde"]
description: "Otero 28 - Ein älteres Mittelklasse-Handaufzugswerk aus Deutschland"
abstract: "Ein älteres deutsches Handaufzugswerk mit Palettenankerhemmung und Schraubenunruh"
preview_image: "otero_28-mini.jpg"
image: "Otero_28.jpg"
movementlistkey: "otero"
caliberkey: "28"
manufacturers: ["otero"]
manufacturers_weight: 28
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam verbaut in einer sichtlich lange und intensiv getragenen Uhr ins Werk, lief aber so gut, daß auf einen Service verzichtet werden konnte.
timegrapher_old: 
  rates:
    ZO: "0"
    ZU: "0"
    3O: "-99"
    6O: "-143"
    9O: "-126"
    12O: "-74"
  amplitudes:
    ZO: "237"
    ZU: "285"
    3O: "227"
    6O: "200"
    9O: "223"
    12O: "227"
  beaterrors:
    ZO: "5.6"
    ZU: "4.4"
    3O: "6.8"
    6O: "6.7"
    9O: "5.8"
    12O: "6.2"
---
Lorem Ipsum