---
title: "Otero 251"
date: 2017-11-18T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 251","Otero","Epple","251","Formwerk","17 Jewels","Monorex","Datum","17 Rubis"]
description: "Otero 251 - Ein Formwerk in Damenuhrengröße mit Datumsanzeige"
abstract: "Ein Formwerk für Damenuhren, das mit einer Datumsanzeige ergänzt wurde. Es gehört zu den wenigen Werken mit Datum, die keine Sekundenanzeige besitzen."
preview_image: "otero_251-mini.jpg"
image: "Otero_251.jpg"
movementlistkey: "otero"
caliberkey: "251"
manufacturers: ["otero"]
manufacturers_weight: 251
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum