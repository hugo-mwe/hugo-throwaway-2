---
title: "Otero 441"
date: 2009-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 441","Otero","441","17 Jewels","21 Jewels","Epple","germany","date indication"]
description: "Otero 441"
abstract: ""
preview_image: "otero_441-mini.jpg"
image: "Otero_441.jpg"
movementlistkey: "otero"
caliberkey: "441"
manufacturers: ["otero"]
manufacturers_weight: 441
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/praetina/Praetina_HAU_3_Zifferblatt.jpg"
    description: "Prätina gents watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: Otero 441](http://ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&102&2uswk&Otero_441)</p><p>This movement in the version with screw balance was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum