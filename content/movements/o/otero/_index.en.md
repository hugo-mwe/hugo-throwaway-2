---
title: "Otero"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "en"
keywords: ["Otero","Otto Epple","Epple","Uhrenrohwerke","Königsbach","Germany","german","movements"]
categories: ["movements","movements_o"]
movementlistkey: "otero"
abstract: "Movements from the german manufacturer Otero (Uhrenrohwerke Otto Epple) from Königsbach"
description: "Movements from the german manufacturer Otero (Uhrenrohwerke Otto Epple) from Königsbach"
---
(Uhrenrohwerke Otto Epple, Königsbach, Germany)
{{< movementlist "otero" >}}

{{< movementgallery "otero" >}}