---
title: "Otero 251"
date: 2017-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 251","Otero","Epple","251","form movement","17 Jewels","Monorex","date","17 Rubis"]
description: "Otero 251 - A form movement in ladies' size with date indication"
abstract: "A form movement for ladies' watches, which has got a date indication. It belongs to the few movements with date, but without seconds indication."
preview_image: "otero_251-mini.jpg"
image: "Otero_251.jpg"
movementlistkey: "otero"
caliberkey: "251"
manufacturers: ["otero"]
manufacturers_weight: 251
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum