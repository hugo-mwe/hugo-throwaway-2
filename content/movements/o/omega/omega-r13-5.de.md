---
title: "Omega R13.5"
date: 2016-03-11T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Omega","R13.5","Omega 240","240","Formwerk","Schraubenunruh"]
description: "Omega R13.5 (Omega 240) - Ein recht gängiges Damenuhren-Formwerk aus den 1940er Jahren"
abstract: "Ein relativ gängiges Handaufzugswerk in guter Verarbeitung"
preview_image: "omega_r13.5-mini.jpg"
image: "Omega_R13.5.jpg"
movementlistkey: "omega"
caliberkey: "R13.5"
manufacturers: ["omega"]
manufacturers_weight: 135
categories: ["movements","movements_o","movements_o_omega"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Werk aus dem Jahr 1947 zeigt deutliche Gebrauchsspuren, das Zifferblatt der zugehörigen Uhr ist kaum mehr lesbar und am Werk finden sich diverse Rost-Spuren. Leider hat wohl auch die Unruhwelle im Laufe der Zeit gelitten, sei es durch starke Stöße oder durch Rost, jedenfalls zeigt das getestete Exemplar auch nach einer Reinigung auf der Zeitwaage nur bescheidene Ergebnisse.
donation: "Dieses Werk samt Uhr wurde von [Günter G.](/supporters/guenter-g/) gespendet, ganz herzlichen Dank dafür!"
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "-7"
    3O: "+197"
    6O: "+115"
    9O: "-300"
    12O: "-270"
  amplitudes:
    ZO: "148"
    ZU: "134"
    3O: "201"
    6O: "131"
    9O: "133"
    12O: "125"
  beaterrors:
    ZO: "2.7"
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
usagegallery: 
  - image: "o/omega/Omega_DAU_Omega_R13.5.jpg"
    description: "Omega Damenuhr"
---
Lorem Ipsum