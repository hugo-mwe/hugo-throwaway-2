---
title: "Omega 690"
date: 2009-08-07T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Omega 690","Omega","690","17 Jewels","Biel","Bienne","Baguette","Fashion","Brandt Freres","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","17 Steine","Formwerk"]
description: "Omega 690 - A rare and extremely small baguette form movement for fashion watches"
abstract: "A rare and extremely small baguette form movement for fashion watches"
preview_image: "omega_690-mini.jpg"
image: "Omega_690.jpg"
movementlistkey: "omega"
caliberkey: "690"
manufacturers: ["omega"]
manufacturers_weight: 690
categories: ["movements","movements_o","movements_o_omega_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/omega/Omega_Unknown_Zifferblatt.jpg"
    description: "unknown Omega fashion watch"
donation: "This movement was donated by [Harald Hoeber](). Thank you very much!"
---
Lorem Ipsum