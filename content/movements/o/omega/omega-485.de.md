---
title: "Omega 485"
date: 2014-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Omega 485","Omega","Formwerk","Damenuhr","17 Jewels","17 Steine","KIF Ultraflex","Kupfer"]
description: "Omega 485 - ein relativ spätes Damenuhrenformwerk"
abstract: "Ein klassisches Damenuhren-Formwerk, das ab 1969 produziert wurde"
preview_image: "omega_485-mini.jpg"
image: "Omega_485.jpg"
movementlistkey: "omega"
caliberkey: "485"
manufacturers: ["omega"]
manufacturers_weight: 485
categories: ["movements","movements_o","movements_o_omega"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  images:
    ZO: Zeitwaage_Omega_485_ZO.jpg
    ZU: Zeitwaage_Omega_485_ZU.jpg
    3O: Zeitwaage_Omega_485_3O.jpg
    6O: Zeitwaage_Omega_485_6O.jpg
    9O: Zeitwaage_Omega_485_9O.jpg
    12O: Zeitwaage_Omega_485_12O.jpg
  values:
    ZO: "+2"
    ZU: "+7"
    3O: "+4"
    6O: "+15"
    9O: "+27"
    12O: "+40"
usagegallery: 
  - image: "o/omega/Omega_DAU_Omega_485.jpg"
    description: "Omega Genève Damenuhr Ref. 511.346"
donation: "Vielen Dank an [Frau Müller](/supporters/g-mueller/) für die Spende dieses Werks samt Uhr!"
labor: |
  Das vorliegende Werk befand sich in einwandfreiem Zustand, deswegen wurde auf ein Zerlegen und eine Reinigung verzichtet. Es wurde lediglich eine minimale Korrektur des Abfallfehlers, sowie eine kleine Justierung auf der Zeitwaage vorgenommen.
---
Lorem Ipsum