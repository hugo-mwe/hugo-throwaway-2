---
title: "Orient G11"
date: 2010-03-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko","Diafix","balance brigde","Diashock","Orient G11","Orient","G11","21 Jewels","Tokio","Japan","Seiko","Tokyo"]
description: "Orient G11 - A 21j handwinding movement from Japan. Detailed description with photos, video and data sheet."
abstract: ""
preview_image: "orient_g11-mini.jpg"
image: "Orient_G11.jpg"
movementlistkey: "orient"
caliberkey: "G11"
manufacturers: ["orient"]
manufacturers_weight: 11
categories: ["movements","movements_o","movements_o_orient_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/orient/Orient_Multi_Year_Calendar_HAU_Orient_G11.jpg"
    description: "Orient Multi Year Calendar gents watch"
---
Lorem Ipsum