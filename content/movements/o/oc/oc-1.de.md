---
title: "OC 1"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["OC 1","OC Cal. R.1","OC","2 Jewels","mechanisch","Handaufzug","Stiftanker","Steine"]
description: "OC 1"
abstract: ""
preview_image: "oc_1-mini.jpg"
image: "OC_1.jpg"
movementlistkey: "oc"
caliberkey: "1"
manufacturers: ["oc"]
manufacturers_weight: 1
categories: ["movements","movements_o","movements_o_oc"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/erc/ERC_Super_de_Luxe.jpg"
    description: "ERC Super de Luxe Damenuhr"
---
Lorem Ipsum