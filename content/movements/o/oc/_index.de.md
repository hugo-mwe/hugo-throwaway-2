---
title: "OC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["OC","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_o"]
movementlistkey: "oc"
abstract: "Uhrwerke des unbekannten Herstellers \"OC\""
description: "Uhrwerke des unbekannten Herstellers \"OC\""
---
(leider ohne nähere bekannte Details)
{{< movementlist "oc" >}}

{{< movementgallery "oc" >}}