---
title: "ZIM"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["z"]
language: "de"
keywords: ["ZIM","Maslinikow","Samara","Russland"]
categories: ["movements","movements_z"]
movementlistkey: "zim"
abstract: "Uhrwerke des russischen Herstellers ZIM aus der Uhrenfabrik Maskinikow in Samara, Russland"
description: "Uhrwerke des russischen Herstellers ZIM aus der Uhrenfabrik Maskinikow in Samara, Russland"
---
(Uhrenfabrik Maslinikow, Samara, Russland)
{{< movementlist "zim" >}}

{{< movementgallery "zim" >}}