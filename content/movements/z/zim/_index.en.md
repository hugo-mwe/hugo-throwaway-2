---
title: "ZIM"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["z"]
language: "en"
keywords: ["ZIM","Maslinikow","Russia","Samara","USSR"]
categories: ["movements","movements_z"]
movementlistkey: "zim"
abstract: "Movements of ZIM, the Maslinikow Watch Factory of Samara, Russia"
description: "Movements of ZIM, the Maslinikow Watch Factory of Samara, Russia"
---
(Maslinikow Watch Factory, Samara, Russia)
{{< movementlist "zim" >}}

{{< movementgallery "zim" >}}