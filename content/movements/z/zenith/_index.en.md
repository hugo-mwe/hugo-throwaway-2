---
title: "Zenith"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["z"]
language: "en"
keywords: ["Zenith","Le Locle","Swiss","Switzerland"]
categories: ["movements","movements_z"]
movementlistkey: "zenith"
description: "Movements of the swiss manufacturer Zenith, located in Le Locle."
abstract: "Movements of the swiss manufacturer Zenith, located in Le Locle."
---
(Zenith, Le Locle, Switzerland)
{{< movementlist "zenith" >}}

{{< movementgallery "zenith" >}}