---
title: "Zenith"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["z"]
language: "de"
keywords: ["UMF","Ruhla","VEB. DDR"]
categories: ["movements","movements_z"]
movementlistkey: "zenith"
abstract: "Uhrwerke der UMF (VEB Uhren und Maschinenfabrik Ruhla, Deutschland)"
description: "Uhrwerke der UMF (VEB Uhren und Maschinenfabrik Ruhla, Deutschland)"
---
(Zenith SA, Le Locle, Schweiz)
{{< movementlist "zenith" >}}

{{< movementgallery "zenith" >}}