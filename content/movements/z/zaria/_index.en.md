---
title: "Zaria"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["z"]
language: "en"
keywords: ["Zaria","Minsk","Pensa","Russia","USSR"]
categories: ["movements","movements_z"]
movementlistkey: "zaria"
abstract: "Movements of the Minsk Watch Factory / Pensa Watch Factory from Minsk and Pensa, Russia"
description: "Movements of the Minsk Watch Factory / Pensa Watch Factory from Minsk and Pensa, Russia"
---
(Minsk Watch Factory, Minsk, Russia / Pensa Watch Factory, Pensa, Russia)
{{< movementlist "zaria" >}}

{{< movementgallery "zaria" >}}