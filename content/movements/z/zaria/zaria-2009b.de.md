---
title: "Zaria 2009B"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zaria","Zaria 2009B","Zaria 2009","2009","2009B","Russland","UdSSR","Sowjetunion"]
description: "Zaria 2009B"
abstract: ""
preview_image: "zaria_2009b-mini.jpg"
image: "Zaria_2009B.jpg"
movementlistkey: "zaria"
caliberkey: "2009B"
manufacturers: ["zaria"]
manufacturers_weight: 200920
categories: ["movements","movements_z","movements_z_zaria"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum