---
title: "Zaria 1509"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zaria 1509","Zaria","1509","Zarja","Saria","Sarja","17 Jewels","Russland","UdSSR","Sowjetunion"]
description: "Zaria 1509"
abstract: ""
preview_image: "zaria_1509-mini.jpg"
image: "Zaria_1509.jpg"
movementlistkey: "zaria"
caliberkey: "1509"
manufacturers: ["zaria"]
manufacturers_weight: 150900
categories: ["movements","movements_z","movements_z_zaria"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_DAU_russ_werk.jpg"
    description: "Meister-Anker Damenuhr"
  - image: "c/corsar/Corsar_DAU.jpg"
    description: "Corsar Damenuhr"
  - image: "c/cornavin/Cornavin_HAU.jpg"
    description: "Cornavin Herrenuhr"
  - image: "z/zaria/Zaria_DAU_2.jpg"
    description: "Zaria Damenuhr"
---
Lorem Ipsum