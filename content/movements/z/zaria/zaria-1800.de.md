---
title: "Zaria 1800"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zaria 1800","Zaria","1800","Zarja","Saria","Sarja","16 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Uhrwerk","Kaliber","Russland","UdSSR","Sowjetunion"]
description: "Zaria 1800"
abstract: ""
preview_image: "zaria_1800-mini.jpg"
image: "Zaria_1800.jpg"
movementlistkey: "zaria"
caliberkey: "1800"
manufacturers: ["zaria"]
manufacturers_weight: 180000
categories: ["movements","movements_z","movements_z_zaria"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "z/zaria/Zaria_DAU.jpg"
    description: "Zaria Damenuhr"
  - image: "u/umf/UMF_Ruhla_DAU_16_Rubis.jpg"
    description: "UMF Ruhla Damenuhr"
  - image: "z/zaria/Zaria_DAU_3.jpg"
    description: "Zaria Damenuhr"
---
Lorem Ipsum