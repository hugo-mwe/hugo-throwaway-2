---
title: "HPP 602"
date: 2010-03-08T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pallet lever movement","minute wheel","mainspring barrel","transmission wheel","HP 602","HPP 602","HPP","602","HP 602","Henzi & Pfaff","Henzi und Pfaff","17 Jewels","Roskopf","germany"]
description: "HPP 602 - A simple, 17j lever movement of Roskopf type from Germany. Detailed description with photos, macro image, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "hpp_602-mini.jpg"
image: "HPP_602.jpg"
movementlistkey: "hpp"
caliberkey: "602"
manufacturers: ["hpp"]
manufacturers_weight: 602
categories: ["movements","movements_h","movements_h_hpp_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    For a movement of approx. 55, which is of a simple construction, the results are astonishing well. There are no abnormalities, and the deviation window of -15 ... +15 seconds per day is more than just "good"! The slanted patterns on position "dial up" can be a sign of the broken center second hand axle, which avoids a proper centering of that wheel.
  images:
    ZO: Zeitwaage_HPP_602_ZO.jpg
    ZU: Zeitwaage_HPP_602_ZU.jpg
    3O: Zeitwaage_HPP_602_3O.jpg
    6O: Zeitwaage_HPP_602_6O.jpg
    9O: Zeitwaage_HPP_602_9O.jpg
    12O: Zeitwaage_HPP_602_12O.jpg
  values:
    ZO: "+-0"
    ZU: "+10"
    3O: "-5"
    6O: "+15"
    9O: "+10"
    12O: "-15"
usagegallery: 
  - image: "a/anker/Anker_HAU_HPP_602.jpg"
    description: "Anker gents watch"
labor: |
  <p>The specimen shown here came dirty and gummed to the lab. It was disassembled, cleaned and olied.</p><p>The missing coupling wheel and the broken axle of the center second wheel could not be replaced due to missing spare parts, but that made no differences fot the lab tests. Nevertheless, you can see, that several people took hands on this movement in the past, and not all were lucky.</p>
links: |
  * [Ranfft Uhren: HPP 602](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&HPP_602)  (The specimen shown there uses a different click spring)
---
Lorem Ipsum