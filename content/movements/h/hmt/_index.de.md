---
title: "HMT"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "de"
keywords: ["HMT","HMT Limited","Hindustan Machine Tools","Hindustan Machine Tools Limited","Banglore","Indien"]
categories: ["movements","movements_h"]
movementlistkey: "hmt"
description: "Uhrwerke des indischen Herstellers HMT Limited (Hindustan Machine Tools Limited) als Bangalore"
abstract: "Uhrwerke des indischen Herstellers HMT Limited (Hindustan Machine Tools Limited) als Bangalore"
---
(HMT Limited / Hindustan Machine Tools Limited, Bangalore, Indien)
{{< movementlist "hmt" >}}

{{< movementgallery "hmt" >}}