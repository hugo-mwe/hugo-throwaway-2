---
title: "Sefea"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "sefea"
abstract: "(Sefea, Société européenne de fabrication d'ébauches d'Annemasse, Annemasse, France)"
description: ""
---
(Sefea, Société européenne de fabrication d'ébauches d'Annemasse, Annemasse, France)
{{< movementlist "sefea" >}}

{{< movementgallery "sefea" >}}