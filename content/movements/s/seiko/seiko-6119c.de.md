---
title: "Seiko 6119C"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Seiko 6119C","Seiko 6119","6119C","Seiko","Automatic","21 Jewels","mechanisch","Automatic","Japan","japanisch","Steine"]
description: "Seiko 6119C"
abstract: ""
preview_image: "seiko_6119c-mini.jpg"
image: "Seiko_6119C.jpg"
movementlistkey: "seiko"
caliberkey: "6119C"
manufacturers: ["seiko"]
manufacturers_weight: 6119
categories: ["movements","movements_s","movements_s_seiko"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_HAU_Automatic_4.jpg"
    description: "Seiko 5 Herrenuhr"
---
Lorem Ipsum