---
title: "Seiko 2206A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 2206A","Seiko","2206A","Seikosha","Japan","17 Jewels","Hi-Beat","Hi Beat","Automatic","selfwindig","ladies` watch"]
description: "Seiko 2206A"
abstract: ""
preview_image: "seiko_2206a-mini.jpg"
image: "Seiko_2206A.jpg"
movementlistkey: "seiko"
caliberkey: "2206A"
manufacturers: ["seiko"]
manufacturers_weight: 2206
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Seiko 2206A](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Seiko_2206A)
usagegallery: 
  - image: "s/seiko/Seiko_DAU_Automatic_2.jpg"
    description: "Seiko Automatic ladies' watch"
---
Lorem Ipsum