---
title: "Seiko 7S26A"
date: 2018-01-01T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 7S26A","7S26","Seiko 7S26","Automatic","selfwinding","Magic Lever","weekday","date","center second","Quickset","21 Jewels","21 Rubis","selfwinding"]
description: "Seiko 7S26A - A very rational constructed modern selfwinding movement"
abstract: "A very well known-movement,  purposefully constructed, at the expense of the appearance."
preview_image: "seiko_7s26a-mini.jpg"
image: "Seiko_7S26A.jpg"
movementlistkey: "seiko"
caliberkey: "7S26A"
manufacturers: ["seiko"]
manufacturers_weight: 726
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here came without case, slight water damage and broken magic lever into the lab. Upon further inspection, it became clear, that especially the plastic parts are pretty sensitive, especially those of the calender mechanism.
timegrapher_old: 
  description: |
    Before the movement was tested on the timegrapher, it was adjusted a little bit, since it ran pretty slow. Afterwards, the results were very good, especially, since there movement got not revision and was not in a really good shape:
  rates:
    ZO: "+33"
    ZU: "+12"
    3O: "0"
    6O: ""
    9O: "+8"
    12O: "+3"
  amplitudes:
    ZO: "184"
    ZU: "209"
    3O: "168.6O=-17"
    6O: ""
    9O: "169"
    12O: "175"
  beaterrors:
    ZO: "0.3"
    ZU: "0.0"
    3O: "0.1"
    6O: ""
    9O: "0.5"
    12O: "0.0"
usagegallery: 
  - image: "s/seiko/Seiko_HAU_7S26-0364_Seiko_7S26_Zifferblatt.jpg"
    description: "Seiko gents' watch model 0364  (case missing)"
---
Lorem Ipsum