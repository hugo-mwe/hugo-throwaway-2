---
title: "Seiko 11A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Seiko","Seikosha","Seiko 11A","11A","Japan","Damenuhr","21","17","Steine","Jewels"]
description: "Seiko 11A"
abstract: ""
preview_image: "seiko_11a-mini.jpg"
image: "Seiko_11A.jpg"
movementlistkey: "seiko"
caliberkey: "11A"
manufacturers: ["seiko"]
manufacturers_weight: 11
categories: ["movements","movements_s","movements_s_seiko"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_DAU_3.jpg"
    description: "Seiko Damenuhr"
  - image: "s/seiko/Seiko_DAU_3380_Seiko_11A.jpg"
    description: "Seiko Damenuhr Modell 3380"
donation: "Vielen Dank an [U.Ströbel-Dettmer](/supporters/u-stroebelttmer/) für die Spende der Seiko Damenuhr Modell 3380 und des zugehörigen Werks!"
---
Lorem Ipsum