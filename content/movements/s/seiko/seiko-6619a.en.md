---
title: "Seiko 6619A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 6619A","Seiko","6619A","21 Jewels","Japan","Automatic","Seiko 5"]
description: "Seiko 6619A"
abstract: ""
preview_image: "seiko_6619a-mini.jpg"
image: "Seiko_6619A.jpg"
movementlistkey: "seiko"
caliberkey: "6619A"
manufacturers: ["seiko"]
manufacturers_weight: 6619
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_Weekdater_HAU.jpg"
    description: "Seiko Weekdater gents watch"
links: |
  * [Ranfft Uhren: Seiko 6619A](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Seiko_6619A)
  * [The Watch Spot: Seite 6619-7990 (Sportsmatic)](http://thewatchspot.blogspot.com/2007/10/seiko-6619-7990-sportsmatic.html)
---
Lorem Ipsum