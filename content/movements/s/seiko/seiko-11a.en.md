---
title: "Seiko 11A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko","Seikosha","Seiko 11A","11A","Japan","ladies' watch","21","17","Rubis","Jewels"]
description: "Seiko 11A"
abstract: ""
preview_image: "seiko_11a-mini.jpg"
image: "Seiko_11A.jpg"
movementlistkey: "seiko"
caliberkey: "11A"
manufacturers: ["seiko"]
manufacturers_weight: 11
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "The model 3380 watch and its movement were kindly donated by [U.Ströbel-Dettmer](/supporters/u-stroebelttmer/). Thank you very much!"
usagegallery: 
  - image: "s/seiko/Seiko_DAU_3.jpg"
    description: "Seiko ladies' watch"
  - image: "s/seiko/Seiko_DAU_3380_Seiko_11A.jpg"
    description: "Seiko ladies' watch model 3380"
---
Lorem Ipsum