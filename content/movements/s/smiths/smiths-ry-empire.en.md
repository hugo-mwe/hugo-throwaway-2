---
title: "Smiths RY Empire"
date: 2018-01-11T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Smiths","Wales","Great Britain","5 Jewels","5 Rubis","Pin lever"]
description: "Smiths RY Empire - A pretty rare british pin lever movement with a Roskopf construction"
abstract: "By its looks, you would assign this movement to Switzerland, but in fact, it really comes from Great Britain."
preview_image: "smiths_ry_emipre-mini.jpg"
image: "Smiths_RY_Emipre.jpg"
movementlistkey: "smiths"
caliberkey: "RY Empire"
manufacturers: ["smiths"]
manufacturers_weight: 0
categories: ["movements","movements_s","movements_s_smiths_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The condition of the specimen shown here was pretty good, so no service was made.
timegrapher_old: 
  description: |
    On the timegrapher, the Smiths RY Empire performed as expected:<br/> The position rates differ pretty much, but on being worn, they would more or less egalize each other. With a higher amplitude (could be achieved by a service and/or exchange of the mainspring) and a lower beat error, the rates could be be better. Nevertheless, for a pin lever movement of that age, they are yet acceptable.  
  rates:
    ZO: "+15"
    ZU: "-18"
    3O: "+60"
    6O: "+45"
    9O: "-97"
    12O: "-150"
  amplitudes:
    ZO: "176"
    ZU: "216"
    3O: "213"
    6O: "212"
    9O: "249"
    12O: "208"
  beaterrors:
    ZO: "2.1"
    ZU: "2.6"
    3O: "2.1"
    6O: "2.3"
    9O: "2.2"
    12O: "2.9"
usagegallery: 
  - image: "s/smiths/Smiths_Empire_HU_Smiths_RY_Empire.jpg"
    description: "Smiths Empire gents watch"
---
Lorem Ipsum