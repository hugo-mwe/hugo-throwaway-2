---
title: "Sea-Gull ST6"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Sea-Gull ST6","Sea-Gull","ST6","17 Jewels","China","Automatic","selfwinding"]
description: "Sea-Gull ST6"
abstract: ""
preview_image: "sea-gull_st6-mini.jpg"
image: "Sea-Gull_ST6.jpg"
movementlistkey: "sea-gull"
caliberkey: "ST6"
manufacturers: ["sea-gull"]
manufacturers_weight: 6
categories: ["movements","movements_s","movements_s_sea_gull_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/RxFk.jpg"
    description: "From China..."
---
Lorem Ipsum