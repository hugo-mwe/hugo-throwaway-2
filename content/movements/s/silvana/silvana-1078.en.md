---
title: "Silvana 1078"
date: 2013-10-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Silvana 1078","Venus 75","15 Jewels","15 Rubis","Silvana"]
description: "Silvana 1078 - A typical early handwound movement, probably from the 1930ies"
abstract: "A typical early handwound movement with pallet lever, probably from the 1930ies."
preview_image: "silvana_1078-mini.jpg"
image: "Silvana_1078.jpg"
movementlistkey: "silvana"
caliberkey: "1078"
manufacturers: ["silvana"]
manufacturers_weight: 1078
categories: ["movements","movements_s","movements_s_silvana_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen shown here came in a heavily used watch with cracked crystal and broken decentral second axle into the lab and was, except the balance, cleaned and oiled.
usagegallery: 
  - image: "p/pharos/Pharos_HAU_Silvana_1078.jpg"
    description: "Pharos mens' watch"
timegrapher_old: 
  description: |
    In the position "dial down" the movement showed perfect times besides a larger beat error, which cannot be easily resolved, since there's moveable hairspring stud. But already in position "dial up" it ran very slow, maybe due to a broken or bent balance wheel or too less oiling. In the vertical positions, the rates are all very bad, so maybe the balance axle is really bent or broken. At last, the bad condition of the case would be an indication.
  images:
    ZO: Zeitwaage_Silvana_1078_ZO.jpg
    ZU: Zeitwaage_Silvana_1078_ZU.jpg
    3O: Zeitwaage_Silvana_1078_3O.jpg
    6O: Zeitwaage_Silvana_1078_6O.jpg
    9O: Zeitwaage_Silvana_1078_9O.jpg
    12O: Zeitwaage_Silvana_1078_12O.jpg
  values:
    ZO: "-120"
    ZU: "+-0"
    3O: "-350"
    6O: "-200"
    9O: "+270"
    12O: "-400"
---
Lorem Ipsum