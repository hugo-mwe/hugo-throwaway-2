---
title: "Slava"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: ["Slava","Moskau","Russland","UdSSR","zweite Moskauer Uhrenfabrik"]
categories: ["movements","movements_s"]
movementlistkey: "slava"
abstract: "Uhrwerke von Slava, der zweiten Moskauer Uhrenfabrik in Russland"
description: "Uhrwerke von Slava, der zweiten Moskauer Uhrenfabrik in Russland"
---
(Zweite Moskauer Uhrenfabrik, Moskau, Russland)
{{< movementlist "slava" >}}

{{< movementgallery "slava" >}}