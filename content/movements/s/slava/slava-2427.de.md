---
title: "Slava 2427"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Slava","Slava 2427","2427","Doppelfederhaus","27","Steine","27 Steine","27 Rubis","Russland"]
description: "Slava 2427"
abstract: ""
preview_image: "slava_2427-mini.jpg"
image: "Slava_2427.jpg"
movementlistkey: "slava"
caliberkey: "2427"
manufacturers: ["slava"]
manufacturers_weight: 2427
categories: ["movements","movements_s","movements_s_slava"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum