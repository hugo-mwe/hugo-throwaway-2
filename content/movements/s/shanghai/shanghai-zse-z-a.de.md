---
title: "Shanghai ZSE Z-A"
date: 2009-11-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Shanghai ZSE Z-A","Shanghai","ZSE Z-A","17 Jewels","17 Zuan","China","17 Steine","Handaufzug"]
description: "Shanghai ZSE Z-A"
abstract: ""
preview_image: "shanghai_zse_z-a-mini.jpg"
image: "Shanghai_ZSE_Z-A.jpg"
movementlistkey: "shanghai"
caliberkey: "ZSE Z-A"
manufacturers: ["shanghai"]
manufacturers_weight: 0
categories: ["movements","movements_s","movements_s_shanghai"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Micmicmor Vintage Watch: Shanghai 7 Series](http://micmicmor.blogspot.com/2008/01/shanghia-7120-7series.html)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum