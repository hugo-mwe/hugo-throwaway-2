---
title: "Sonceboz"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: ["Sonceboz","Semag","Ebauches Sonceboz","ES","Schweiz","Swiss"]
categories: ["movements","movements_s"]
movementlistkey: "sonceboz"
abstract: "Uhrwerke von Sonceboz / Semag / Ebauches Sonceboz (ES) aus der Schweiz"
description: "Uhrwerke von Sonceboz / Semag / Ebauches Sonceboz (ES) aus der Schweiz"
---
(Sonceboz, Semag, Ebauches Sonceboz (ES), Sonceboz, Schweiz)
{{< movementlist "sonceboz" >}}

{{< movementgallery "sonceboz" >}}