---
title: "Sonceboz ES 95"
date: 2011-01-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Sonceboz ES 95","Sonceboz","ES 95","Sonceboz SA","Semag AV","Swiss","17 Jewels","Switzerland","17 Rubis"]
description: "Sonceboz ES 95"
abstract: ""
preview_image: "sonceboz_es_95-mini.jpg"
image: "Sonceboz_ES_95.jpg"
movementlistkey: "sonceboz"
caliberkey: "ES 95"
manufacturers: ["sonceboz"]
manufacturers_weight: 95
categories: ["movements","movements_s","movements_s_sonceboz_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/piranha/Piranha_Skelett.jpg"
    description: "Piranha skeleton watch"
links: |
  * [Claro Watch - History  (the history of Claro / Semag / Sonceboz )](http://www.clarowatch.com/family_heritage2.html)
  * [TimeZone Watch School ES 95 Parts](http://www.tztoolshop.com/page190.html)
---
Lorem Ipsum