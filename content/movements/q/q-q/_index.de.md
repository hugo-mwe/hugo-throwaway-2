---
title: "Q&Q"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["q"]
language: "de"
keywords: ["Q&Q","Japan","Korea","Quality And Quantity"]
categories: ["movements","movements_q"]
movementlistkey: "q-q"
description: "Uhrwerke des japanisch-koreanischen Unternehmens Q&Q, einer Citizen-Tochter."
abstract: "Uhrwerke des japanisch-koreanischen Unternehmens Q&Q, einer Citizen-Tochter."
---
(Quality and Quantity, Südkorea)
{{< movementlist "q-q" >}}

{{< movementgallery "q-q" >}}