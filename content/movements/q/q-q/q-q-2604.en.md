---
title: "Q&Q 2604"
date: 2015-09-11T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Q&Q 2604","2604","Plastic","0 Jewels","lever","selfwinding"]
description: "Q&Q 2604 - a far eastern selfwinding plastic lever movement with day/date indication"
abstract: "A very efficiently made far east selfwinding movement with many features but also many plastic parts, including the escapement"
preview_image: "q_q_2604-mini.jpg"
image: "Q_Q_2604.jpg"
movementlistkey: "q-q"
caliberkey: "2604"
manufacturers: ["q-q"]
manufacturers_weight: 2604
categories: ["movements","movements_q","movements_q_q_q_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "q/q_q/Q_Q_Automatic_Q_Q_2604.jpg"
    description: "Q&amp;Q selfwinding mens' watch"
  - image: "q/q_q/Q_Q_HAU_Automatic.jpg"
    description: "Q&amp;Q selfwinding mens' watch"
labor: |
  <p>The specimen shown here came in a heavily used case into the lab. It looks like it was worn ten years and more under hard conditions.</p><p>Probably, after that time, it didn't work well any more (sloppy balance wheel, worn out pivots and so on) and someone, who only knows quartz watches flooded it with oil, which caused it to stop working at all. Sticky hairspring and an oil-sticky lever were the cause.</p><p>The movement was completely cleaned, but since the plastic axles of the wheels became too soft, it wasn't possible to bring it back to work. But at least, it ticks now for a few seconds, so it's not a complete loss.</p>
---
Lorem Ipsum