---
title: "Geneva Sport"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "de"
keywords: ["Geneva Sport","SYW","Schweiz"]
categories: ["movements","movements_g"]
movementlistkey: "geneva-sport"
abstract: "Uhrwerke des schweizer Herstellers Geneva Sport (SYW)"
description: "Uhrwerke des schweizer Herstellers Geneva Sport (SYW)"
---
(Geneva Sport (SYW), Schweiz)
{{< movementlist "geneva-sport" >}}

{{< movementgallery "geneva-sport" >}}