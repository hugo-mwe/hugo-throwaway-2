---
title: "Geneva Sport 230"
date: 2009-05-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["SYW 230","SYW","230","17 Jewels","Geneva Sport 230","Geneva Sport"]
description: "Geneva Sport 230 / SYW 230"
abstract: " "
preview_image: "geneva_sport_230-mini.jpg"
image: "Geneva_Sport_230.jpg"
movementlistkey: "geneva-sport"
caliberkey: "230"
manufacturers: ["geneva-sport"]
manufacturers_weight: 230
categories: ["movements","movements_g","movements_g_geneva_sport_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/cornavin/Cornavin_Geneve_HAU.jpg"
    description: "Cornavin Geneve gents watch"
---
Lorem Ipsum