---
title: "GUB 75"
date: 2009-11-21T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["GUB 75","GUB","75","GUB 06-26","GUB 06","26 Jewels","Glashütte","Glashuette","Glashütter Uhrenbetrieb","DDR","GDR","Spezimatic","26 Steine","Automatik"]
description: "GUB 75"
abstract: " "
preview_image: "gub_75-mini.jpg"
image: "GUB_75.jpg"
movementlistkey: "gub"
caliberkey: "75"
manufacturers: ["gub"]
manufacturers_weight: 75
categories: ["movements","movements_g","movements_g_gub"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "g/glashuette/Glashuette_Spezimatic.jpg"
    description: "Glashütte Spezimatic Herrenuhr"
links: |
  * [Ranfft Uhren: GUB 75](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&GUB_75&)
---
Lorem Ipsum