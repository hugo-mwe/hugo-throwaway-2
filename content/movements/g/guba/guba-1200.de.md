---
title: "Guba 1200"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Guba 1200","Guba","1200","21 Jewels","Glucydur","17 Jewels","17 Steine","21 Steine"]
description: "Guba 1200"
abstract: " "
preview_image: "guba_1200-mini.jpg"
image: "Guba_1200.jpg"
movementlistkey: "guba"
caliberkey: "1200"
manufacturers: ["guba"]
manufacturers_weight: 12000
categories: ["movements","movements_g","movements_g_guba"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Guba 1200](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Guba_1200)
usagegallery: 
  - image: "i/isoma/Isoma_HAU.jpg"
    description: "Isoma Herrenuhr"
  - image: "k/kuester/Kuester_HAU.jpg"
    description: "Küster Herrenuhr"
---
Lorem Ipsum