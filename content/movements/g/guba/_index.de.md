---
title: "Guba"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "de"
keywords: ["Guba","Ellmendingen","Gustav Bauer","Bauer","Deutschland"]
categories: ["movements","movements_g"]
movementlistkey: "guba"
description: "Uhrwerke des deutschen Herstellers Guba (Gustav Bauer) aus Ellmendingen"
abstract: "Uhrwerke des deutschen Herstellers Guba (Gustav Bauer) aus Ellmendingen"
---
(Guba Uhrenrohwerke GmbH (Gustav Bauer), Ellmendingen, Deutschland)
{{< movementlist "guba" >}}

{{< movementgallery "guba" >}}