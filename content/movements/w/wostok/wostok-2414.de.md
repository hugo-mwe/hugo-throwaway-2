---
title: "Wostok 2414"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Wostok 2414","Wostok","2414","17 Jewels","CCCP","USSR","17 Steine","UdSSR","Russland"]
description: "Wostok 2414"
abstract: ""
preview_image: "wostok_2414-mini.jpg"
image: "Wostok_2414.jpg"
movementlistkey: "wostok"
caliberkey: "2414"
manufacturers: ["wostok"]
manufacturers_weight: 241400
categories: ["movements","movements_w","movements_w_wostok"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "w/wostok/Wostok_Kommandirski.jpg"
    description: "Wostok Kommandirski Herrenuhr"
---
Lorem Ipsum