---
title: "Wostok 2214"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok 2214","Wostok","2214","18 Jewels","CCCP","USSR","russia","Breguet hairspring"]
description: "Wostok 2214"
abstract: ""
preview_image: "wostok_2214-mini.jpg"
image: "Wostok_2214.jpg"
movementlistkey: "wostok"
caliberkey: "2214"
manufacturers: ["wostok"]
manufacturers_weight: 221400
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "w/wostok/Wostok_HAU.jpg"
    description: "Wostok gents watch"
---
Lorem Ipsum