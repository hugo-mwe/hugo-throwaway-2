---
title: "Wostok 2414A"
date: 2017-01-06T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Wostok 2414A","Wostok","2414A","17 Jewels","CCCP","USSR","17 Steine","UdSSR","Russland"]
description: "Wostok 2414A"
abstract: "Eines der gängisten und meistverbauten russischen Handaufzugswerke, hier in der ersten Überarbeitung"
preview_image: "wostok_2414a-mini.jpg"
image: "Wostok_2414A.jpg"
movementlistkey: "wostok"
caliberkey: "2414A"
manufacturers: ["wostok"]
manufacturers_weight: 241401
categories: ["movements","movements_w","movements_w_wostok"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk und die drei gezeigten Uhren sind eine Spende der [Eheleute Ditte](/supporters/ditte/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchives!"
timegrapher_old: 
  description: |
    Die Gangwerte sind recht ordentlich, dennoch dürften sie bei einem fabrikneuen und/oder redivierten Exemplar noch deutlich besser aussehen:
  rates:
    ZO: "+7"
    ZU: "-1"
    3O: "+25"
    6O: "+10"
    9O: "-90"
    12O: "-70"
  amplitudes:
    ZO: "222"
    ZU: "205"
    3O: "192"
    6O: "188"
    9O: "171"
    12O: "172"
  beaterrors:
    ZO: "0.0"
    ZU: "0.0"
    3O: "0.1"
    6O: "0.1"
    9O: "0.1"
    12O: "0.1"
usagegallery: 
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A.jpg"
    description: "Wostok Kommandirski Herrenuhr"
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A_2.jpg"
    description: "Wostok Kommandirski Herrenuhr"
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A_Russia.jpg"
    description: "Wostok Kommandirski Herrenuhr mit originalem Stahlband"
labor: |
  Das getestete Exemplar kam in gut gangfähigen Zustand ins Labor und wurde daher nicht revidiert, sondern nur minimal justiert.
---
Lorem Ipsum