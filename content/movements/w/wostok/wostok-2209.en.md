---
title: "Wostok 2209"
date: 2009-05-29T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok 2209","Wostok","2209","18 Jewels","CCCP","USSR","wrussia","Breguet hairspring"]
description: "Wostok 2209"
abstract: ""
preview_image: "wostok_2209-mini.jpg"
image: "Wostok_2209.jpg"
movementlistkey: "wostok"
caliberkey: "2209"
manufacturers: ["wostok"]
manufacturers_weight: 220900
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mnp/MNP_HAU.jpg"
    description: "MNP gents watch"
---
Lorem Ipsum