---
title: "Würthner"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["w"]
language: "en"
keywords: ["Würthner","Deisslingen","Germany","movement","movements"]
categories: ["movements","movements_w"]
movementlistkey: "wuerthner"
description: "Movements of the german manufacturer Würthner from Deisslingen"
abstract: "Movements of the german manufacturer Würthner from Deisslingen"
---
(Georg Würthner GmbH, Deisslingen, Germany)
{{< movementlist "wuerthner" >}}

{{< movementgallery "wuerthner" >}}