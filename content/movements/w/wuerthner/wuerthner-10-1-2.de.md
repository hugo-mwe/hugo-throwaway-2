---
title: "Würthner 10 1/2"
date: 2009-07-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Würthner 10 1/2","1 Jewels","Wuerthner","Dissingen","Schwenningen","OC Cal. R 1","Deutschland","Stiftanker"]
description: "Würthner 10 1/2"
abstract: ""
preview_image: "wuerthner_10_5-mini.jpg"
image: "Wuerthner_10_5.jpg"
movementlistkey: "wuerthner"
caliberkey: "10 1/2"
manufacturers: ["wuerthner"]
manufacturers_weight: 1012
categories: ["movements","movements_w","movements_w_wuerthner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Unknown_HAU_Wuerther.jpg"
    description: "anonyme Herrenuhr 'Made in Germany'"
  - image: "l/lings/Lings_21_Prix.jpg"
    description: "Lings 21 Prix Herrenuhr"
links: |
  * [Ranfft Uhren: Würthner 10 1/2](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Wuerthner_10_5)  (Die dort abgebildete Version ist neueren Datums)
---
Lorem Ipsum