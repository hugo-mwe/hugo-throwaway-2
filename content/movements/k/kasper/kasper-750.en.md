---
title: "Kasper 750"
date: 2017-10-25T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kasper 750","Kasper","pallet lever","17 Jewels","Monorex","17 Rubis","center second","pillar construction","rocking bar winding"]
description: "Kasper 750 - A better quality pillar movement from Germany"
abstract: "A better quality pallet lever movement with a pillar construction and an unusually constructed rocking bar winding system."
preview_image: "kasper_750-mini.jpg"
image: "Kasper_750.jpg"
movementlistkey: "kasper"
caliberkey: "750"
manufacturers: ["kasper"]
manufacturers_weight: 7500
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/anker/Anker_HU_Kasper_750.jpg"
    description: "Anker gents watch"
links: |
  * [Ranfft Uhren: Kasper 750](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Kasper_750) (probably the Kasper 700 is shown in the images)
---
Lorem Ipsum