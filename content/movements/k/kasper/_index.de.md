---
title: "Kasper"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["k"]
language: "de"
keywords: ["Kasper","Pforzheim","Uhrwerk","Uhrwerke","Emil Kasper"]
categories: ["movements","movements_k"]
movementlistkey: "kasper"
abstract: "Uhrwerke des deutschen Herstellers Kasper aus Pforzheim"
description: "Uhrwerke des deutschen Herstellers Kasper aus Pforzheim"
---
(Emil Kasper & Co., Pforzheim, Deutschland)
{{< movementlist "kasper" >}}

{{< movementgallery "kasper" >}}