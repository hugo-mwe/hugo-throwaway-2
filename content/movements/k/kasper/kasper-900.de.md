---
title: "Kasper 900"
date: 2017-03-10T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Palettenankerwerk","direkt angetriebener Zentralsekunde","Monorex-Stoßsicherung","Schraubenunruh","langem Rückerzeiger","Kupplungsaufzugs","Kasper 900","Kasper","900","Pforzheim","17 Jewels","Deutschland","17 Steine","21 Jewels","21 Steine"]
description: "Kasper 900 - Ein schönes 50er-Jahre-Handaufzugswerk. Detaillierte Beschreibung mit Bildern, Video und Datenblatt.  "
abstract: "Das Kasper 900 ist jetzt auch in der Ausführung mit 21 Steinen im Archiv vorhanden."
preview_image: "kasper_900-mini.jpg"
image: "Kasper_900.jpg"
movementlistkey: "kasper"
caliberkey: "900"
manufacturers: ["kasper"]
manufacturers_weight: 9000
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["true"]
links: |
  * [Ranfft Uhren: Kasper 900](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?03&ranfft&0&2uswk&Kasper_900&)
donation: "Das Werk in der Ausführung mit 21 Steinen ist, mitsamt Uhr, eine Spende von [Familie Ristow](/supporters/ristow/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
usagegallery: 
  - image: "e/ebp/EBP_Monorex_HAU_Kasper_900.jpg"
    description: "E.B.P. Monorex Herrenuhr"
  - image: "a/anker/Anker_HAU_Kasper_900.jpg"
    description: "Anker Herrenuhr"
---
Lorem Ipsum