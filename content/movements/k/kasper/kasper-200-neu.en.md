---
title: "Kasper 200 neu"
date: 2010-01-08T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kasper 200 neu","Kasper","200 neu","10 Jewels","Pforzheim","germany","cylinder escapement","new"]
description: "Kasper 200 neu - A 10 jewel form movement with 5 1/4 lignes and cylinder escapement. Detailed description with photos and data sheet."
abstract: ""
preview_image: "kasper_200_neu-mini.jpg"
image: "Kasper_200_neu.jpg"
movementlistkey: "kasper"
caliberkey: "200 neu"
manufacturers: ["kasper"]
manufacturers_weight: 2002
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kasper/Kasper_DAU_Kasper_200_neu.jpg"
    description: "Kasper ladies' watch"
---
Lorem Ipsum