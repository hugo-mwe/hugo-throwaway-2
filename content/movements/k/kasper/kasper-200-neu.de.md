---
title: "Kasper 200 neu"
date: 2010-01-08T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kasper 200 neu","Kasper","200 neu","10 Jewels","Pforzheim","10 Steine","Zylinderwerk","Zylinderhemmung"]
description: "Kasper 200 neu - Ein 10-steiniges Formwerk mit 5 1/4 Linien und Zylinderhemmung. Detaillierte Beschreibung mit Bildern und Datenblatt."
abstract: ""
preview_image: "kasper_200_neu-mini.jpg"
image: "Kasper_200_neu.jpg"
movementlistkey: "kasper"
caliberkey: "200 neu"
manufacturers: ["kasper"]
manufacturers_weight: 2002
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kasper/Kasper_DAU_Kasper_200_neu.jpg"
    description: "Kasper Damenuhr"
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum