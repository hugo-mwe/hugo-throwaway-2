---
title: "Kienzle 48/4"
date: 2009-09-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 48/4","Kienzle","48","Schwenningen","4 Jewels","4 Rubis","pin lever"]
description: "Kienzle 48/4"
abstract: ""
preview_image: "kienzle_48_4-mini.jpg"
image: "Kienzle_48_4.jpg"
movementlistkey: "kienzle"
caliberkey: "48/4"
manufacturers: ["kienzle"]
manufacturers_weight: 48400
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_5.jpg"
    description: "Kienzle gents watch"
---
Lorem Ipsum