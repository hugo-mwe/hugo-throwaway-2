---
title: "Kienzle 048/76"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 048/76 Kienzle","048","48","76","Schwenningen","7 Jewels","7 Steine","7 Rubis","Stiftanker"]
description: "Kienzle 048/76"
abstract: ""
preview_image: "kienzle_048_76-mini.jpg"
image: "Kienzle_048_76.jpg"
movementlistkey: "kienzle"
caliberkey: "048/76"
manufacturers: ["kienzle"]
manufacturers_weight: 48760
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_7_Jewels_2.jpg"
    description: "Kienzle Damenuhr"
---
Lorem Ipsum