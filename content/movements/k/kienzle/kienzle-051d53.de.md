---
title: "Kienzle 051d53"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle","Kienzle 051d53","Kienzle 051","51d53","051d53","Deutschland"]
description: "Kienzle 051/d53"
abstract: ""
preview_image: "kienzle_051d53-mini.jpg"
image: "Kienzle_051d53.jpg"
movementlistkey: "kienzle"
caliberkey: "051d53"
manufacturers: ["kienzle"]
manufacturers_weight: 51534
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum