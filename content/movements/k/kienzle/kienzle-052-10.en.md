---
title: "Kienzle 052/10"
date: 2009-12-04T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 052/10","Schwenningen","7 Jewels","7 Rubis","Germany","pin lever"]
description: "Kienzle 052/10"
abstract: ""
preview_image: "kienzle_052_10-mini.jpg"
image: "Kienzle_052_10.jpg"
movementlistkey: "kienzle"
caliberkey: "052/10"
manufacturers: ["kienzle"]
manufacturers_weight: 5210
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_7Jewels.jpg"
    description: "Kienzle gents watch"
---
Lorem Ipsum