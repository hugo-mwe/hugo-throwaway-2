---
title: "Kienzle"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["k"]
language: "en"
keywords: ["Kienzle","Schwenningen","Germany"]
categories: ["movements","movements_k"]
movementlistkey: "kienzle"
abstract: "Movements by Kienzle, Germany"
description: "Movements by Kienzle, Germany"
---
(Kienzle Uhrenfabriken GmbH, Schwenningen, Germany)
{{< movementlist "kienzle" >}}

{{< movementgallery "kienzle" >}}