---
title: "Kienzle 051N/53"
date: 2009-12-04T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 051N/53","0 Jewels","Schwenningen","0 Steine","Stiftanker"]
description: "Kienzle 051N/53"
abstract: ""
preview_image: "kienzle_051n53-mini.jpg"
image: "Kienzle_051N53.jpg"
movementlistkey: "kienzle"
caliberkey: "051N/53"
manufacturers: ["kienzle"]
manufacturers_weight: 51539
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_Kienzle_051N53.jpg"
    description: "Kienzle Herrenuhr"
links: |
  * [Ranfft Uhren: Kienzle 051N/53](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Kienzle_051N_53)
---
Lorem Ipsum