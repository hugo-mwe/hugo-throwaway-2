---
title: "Kaiser K48"
date: 2013-03-31T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kaiser K48","Kaiser 48","K48","Stiftanker","Villingen","5 Jewels","5 Steine","Zentralsekunde","indirekte Zentralsekunde"]
description: "Kaiser K48: Ein einfaches Stiftankerwerk mit indirekter Zentralsekunde aus Villingen"
abstract: "Ein einfaches Stiftankerwerk mit indirekter Zentralsekunde, das Anfang der 50 Jahre auf den Markt kam."
preview_image: "kaiser_k48-mini.jpg"
image: "Kaiser_K48.jpg"
movementlistkey: "kaiser"
caliberkey: "K48"
manufacturers: ["kaiser"]
manufacturers_weight: 48
categories: ["movements","movements_k","movements_k_kaiser"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kaiser/Kaiser_HAU_Kaiser_K48.jpg"
    description: "Kaiser Herrenuhr"
---
Lorem Ipsum