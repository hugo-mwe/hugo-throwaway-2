---
title: "Kaiser K9"
date: 2018-03-25T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kaiser K9","Kaiser 9","K9","Stiftanker","Villingen","1 Jewels","1 Steine","1 Rubis"]
description: "Kaiser K9: Ein einfaches Stiftankerwerk aus Villingen"
abstract: "Ein einfaches Stiftankerwerk um 1950"
preview_image: "kaiser_K9-mini.jpg"
image: "Kaiser_K9.jpg"
movementlistkey: "kaiser"
caliberkey: "K9"
manufacturers: ["kaiser"]
manufacturers_weight: 9
categories: ["movements","movements_k","movements_k_kaiser"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum