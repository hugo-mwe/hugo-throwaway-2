---
title: "Kaiser "
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["k"]
language: "en"
keywords: ["Kaiser","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_k"]
movementlistkey: "kaiser"
description: "Uhrwerke des deutschen Herstellers Kaiser"
abstract: "Uhrwerke des deutschen Herstellers Kaiser"
---
(Josef Kaiser GmbH, Villingen, Germany)
{{< movementlist "kaiser" >}}

{{< movementgallery "kaiser" >}}