---
title: "Kaiser K48"
date: 2013-03-31T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kaiser K48","Kaiser 48","K48","pin lever","Germany","Villingen","5 Jewels","5 Rubis","center second"]
description: "Kaiser K48: A simple german pin lever movement with indirectly driven center second"
abstract: "A simple pin lever movement with indirectly driven center second, which was launched in the early 1950ies."
preview_image: "kaiser_k48-mini.jpg"
image: "Kaiser_K48.jpg"
movementlistkey: "kaiser"
caliberkey: "K48"
manufacturers: ["kaiser"]
manufacturers_weight: 48
categories: ["movements","movements_k","movements_k_kaiser_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kaiser/Kaiser_HAU_Kaiser_K48.jpg"
    description: "Kaiser mens' watch"
---
Lorem Ipsum