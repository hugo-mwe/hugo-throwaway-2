---
title: "Newmark 10 1/2'''"
date: 2014-10-17T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Newmark 10 1/2","Newmark","manual wind","5 Jewels","pin lever"]
description: "Newmark 10 1/2 - a very rare manufactury pin lever movement from the UK"
abstract: "In the 1950ies there were still movements produced in the UK, the rare pin lever caliber Newmark 10 1/2''' is one of them."
preview_image: "newmark_10_1_2-mini.jpg"
image: "Newmark_10_1_2.jpg"
movementlistkey: "newmark"
caliberkey: "10 1/2'''"
manufacturers: ["newmark"]
manufacturers_weight: 1012
categories: ["movements","movements_n","movements_n_newmark_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  There's an article about Newmark in the [WatchUSeek-Forum](http://forums.watchuseek.com/f11/newmark-watch-co-info-please-612406.html).
usagegallery: 
  - image: "n/newmark/Newmark_Crescent_Newmark_10_1_2.jpg"
    description: "Newmark Crescent cocktail watch"
---
Lorem Ipsum