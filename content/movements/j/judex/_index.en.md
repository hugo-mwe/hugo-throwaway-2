---
title: "Judex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "en"
keywords: ["Judex","France","french"]
categories: ["movements","movements_j"]
movementlistkey: "judex"
abstract: "Movements of the french manufacturer Judex"
description: "Movements of the french manufacturer Judex"
---
(Judex, France)
{{< movementlist "judex" >}}

{{< movementgallery "judex" >}}