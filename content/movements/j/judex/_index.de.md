---
title: "Judex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "de"
keywords: ["Judex","Frankreich"]
categories: ["movements","movements_j"]
movementlistkey: "judex"
description: "Uhrwerke des französischen Herstellers Judex"
abstract: "Uhrwerke des französischen Herstellers Judex"
---
(Judex, Frankreich)
{{< movementlist "judex" >}}

{{< movementgallery "judex" >}}