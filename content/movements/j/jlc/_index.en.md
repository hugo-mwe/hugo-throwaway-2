---
title: "JLC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "en"
keywords: ["JLC","Jaeger-LeCoultre","Le Sentier","Jaeger","LeCoultre","Swiss","Switzerland"]
categories: ["movements","movements_j"]
movementlistkey: "jlc"
description: "Movements of the swiss manufacturer Jaeger-LeCoultre (JLC) from Le Sentier."
abstract: "Movements of the swiss manufacturer Jaeger-LeCoultre (JLC) from Le Sentier."
---
(Jaeger-LeCoultre, Le Sentier, Switzerland)
{{< movementlist "jlc" >}}

{{< movementgallery "jlc" >}}