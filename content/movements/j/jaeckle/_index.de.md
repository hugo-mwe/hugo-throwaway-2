---
title: "Jäckle"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "de"
keywords: ["Jäckle","Deutschland","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_j"]
movementlistkey: "jaeckle"
abstract: "Uhrwerke des deutschen Herstellers Jäckle"
description: "Uhrwerke des deutschen Herstellers Jäckle"
---
(Jäckle, Deutschland)
{{< movementlist "jaeckle" >}}

{{< movementgallery "jaeckle" >}}