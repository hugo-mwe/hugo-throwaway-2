---
title: "Jäckle"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "en"
keywords: ["Jäckle","movement","movements","Germany"]
categories: ["movements","movements_j"]
movementlistkey: "jaeckle"
abstract: "Movements of the german manufacturer Jäckle"
description: "Movements of the german manufacturer Jäckle"
---
(Jäckle, Germany)
{{< movementlist "jaeckle" >}}

{{< movementgallery "jaeckle" >}}