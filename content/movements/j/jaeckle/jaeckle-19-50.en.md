---
title: "Jäckle 19/50 / Intex 1055"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Intex 1055","Jäckle 19/50","Intex","Jäckle","Germany","4 Jewels","4 Rubis","pin lever"]
description: "Jäckle 19/50 / Intex 1055"
abstract: ""
preview_image: "jaeckle_19_50-mini.jpg"
image: "Jaeckle_19_50.jpg"
movementlistkey: "jaeckle"
caliberkey: "19/50"
manufacturers: ["jaeckle"]
manufacturers_weight: 1950
categories: ["movements","movements_j","movements_j_jaeckle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/ecly/Ecly_Anker.jpg"
    description: "Ecly Anker gents watch"
---
Lorem Ipsum