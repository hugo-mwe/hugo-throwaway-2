---
title: "Jeambrun 23D"
date: 2011-01-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Glucydur screw balance","Incabloc","Ruby-Shock","Jeambrun 23D","Jeambrun","23D","17 Jewels","23 Jewels","Maîche","france"]
description: "Jeambrun 23D - A fair quality handwound movement, made in France. Detailed description with photos, macro image, data sheet and video."
abstract: ""
preview_image: "jeambrun_23d-mini.jpg"
image: "Jeambrun_23D.jpg"
movementlistkey: "jeambrun"
caliberkey: "23D"
manufacturers: ["jeambrun"]
manufacturers_weight: 23
categories: ["movements","movements_j","movements_j_jeambrun_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/cleva/Cleva_HAU_Jeambrun_23D.jpg"
    description: "Cleva gents watch"
  - image: "s/surex/Surex_Nautika_HAU_Jeambrun_23D.jpg"
    description: "Surex Nautika gents watch"
links: |
  * [Ranfft Uhren: Jeambrun 23D](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Jeambrun_23D)
---
Lorem Ipsum