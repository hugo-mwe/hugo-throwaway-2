---
title: "Jeambrun 23D"
date: 2011-01-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Glucydur-Schraubenunruh","Incabloc","Ruby-Shock","Jeambrun 23D","Jeambrun","23D","17 Jewels","23 Jewels","Maîche","Frankreich","17 Steine","23 Steine"]
description: "Jeambrun 23D - Ein gängiges Handaufzugswerk besserer Qualität aus Frankreich. Detaillierte Beschreibung mit Bildern, Makro-Aufnahme, Datenblatt und Video."
abstract: ""
preview_image: "jeambrun_23d-mini.jpg"
image: "Jeambrun_23D.jpg"
movementlistkey: "jeambrun"
caliberkey: "23D"
manufacturers: ["jeambrun"]
manufacturers_weight: 23
categories: ["movements","movements_j","movements_j_jeambrun"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Jeambrun 23D](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Jeambrun_23D)
usagegallery: 
  - image: "c/cleva/Cleva_HAU_Jeambrun_23D.jpg"
    description: "Cleva Herrenuhr"
  - image: "s/surex/Surex_Nautika_HAU_Jeambrun_23D.jpg"
    description: "Surex Nautika Herrenuhr"
---
Lorem Ipsum