---
title: "Junghans J98/3 (698.71)"
date: 2009-05-01T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans J 98","Junghans","J 98","Schramberg","17 Steine","Trilastic","Trishock","Germany"]
description: "Junghans J 98"
abstract: ""
preview_image: "junghans_j_98-mini.jpg"
image: "Junghans_J_98.jpg"
movementlistkey: "junghans"
caliberkey: "J98/3"
manufacturers: ["junghans"]
manufacturers_weight: 69871
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Junghans J98 (Junghans 698.70)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Junghans_698_71&)
  * [Junghans Vintage: Junghans J98/3; Junghans 698.71](https://junghans-vintage.de/en/uhren/junghans-armbanduhr-kaliber/kaliberfamilie-junghans-698-junghans-j98/junghans-j98/3-junghans-69871-16-jewels/junghans-j98/3-junghans-69871.html)
usagegallery: 
  - image: "j/junghans/Junghans_HAU_2.jpg"
    description: "Junghans gents watch"
---
Lorem Ipsum