---
title: "Junghans J93 (693.81)"
date: 2010-05-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["dezentraler Sekunde","zweischenkligen Ringunruh","Junghans 693.81","Wippenaufzugs","Sperrklinke","Junghans J93","Junghans","J93","15 Jewels","Schramberg","693.81","Deutschland","Schwarzwald","15 Steine","Handaufzug"]
description: "Junghans J93 - Ein gutes Herren-Handaufzugswerk mit 15 Steinen. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "junghans_j_93-mini.jpg"
image: "Junghans_J_93.jpg"
movementlistkey: "junghans"
caliberkey: "J93"
manufacturers: ["junghans"]
manufacturers_weight: 69381
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Junghans 693.81 (J93)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Junghans_693_81)
labor: |
  <p>Das vorliegende Werk bestacht (und besticht immer noch) durch seinen extrem starken Geruch, der an einen Schiffsdiesel erinnert. Aus diesem Grund wurde es auch komplett gereinigt und geölt; der Geruch aber blieb trotzdem.</p><p>Der technische Zustand ist gemessem am Alter und am sehr intensiven Gebrauch dieser Uhr durchaus gut.</p>
usagegallery: 
  - image: "z/zeda/Zeda_HAU_Junghans_J_93.jpg"
    description: "Zeda Herrenuhr"
timegrapher_old: 
  description: |
    Auf der Zeitwaage (hier in doppelt genauer Auflösung) konnte sich der gute Zustand leider nicht bemerkbar machen; das Werk zeigt in jeder Lage unterschiedlich starke Abweichungen, die von starkem Nachgang (2 1/2 Minuten pro Tag) bis zu gemäßigtem Vorgang (40 Sekunden pro Tag) reichen.
  images:
    ZO: Zeitwaage_Junghans_J_93_ZO.jpg
    ZU: Zeitwaage_Junghans_J_93_ZU.jpg
    3O: Zeitwaage_Junghans_J_93_3O.jpg
    6O: Zeitwaage_Junghans_J_93_6O.jpg
    9O: Zeitwaage_Junghans_J_93_9O.jpg
    12O: Zeitwaage_Junghans_J_93_12O.jpg
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum