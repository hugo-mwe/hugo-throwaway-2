---
title: "Junghans 623.20"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans","Junghans 623","Junghans 623.20","623","623.20","17 jewels","17","jewels","black wood forest","Schramberg"]
description: "Junghans 623.20"
abstract: ""
preview_image: "junghans_623_20-mini.jpg"
image: "Junghans_623_20.jpg"
movementlistkey: "junghans"
caliberkey: "623.20"
manufacturers: ["junghans"]
manufacturers_weight: 62320
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum