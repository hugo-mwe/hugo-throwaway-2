---
title: "Junghans 674"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans","674","Junghans 674","17","Rubis","Jewels","Germany","Black forest"]
description: "Junghans 674"
abstract: ""
preview_image: "junghans_674-mini.jpg"
image: "Junghans_674.jpg"
movementlistkey: "junghans"
caliberkey: "674"
manufacturers: ["junghans"]
manufacturers_weight: 67400
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum