---
title: "Junghans 687.00 (J87/10)"
date: 2014-09-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans","Junghans 687","J687","687.00","87/10","17 Jewels","17 Rubis","yoke winding","handwind","center second"]
description: "Junghans 687 - a large handwound movement from the 1960ies, well equipped with pallet lever escapement, 17 jewels and yoke winding system."
abstract: "A large (11 1/2 lignes) mens' handwound movement from the 1960ies, which was well equipped with pallet lever escapement, 17 jewels and a yoke winding system."
preview_image: "junghans_687-mini.jpg"
image: "Junghans_687.jpg"
movementlistkey: "junghans"
caliberkey: "687.00"
manufacturers: ["junghans"]
manufacturers_weight: 68700
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  images:
    ZO: Zeitwaage_Junghans_687_ZO.jpg
    ZU: Zeitwaage_Junghans_687_ZU.jpg
    3O: Zeitwaage_Junghans_687_3O.jpg
    6O: Zeitwaage_Junghans_687_6O.jpg
    9O: Zeitwaage_Junghans_687_9O.jpg
    12O: Zeitwaage_Junghans_687_12O.jpg
  values:
    ZO: "+10"
    ZU: "-8"
    3O: "+-0"
    6O: "-30"
    9O: "-10"
    12O: "+20"
usagegallery: 
  - image: "j/junghans/Junghans_HAU_Junghans_687.jpg"
    description: "Junghans gents watch (1)"
  - image: "j/junghans/Junghans_HAU_2_Junghans_687.jpg"
    description: "Junghans gents watch (2)"
donation: "This movement is, in watch (1) a donation from [G. Müller](/supporters/g-mueller/), in watch (2) an [anonymous donation](/supporters/anonymous/). Thank you very much for the support of the movement archive!"
labor: |
  The watch with the Junghans 687 movement came into the lab in good condition, so besides a minor adjustment, no further service was made.
---
Lorem Ipsum