---
title: "Junghans J93S10 (693.02)"
date: 2010-10-09T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["minute wheel cock","directly driven center second","regulator arm","Junghans J93S10","Junghans","J93S10","693.02","17 Jewels","Schramberg","Schwarzwald","german","germany","manual wind"]
description: "Junghans J93S10 - An often used movement with 17 jewels and date indication. Detailed description with photos and data sheet."
abstract: ""
preview_image: "junghans_j_93s10-mini.jpg"
image: "Junghans_J_93S10.jpg"
movementlistkey: "junghans"
caliberkey: "J93S10"
manufacturers: ["junghans"]
manufacturers_weight: 69302
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/junghans/Junghans_Trilastic_HAU_Junghans_J93_S10.jpg"
    description: "Junghans Trilastic gents watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: Junghans 693.02 (J93S10)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Junghans_693_02)</p><p>This movement was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum