---
title: "Junghans 625.20"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans","Junghans 625. Junghans 625.20","625","625.20","25 Steine","25","Steine","Schwarzwald","Schramberg"]
description: "Junghans 625.20"
abstract: ""
preview_image: "junghans_625_20-mini.jpg"
image: "Junghans_625_20.jpg"
movementlistkey: "junghans"
caliberkey: "625.20"
manufacturers: ["junghans"]
manufacturers_weight: 62520
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum