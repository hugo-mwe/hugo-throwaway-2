---
title: "PUW 49"
date: 2009-06-26T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 49","PUW","49","17 Jewels","17 Steine","Formwerk","Deutschland"]
description: "PUW 49"
abstract: ""
preview_image: "puw_49-mini.jpg"
image: "PUW_49.jpg"
movementlistkey: "puw"
caliberkey: "49"
manufacturers: ["puw"]
manufacturers_weight: 490
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/ruka/Ruka_DAU.jpg"
    description: "Ruka Damenuhr"
  - image: "p/para/Para_DAU.jpg"
    description: "Para Damenuhr"
  - image: "p/porta/Porta_DAU.jpg"
    description: "Porta Damenuhr"
---
Lorem Ipsum