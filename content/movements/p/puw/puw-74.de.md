---
title: "PUW 74"
date: 2014-10-12T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 74","Pforzheimer Uhrenrohwerke","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Formwerk","17 Steine","Deutschland"]
description: "PUW 74"
abstract: " Ein typisches Damenuhren-Formwerk aus den 60er Jahren "
preview_image: "puw_74-mini.jpg"
image: "PUW_74.jpg"
movementlistkey: "puw"
caliberkey: "74"
manufacturers: ["puw"]
manufacturers_weight: 740
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [G.Müller](/supporters/g-mueller/) für die Spende dieses Werks samt Uhr!"
usagegallery: 
  - image: "d/dugena/Dugena_DAU_PUW_74.jpg"
    description: "Dugena Damenuhr"
---
Lorem Ipsum