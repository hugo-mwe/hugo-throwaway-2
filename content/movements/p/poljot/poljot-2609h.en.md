---
title: "Poljot 2609H"
date: 2017-09-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Poljot 2609H","Poljot","NOVET","2609H","17 Jewels","17 Rubis","manual wind","shock protection"]
description: "Poljot 2609H - an often used russian manual wind movement with 17 jewels and center second"
abstract: "An often used russian manual wind movement with 17 jewels."
preview_image: "poljot_2609H-mini.jpg"
image: "Poljot_2609H.jpg"
movementlistkey: "poljot"
caliberkey: "2609H"
manufacturers: ["poljot"]
manufacturers_weight: 260901
categories: ["movements","movements_p","movements_p_poljot_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement was donated by [Hans](/supporters/hans/). Thank you very much for the support of the movement archive!"
timegrapher_old: 
  rates:
    ZO: "+20"
    ZU: "+15"
    3O: "+17"
    6O: "+20"
    9O: "+11"
    12O: "+24"
  amplitudes:
    ZO: "255"
    ZU: "240"
    3O: "203"
    6O: "232"
    9O: "224"
    12O: "223"
  beaterrors:
    ZO: "0.3"
    ZU: "0.2"
    3O: "0.2"
    6O: "0.3"
    9O: "0.3"
    12O: "0.2"
labor: |
  The movement shown here came as NOS into the lab and directly into the timegrapher. No revision and no adjustments were done.
---
Lorem Ipsum