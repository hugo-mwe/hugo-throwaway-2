---
title: "Poljot 2614.2H"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Poljot 2614.2H","Poljot 2614","Poljot","2614.2H","2614","17 Jewels","Rußland","UdSSR","Sowjetunion"]
description: "Poljot 2614.2H"
abstract: ""
preview_image: "poljot_2614_2h-mini.jpg"
image: "Poljot_2614_2H.jpg"
movementlistkey: "poljot"
caliberkey: "2614.2H"
manufacturers: ["poljot"]
manufacturers_weight: 261421
categories: ["movements","movements_p","movements_p_poljot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_100_HAU_Poljot_Werk.jpg"
    description: "Anker 100 Herrenuhr"
  - image: "a/anker/Anker_100_HAU_2.jpg"
    description: "Anker 100 Herrenuhr"
---
Lorem Ipsum