---
title: "Poljot 2616.1H"
date: 2018-03-17T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Poljot","Poljot 2616.1H","2616.1","30 Jewels","30 Rubis","Automatic","30 Steine","Meister-Anker","Quelle"]
description: "Poljot 2616.1H - Ein sehr aufwändig gebautes und zahlreichen überraschenden Details versehenes russisches Automaticwerk."
abstract: "Mit 30 funktionalen Steinen ist dieses russische Automaticwerk ausgestattet, das mit einigen überraschenden Details aufwarten kann."
preview_image: "poljot_2616_1h-mini.jpg"
image: "Poljot_2616_1H.jpg"
movementlistkey: "poljot"
caliberkey: "2616.1H"
manufacturers: ["poljot"]
manufacturers_weight: 261611
categories: ["movements","movements_p","movements_p_poljot"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam verharzt und verdreckt, sowie mit fehlendem Halteplättchen des Rotors und mit kariösem Stundenrad ins Labor. Es wurde zwar gereinigt und geölt, die defekten bzw. fehlenden Teile konnten aber nicht ersetzt werden. Dennoch konnte das Werk zumindest wieder soweit instand gesetzt werden, daß es auf der Zeitwaage gemessen werden konnte.
timegrapher_old: 
  rates:
    ZO: "+1"
    ZU: "+6"
    3O: "-15"
    6O: "+20"
    9O: "-20"
    12O: "-11"
  amplitudes:
    ZO: "311"
    ZU: "292"
    3O: "196"
    6O: "183"
    9O: "215"
    12O: "226"
  beaterrors:
    ZO: "0.1"
    ZU: "0.0"
    3O: "0.3"
    6O: "0.0"
    9O: "0.2"
    12O: "0.2"
usagegallery: 
  - image: "m/meister-anker/Meister-Anker_HU_Poljot_2616_H1.jpg"
    description: "Meister Anker Automatic Herrenuhr"
---
Lorem Ipsum