---
title: "Parrenin 1901"
date: 2010-01-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["11 1/2 Linien","7","9mm","Parrenin 1901","Parrenin","1901","25 Jewels","Automatic","Frankreich","25 Steine","Automatik"]
description: "Parrenin 1901 - Ein seltenes, sehr hoch bauendes, 11 1/2 liniges Automaticwerk aus Frankreich. Detaillierte Beschreibung mit Bildern, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "parrenin_1901-mini.jpg"
image: "Parrenin_1901.jpg"
movementlistkey: "parrenin"
caliberkey: "1901"
manufacturers: ["parrenin"]
manufacturers_weight: 19010
categories: ["movements","movements_p","movements_p_parrenin"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_Automatic_HAU_Zifferblatt_Parrenin_1901.jpg"
    description: "Anker Automatic Herrenuhr  (nur Zifferblatt)"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Parrenin_1901_ZO.jpg
    ZU: Zeitwaage_Parrenin_1901_ZU.jpg
    3O: Zeitwaage_Parrenin_1901_3O.jpg
    6O: Zeitwaage_Parrenin_1901_6O.jpg
    9O: Zeitwaage_Parrenin_1901_9O.jpg
    12O: Zeitwaage_Parrenin_1901_12O.jpg
labor: |
  Das getestete Exemplar kam als loses Werk in die Werkstatt, daher ist davon auszugehen, daß es sich nicht mehr im besten Zustand befand. Das Zeitwaagen-Protokoll belegt dies auch deutlich:
---
Lorem Ipsum