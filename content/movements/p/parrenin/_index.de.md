---
title: "Parrenin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: ["Parrenin","Frankreich","France","Villers-le-Lac"]
categories: ["movements","movements_p"]
movementlistkey: "parrenin"
description: "Uhrwerke des französischen Herstellers Parrrenin aus Villers-le-Lac"
abstract: "Uhrwerke des französischen Herstellers Parrrenin aus Villers-le-Lac"
---
(Parrenin, Villers-le-Lac, Frankreich)
{{< movementlist "parrenin" >}}

{{< movementgallery "parrenin" >}}