---
title: "Parrenin 1901"
date: 2010-01-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["11 1/2 lignes","7.9mm","Parrenin 1901","Parrenin","1901","25 Jewels","Automatic","france","selfwinding"]
description: "Parrenin 1901 - A rare, thick, 11 1/2 ligne selfwinding movement from France. Detailed description with photos, data sheet and timegrapher protocol."
abstract: ""
preview_image: "parrenin_1901-mini.jpg"
image: "Parrenin_1901.jpg"
movementlistkey: "parrenin"
caliberkey: "1901"
manufacturers: ["parrenin"]
manufacturers_weight: 19010
categories: ["movements","movements_p","movements_p_parrenin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The tested specimen here came as loose movement, so it can be assumed, that it is not in a perfect condition. The timegrapher protol proves this:
timegrapher_old: 
  images:
    ZO: Zeitwaage_Parrenin_1901_ZO.jpg
    ZU: Zeitwaage_Parrenin_1901_ZU.jpg
    3O: Zeitwaage_Parrenin_1901_3O.jpg
    6O: Zeitwaage_Parrenin_1901_6O.jpg
    9O: Zeitwaage_Parrenin_1901_9O.jpg
    12O: Zeitwaage_Parrenin_1901_12O.jpg
usagegallery: 
  - image: "a/anker/Anker_Automatic_HAU_Zifferblatt_Parrenin_1901.jpg"
    description: "Anker Automatic gents watch  (dial only)"
---
Lorem Ipsum