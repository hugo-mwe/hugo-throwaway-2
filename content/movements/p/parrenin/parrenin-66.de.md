---
title: "Parrenin 66"
date: 2017-04-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Parrenin 66","Emes 18a","Stiftanker","kleine Sekunde","5 Jewels","5 Rubis","RUFA Anti-Shock"]
description: "Parrenin 66 / Emes 18a: Ein Stiftankerwerk aus Frankreich"
abstract: "Ein französisches Stiftankerwerk mit Unruhbrücke und gestürztem Stiftanker."
preview_image: "parrenin_66-mini.jpg"
image: "Parrenin_66.jpg"
movementlistkey: "parrenin"
caliberkey: "66"
manufacturers: ["parrenin"]
manufacturers_weight: 660
categories: ["movements","movements_p","movements_p_parrenin"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk in der Ausführung als Emes 18a wurde von [Christoph Reiner](/supporters/emesuhren/) gespendet. Ganz herzlichen Dank dafür!"
usagegallery: 
  - image: "e/emes/Emes_DAU_Emes_18a.jpg"
    description: "Emes Damenuhr"
---
Lorem Ipsum