---
title: "Phenix"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: ["Peseux","Switzerland"]
categories: ["movements","movements_p"]
movementlistkey: "phenix"
description: "Movements from the swiss manufacturer Phenix from Pruntut (Porrentruy) in the Jura."
abstract: "Movements from the swiss manufacturer Phenix from Pruntut (Porrentruy) in the Jura."
---
(Phenix Watch Co., Pruntrut (Porrentruy), Switzerland)
{{< movementlist "phenix" >}}

{{< movementgallery "phenix" >}}