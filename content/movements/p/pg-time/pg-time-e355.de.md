---
title: "PG Time E355"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PG Time E355","PG Time Ltd.","PG Time","E355","1 Jewel","Mondphase"]
description: "PG Time E355"
abstract: ""
preview_image: "pg_time_e355-mini.jpg"
image: "PG_Time_E355.jpg"
movementlistkey: "pg-time"
caliberkey: "E355"
manufacturers: ["pg-time"]
manufacturers_weight: 355
categories: ["movements","movements_p","movements_p_pg_time"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/Q_Mondphase.jpg"
    description: "O (oder Q) Herrenuhr mit Pseudo-Indikationen"
---
Lorem Ipsum