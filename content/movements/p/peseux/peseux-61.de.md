---
title: "Peseux 61"
date: 2015-12-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Peseux 61","Schraubenunruh","15 Jewels","Incabloc","Incabloc alt","Incassable"]
description: "Peseux 61"
abstract: "Ein recht frühes Handaufzugswerk mit 8 3/4 Linien Durchmesser und der alten Incabloc-Stoßsicherung"
preview_image: "peseux_61-mini.jpg"
image: "Peseux_61.jpg"
movementlistkey: "peseux"
caliberkey: "61"
manufacturers: ["peseux"]
manufacturers_weight: 61
categories: ["movements","movements_p","movements_p_peseux"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Uhr ist eine Spende von [Erwin](/supporters/erwin/). Ganz herzlichen Dank dafür!"
usagegallery: 
  - image: "f/fanex/Fanex_Incassable_DAU_Peseux_61.jpg"
    description: "Fanex Incassable Damenuhr"
---
Lorem Ipsum