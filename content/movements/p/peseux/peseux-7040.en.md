---
title: "Peseux 7040"
date: 2014-02-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux 7040","Peseux","7040","ETA 7001","ETA","7001 skeleton","skeleton watch"]
description: "Peseux 7040"
abstract: ""
preview_image: "peseux_7040-mini.jpg"
image: "Peseux_7040_hinten.jpg"
movementlistkey: "peseux"
caliberkey: "7040"
manufacturers: ["peseux"]
manufacturers_weight: 7040
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum