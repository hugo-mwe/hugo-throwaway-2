---
title: "Peseux 320"
date: 2009-10-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Peseux 320","Peseux","320","17 Jewels","Schweiz","17 Steine"]
description: "Peseux 320"
abstract: ""
preview_image: "peseux_320-mini.jpg"
image: "Peseux_320.jpg"
movementlistkey: "peseux"
caliberkey: "320"
manufacturers: ["peseux"]
manufacturers_weight: 320
categories: ["movements","movements_p","movements_p_peseux"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Peseux 320](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Peseux_320)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende des Werks in der kupferfarbigen moderneren Ausführung mit Stoßsicherung!</p>
usagegallery: 
  - image: "e/eldor/Eldor_HAU.jpg"
    description: "Eldor Herrenuhr"
  - image: "p/precimax/Precimax_HAU_3_Zifferblatt.jpg"
    description: "Precimax Herrenuhr  (Nur Zifferblatt)"
---
Lorem Ipsum