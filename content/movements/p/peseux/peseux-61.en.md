---
title: "Peseux 61"
date: 2015-12-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux 61","screw balance","15 Jewels","Incabloc","Incabloc old","Incassable","15 Rubis"]
description: "Peseux 61"
abstract: "A quite early manual wind movement with a diameter of 8 3/4 lignes and the old Incabloc shock protection."
preview_image: "peseux_61-mini.jpg"
image: "Peseux_61.jpg"
movementlistkey: "peseux"
caliberkey: "61"
manufacturers: ["peseux"]
manufacturers_weight: 61
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement and watch is a kind donation of [Erwin](/supporters/erwin/). Thank you very much!"
usagegallery: 
  - image: "f/fanex/Fanex_Incassable_DAU_Peseux_61.jpg"
    description: "Fanex Incassable ladies' watch"
---
Lorem Ipsum