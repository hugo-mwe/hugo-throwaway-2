---
title: "Peseux 120"
date: 2017-01-02T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux 120","form movement","double crown wheel","screw balance"]
description: "Peseux 120"
abstract: "A rare ladies' watch form movement with double crown wheel."
preview_image: "peseux_120-mini.jpg"
image: "Peseux_120.jpg"
movementlistkey: "peseux"
caliberkey: "120"
manufacturers: ["peseux"]
manufacturers_weight: 120
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    The specimen shown here was heavily worn, when it came into the archive. Since it still showed fail rates, no revision was made.
  rates:
    ZO: "-45"
    ZU: "+65"
    3O: "-73"
    6O: "-88"
    9O: "-109"
    12O: "-136"
  amplitudes:
    ZO: ""
    ZU: "282"
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: "1.6"
    3O: ""
    6O: ""
    9O: ""
    12O: ""
usagegallery: 
  - image: "b/blumus/Blumus_DAU_Peseux_120.jpg"
    description: "Blumus ladies' watch"
donation: "This movement and watch are a kind donation by [Erwin](/supporters/erwin/). Thank you very much for supporting the movement archive!"
---
Lorem Ipsum