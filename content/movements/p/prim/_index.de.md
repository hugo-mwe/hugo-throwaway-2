---
title: "Prim"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "prim"
abstract: "Uhrwerke des tschechischen Herstellers Prim aus Nové Město nad Metuj"
description: "Uhrwerke des tschechischen Herstellers Prim aus Nové Město nad Metuj"
---
(Prim, Nové Město nad Metuj, Tschechien)
{{< movementlist "prim" >}}

{{< movementgallery "prim" >}}