---
title: "Bifora 85"
date: 2018-01-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 85","manual wind","15 Jewels","15 Rubis","pallet lever","screw balance","decentral second"]
description: "Bifora 85 - A pretty old and beautiful pallet lever movement from Schwäbisch Gmünd in Germany"
abstract: "A very beautiful Bifora windup movement."
preview_image: "bifora_85_mini.jpg"
image: "Bifora_85.jpg"
movementlistkey: "bifora"
caliberkey: "81"
manufacturers: ["bifora"]
manufacturers_weight: 8100
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_DU_Bifora_85.jpg"
    description: "Bifora ladies' watch"
  - image: "b/bifora/Bifora_DU_2_Bifora_85.jpg"
    description: "Bifora ladies' watch"
donation: "This movement and watch was kindly donated by [Erwin](/supporters/erwin/). Thank you very much for the support of the archive!"
labor: |
  The specimen shown here was resinified and has a loose balance cock, so it got a full cleaning treatment. Afterwards, it became clear, that the hairspring lost its shape and kind of falls onto the legs of the balance. In addition to the very compact construction around the balance, it is impossible for a non-watchmaker to fix this.
---
Lorem Ipsum