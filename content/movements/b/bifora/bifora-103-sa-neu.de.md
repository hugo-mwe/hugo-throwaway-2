---
title: "Bifora 103 SA neu"
date: 2017-10-07T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 103 SA","Bifora","Bimag","Bidlingmaier","Automatic","22 Jewels","kleine Sekunde","Rotor"]
description: "Bifora 103 SA (neu) - Das früheste deutsche Automaticwerk in seiner ersten und einzigen Überarbeitung"
abstract: "Das früheste deutsche Automaticwerk, in seiner ersten und einzigen Überarbeitung"
preview_image: "bifora_103_sa_neu-mini.jpg"
image: "Bifora_103_SA_neu.jpg"
movementlistkey: "bifora"
caliberkey: "103 SA neu"
manufacturers: ["bifora"]
manufacturers_weight: 10351
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Automatic_Bifora_103_SA_neu.jpg"
    description: "Bifora Automatic Herrenuhr"
---
Lorem Ipsum