---
title: "Bifora 734"
date: 2017-11-02T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 734","Handaufzug","15 Jewels","15 Rubis","Palettenanker","7 3/4 Linien","Schraubenunruh","Zentralsekunde"]
description: "Bifora 734 - Ein sehr selten zu findendes Handaufzugswerk von Bidlingmaier aus Schwäbisch Gmünd"
abstract: "Eine in jeder Hinsicht kleine Rarität ist das Bifora 734, ein Handaufzugswerk mit Zentralsekunde aus Schwäbisch Gmünd."
preview_image: "bifora_734-mini.jpg"
image: "Bifora_734.jpg"
movementlistkey: "bifora"
caliberkey: "734"
manufacturers: ["bifora"]
manufacturers_weight: 73400
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum