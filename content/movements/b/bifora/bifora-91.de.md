---
title: "Bifora 91"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 91","Bifora 91","Bifora","Bidlingmaier","Bishock","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","17","Steine","Jewels"]
description: "Bifora 91"
abstract: ""
preview_image: "bifora_91-mini.jpg"
image: "Bifora_91.jpg"
movementlistkey: "bifora"
caliberkey: "91"
manufacturers: ["bifora"]
manufacturers_weight: 9100
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Bifora 91](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Bifora_91_0SC)
usagegallery: 
  - image: "a/ankra/Ankra_HAU_2.jpg"
    description: "Ankra Herrenuhr"
---
Lorem Ipsum