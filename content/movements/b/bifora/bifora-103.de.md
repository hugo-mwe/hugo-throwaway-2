---
title: "Bifora 103"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 103","Bifora","Bidlingmaier","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","16","Steine","Jewels","Schraubenunruh"]
description: "Bifora 103"
abstract: ""
preview_image: "bifora_103-mini.jpg"
image: "Bifora_103.jpg"
movementlistkey: "bifora"
caliberkey: "103"
manufacturers: ["bifora"]
manufacturers_weight: 10300
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum