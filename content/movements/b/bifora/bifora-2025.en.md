---
title: "Bifora 2025"
date: 2015-03-08T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 2025","Bifora","Bidlingmaier FormAnker","screw balance","7 Jewels","7 Rubis"]
description: "Bifora 2025 - the first form movement of Bidlingmaier (Bifora)"
abstract: "The first form lever movement by Bidlingmaier, probably the first german form lever movement at all."
preview_image: "bifora_2025-mini.jpg"
image: "Bifora_2025.jpg"
movementlistkey: "bifora"
caliberkey: "2025"
manufacturers: ["bifora"]
manufacturers_weight: 202500
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum