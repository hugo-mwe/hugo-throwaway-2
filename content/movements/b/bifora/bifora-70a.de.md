---
title: "Bifora 70A"
date: 2017-10-28T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 70A","Automatic","7 1/4 Linien","Damenuhr"]
description: "Bifora 70A - Das kleinste Automaticwerk von Bidlingmaier aus Schwäbisch Gmünd"
abstract: "Das kleinste Damenuhren-Automaticwerk von Bifora hatte einen Durchmesser von gerade einmal 16mm!"
preview_image: "bifora_70a-mini.jpg"
image: "Bifora_70A.jpg"
movementlistkey: "bifora"
caliberkey: "70A"
manufacturers: ["bifora"]
manufacturers_weight: 7050
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_DAU_Bifora_70A.jpg"
    description: "Bifora Automatic Damenuhr"
  - image: "b/bifora/Bifora_DAU_2_Bifora_70A.jpg"
    description: "Bifora Automatic Damenuhr"
donation: "Dieses Werk incl. Uhr in gutem Zustand ist eine Spende von [Götz Schweitzer](/supporters/goetz-schweitzer/) an das Uhrwerksarchiv. Ganz herzlichen Dank für die großartige Unterstützung!"
---
Lorem Ipsum