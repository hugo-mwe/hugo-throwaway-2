---
title: "Bifora 735"
date: 2017-11-10T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 735","manual wind","17 Jewels","17 Rubis","pallet lever","7 3/4 lignes","screw balance","center second","Incabloc"]
description: "Bifora 735 - A rare manual wind movement in ladies' size from Bidlingmaier in Schwäbisch Gmünd, Germany"
abstract: "Another rare to find manual wind movement with center second in ladies' size from the german manufactory Bifora."
preview_image: "bifora_735-mini.jpg"
image: "Bifora_735.jpg"
movementlistkey: "bifora"
caliberkey: "735"
manufacturers: ["bifora"]
manufacturers_weight: 73500
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum