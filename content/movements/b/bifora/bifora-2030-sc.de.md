---
title: "Bifora 2030 SC"
date: 2017-01-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 2030 SC","Bifora 2030","Bifora","Bidlingmaier FormAnker","Schraubenunruh","15 Jewels","Zentralsekunde"]
description: "Bifora 2030 SC - Ein frühes Formankerwerk von Bidlingmaier (Bifora) mit Zentralsekunde"
abstract: "Ein frühes Bidlingmaier-Form-Ankerwerk mit Zentralsekunde und Stoßsicherung aus den 1940er Jahren. "
preview_image: "bifora_2030_sc-mini.jpg"
image: "Bifora_2030_SC.jpg"
movementlistkey: "bifora"
caliberkey: "2030 SC"
manufacturers: ["bifora"]
manufacturers_weight: 203050
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam in relativ gutem Zustand ins Labor, leider war die verlängerte Welle des Kleinbodenrads bereits angebrochen, so daß sie beim Abheben des Triebrads für die Zentralsekunde brach. Der restlichen Funktion dieses Werks tat dies keinen Abbruch, auf einen Service wurde aber verzichtet.
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_2030_SC.jpg"
    description: "Bifora Herrenuhr"
timegrapher_old: 
  rates:
    ZO: "+100"
    ZU: "+24"
    3O: "+50"
    6O: "-35"
    9O: "-759"
    12O: "-141"
  amplitudes:
    ZO: "197"
    ZU: "186"
    3O: "124"
    6O: "145"
    9O: "161"
    12O: "126"
  beaterrors:
    ZO: "1.9"
    ZU: "2.3"
    3O: "3.7"
    6O: "2.1"
    9O: "2.6"
    12O: "2.8"
---
Lorem Ipsum