---
title: "Bifora B12"
date: 2017-05-06T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora B12","Quarz","quartz"]
description: "Bifora B12 - The first inhouse quartz movement from Bifora. Short article with video"
abstract: "The first quartz movement from Bifora, whose construction with a rocking lever reminds very much of an electromechanical movement."
preview_image: "bifora_b12-mini.jpg"
image: "Bifora_B12.jpg"
movementlistkey: "bifora"
caliberkey: "B12"
manufacturers: ["bifora"]
manufacturers_weight: 1200
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_Quartz_HU_Bifora_B12.jpg"
    description: "Bifora Quartz gents watch"
links: |
  * [Crazywatches.pl: Bifora B12 (1973)](http://www.crazywatches.pl/bifora-b12-quartz-1973)
  * [Hartmut Wynen: Bifora](http://www.hwynen.de/bifora.html)
labor: |
  <p>The specimen shown here is fully working, but loses a few seconds per day, in sum about one minute per week. This can be either to a dry movement (no wonder, after more than 40 years) or an aged quartz crystal, for which there hardly will exist spare parts and more.</p><p>For archiving purposes, this doesn't matter, since the movement runs in general.</p>
---
Lorem Ipsum