---
title: "Bifora 103 SA new"
date: 2017-10-07T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 103 SA","Bifora","Bimag","Bidlingmaier","Automatic","22 Jewels","decentral second","oscillating weight"]
description: "Bifora 103 SA (new) - The first german selfwinding movement in its first and only revision."
abstract: "The first German automatic selfwinding movement, in its first and only revision"
preview_image: "bifora_103_sa_neu-mini.jpg"
image: "Bifora_103_SA_neu.jpg"
movementlistkey: "bifora"
caliberkey: "103 SA neu"
manufacturers: ["bifora"]
manufacturers_weight: 10351
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Automatic_Bifora_103_SA_neu.jpg"
    description: "Bifora Automatic gents watch"
---
Lorem Ipsum