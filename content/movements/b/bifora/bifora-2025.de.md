---
title: "Bifora 2025"
date: 2015-03-08T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 2025","Bifora","Bidlingmaier FormAnker","Schraubenunruh","7 Jewels"]
description: "Bifora 2025 - das erste Formankerwerk von Bidlingmaier"
abstract: "Das erste Form-Ankerwerk von Bidlingmaier, möglicherweise sogar das erste deutsche Form-Ankerwerk überhaupt.  "
preview_image: "bifora_2025-mini.jpg"
image: "Bifora_2025.jpg"
movementlistkey: "bifora"
caliberkey: "2025"
manufacturers: ["bifora"]
manufacturers_weight: 202500
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum