---
title: "Bifora 934"
date: 2010-01-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["glucydur","screw balance","Bishock","decentral second","pillar construction","rocking bar winding mechanism","Bifora 934","Bifora","934","17 Jewels","Bidlingmaier","Schwäbisch Gmünd","germany"]
description: "Bifora 934 - THE standard movement from Bifora, here probably its best version."
abstract: ""
preview_image: "bifora_934-mini.jpg"
image: "Bifora_934.jpg"
movementlistkey: "bifora"
caliberkey: "934"
manufacturers: ["bifora"]
manufacturers_weight: 93400
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_934.jpg"
    description: "Bifora mens' watch"
timegrapher_old: 
  description: |
    The specimen shown here, was completely gummed and cleaned and olied afterwards. Nevertheless, the timegrapher outputs are very unsteady, especially in the vertical positions. And there, the positions "12 up" and "9 up" are pretty worse; the reasons can be a badly poised balance or a bent balance staff.
  images:
    ZO: Zeitwaage_Bifora_934_ZO.jpg
    ZU: Zeitwaage_Bifora_934_ZU.jpg
    3O: Zeitwaage_Bifora_934_3O.jpg
    6O: Zeitwaage_Bifora_934_6O.jpg
    9O: Zeitwaage_Bifora_934_9O.jpg
    12O: Zeitwaage_Bifora_934_12O.jpg
  values:
    ZO: "+45"
    ZU: "+50"
    3O: "-120"
    6O: "+6"
    9O: "-240"
    12O: "-240"
links: |
  * [Ranfft Watches: Bifora 934](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Bifora_934)
---
Lorem Ipsum