---
title: "Bifora 91/1"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 91/1","Bifora 91","Bifora","Bidlingmaier","Bishock","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","17","Steine","Jewels"]
description: "Bifora 91/1"
abstract: ""
preview_image: "bifora_91_1-mini.jpg"
image: "Bifora_91_1.jpg"
movementlistkey: "bifora"
caliberkey: "91/1"
manufacturers: ["bifora"]
manufacturers_weight: 9110
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_DAU.jpg"
    description: "Bifora Damenuhr"
  - image: "b/bifora/Bifora_DAU_2.jpg"
    description: "Bifora Damenuhr"
---
Lorem Ipsum