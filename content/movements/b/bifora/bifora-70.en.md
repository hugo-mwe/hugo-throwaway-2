---
title: "Bifora 70"
date: 2017-10-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 70","manual wind","16 Jewels","16 Rubis","pallet lever","7 1/4 lignes"]
description: "Bifora 70 - the smallest round manual wind movement from Bifora"
abstract: "The smallest round manual wind movement from Bifora of Schwäbisch Gmünd, Germany"
preview_image: "bifora_70-mini.jpg"
image: "Bifora_70.jpg"
movementlistkey: "bifora"
caliberkey: "70"
manufacturers: ["bifora"]
manufacturers_weight: 7000
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here came in working condition into the lab, so no service and oiling was made.</p><p>The second specimen (the Dugena one) had a broken balance wheel and also a broken minute wheel.</p>
usagegallery: 
  - image: "b/bifora/Bifora_DAU_Bifora_70.jpg"
    description: "Bifora ladies' watch"
  - image: "d/dugena/Dugena_DAU_Bifora_70.jpg"
    description: "Dugena ladies' watch"
timegrapher_old: 
  rates:
    ZO: "+7"
    ZU: "+9"
    3O: "-90"
    6O: "-150"
    9O: "-50"
    12O: "-2"
  amplitudes:
    ZO: "206"
    ZU: "197"
    3O: "177"
    6O: "161"
    9O: "144"
    12O: "163"
  beaterrors:
    ZO: "2.8"
    ZU: "2.0"
    3O: "2.0"
    6O: "2.6"
    9O: "2.6"
    12O: "2.0"
donation: "This movement, including the Bifora watch, is a kind donation of [Götz Schweitzer](/supporters/goetz-schweitzer/) to the movement archive. Thank you very much for the great support!"
---
Lorem Ipsum