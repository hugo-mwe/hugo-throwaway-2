---
title: "Bifora 910/1"
date: 2014-11-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 910/1","Bifora","Automatic","Pfeilerwerk","23 Jewels"]
description: "Bifora 910/1 - Ein deutsches Automaticwerk mittlerer Größe aus Schwäbisch Gmünd"
abstract: " Ein relativ einfaches Automaticwerk aus Deutschland in Pfeilerbauweise, dafür aber mit Palettenanker und beidseitigem Aufzug."
preview_image: "bifora_910_1-mini.jpg"
image: "Bifora_910_1.jpg"
movementlistkey: "bifora"
caliberkey: "910/1"
manufacturers: ["bifora"]
manufacturers_weight: 91010
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  images:
    ZO: Zeitwaage_Bifora_910_1_ZO.jpg
    ZU: Zeitwaage_Bifora_910_1_ZU.jpg
    3O: Zeitwaage_Bifora_910_1_3O.jpg
    6O: Zeitwaage_Bifora_910_1_6O.jpg
    9O: Zeitwaage_Bifora_910_1_9O.jpg
    12O: Zeitwaage_Bifora_910_1_12O.jpg
  values:
    ZO: "-30"
    ZU: "+30"
    3O: "-120"
    6O: "-140"
    9O: "-?"
    12O: "-105"
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_910_1.jpg"
    description: "Bifora Herrenuhr"
labor: |
  Das Werk kam samt Uhr mit fehlendem Boden in recht desolatem, verdreckten Zustand und wurde einem Komplettservice unterzogen. Auch wenn die Gangwerte (siehe unten) nicht wirklich berauschend sind, so wurde das Werk doch zumindest wieder gangfähig gemacht
---
Lorem Ipsum