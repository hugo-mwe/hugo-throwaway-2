---
title: "Bauer, H.F. (HFB)"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["H.F.Bauer","Bauer","HFB","Pforzheim"]
categories: ["movements","movements_b"]
movementlistkey: "bauer"
abstract: "Uhrwerke von H.F.Bauer, Pforzheim"
description: "Uhrwerke von H.F.Bauer, Pforzheim"
---
(Hermann Friedrich Bauer, Pforzheim)
{{< movementlist "bauer" >}}

{{< movementgallery "bauer" >}}