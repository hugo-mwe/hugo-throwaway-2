---
title: "H.F.Bauer 10 1/2 neu"
date: 2017-10-04T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bauer 10 1/2","Bauer","10 1/2","H.F.Bauer","Handaufzug","15 Jewels","15 Rubis","15 Steine"]
description: "Bauer 10 1/2 neu - ein eher seltenes Handaufzugswerk aus den 1940ern"
abstract: "Ein seltenes und optisch recht ansprechendes Werk aus deutscher Produktion."
preview_image: "bauer_10_12_neu-mini.jpg"
image: "Bauer_10_12_neu.jpg"
movementlistkey: "bauer"
caliberkey: "10 1/2 neu"
manufacturers: ["bauer"]
manufacturers_weight: 1012
categories: ["movements","movements_b","movements_b_hfb"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das hier vorgestellt Werk kam mit defekter (abgebrochener) Winkelhebelschraube ins Labor, ansonsten war sein Zustand recht gut. Auf einen Service konnte verzichtet werden, aber die Winkelhebelschraube konnte zum Glück ersetzt werden, so daß das Werk inzwischen wieder funktionsfähig ist
donation: "Dieses Werk wurde von [Rolf-Jürgen](/supporters/rolf-juergen/) gespendet. Ganz herzlichen Dank für die Unterstützung des Uhrwerksachivs!"
timegrapher_old: 
  rates:
    ZO: "+1"
    ZU: "-24"
    3O: "-168"
    6O: "-70"
    9O: "+30"
    12O: "0"
  amplitudes:
    ZO: "268"
    ZU: "323"
    3O: "226"
    6O: "215"
    9O: "217"
    12O: "241"
  beaterrors:
    ZO: "3.5"
    ZU: "3.6"
    3O: "03.7"
    6O: "3.5"
    9O: "4.1"
    12O: "04.1"
---
Lorem Ipsum