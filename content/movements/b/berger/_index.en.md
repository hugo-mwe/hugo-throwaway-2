---
title: "Berger"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Movement","movements","Berger"]
categories: ["movements","movements_b"]
movementlistkey: "berger"
description: "Movements by the swiss manufacturer Berger from Oberdorf"
abstract: "Movements by the swiss manufacturer Berger from Oberdorf"
---
(Berger & Co, Succursale de Felsa SA, Oberdorf, Switzerland)
{{< movementlist "berger" >}}

{{< movementgallery "berger" >}}