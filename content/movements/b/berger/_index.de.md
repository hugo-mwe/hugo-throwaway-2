---
title: "Berger"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["Uhrwerk","Uhrwerke","Berger","Oberdorf","Felsa"]
categories: ["movements","movements_b"]
movementlistkey: "berger"
abstract: "Uhrwerke des schweizer Herstellers Berger aus Oberdorf"
description: "Uhrwerke des schweizer Herstellers Berger aus Oberdorf"
---
(Berger & Co, Niederlassung der Felsa AG, Oberdorf, Schweiz)
{{< movementlist "berger" >}}

{{< movementgallery "berger" >}}