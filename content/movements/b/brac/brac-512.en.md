---
title: "Brac 512"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Brac 512","Brac","512","Swiss","1 Jewel","17 Jewels","watch","watches","wristwatch","wristwatches","pin lever","Switzerland"]
description: "Brac 512"
abstract: ""
preview_image: "brac_512-mini.jpg"
image: "Brac_512.jpg"
movementlistkey: "brac"
caliberkey: "512"
manufacturers: ["brac"]
manufacturers_weight: 512
categories: ["movements","movements_b","movements_b_brac_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/expert/Expert_HAU.jpg"
    description: "Expert mens' watch"
  - image: "s/selecta/Selecta_de_Luxe_Taucheruhr.jpg"
    description: "Selecta de Luxe divers' watch"
---
Lorem Ipsum