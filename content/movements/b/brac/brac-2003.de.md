---
title: "Brac 2003"
date: 2010-02-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Stiftankerwerk","indirekte Zentralsekunde","Blattfeder","doppelt ausgeführtes Sekundenrad","Resomatic","Brac 2003","Brac","2003","17 Jewels","Schweiz","17 Steine"]
description: "Brac 2003 - Ein sehr gut verarbeitetes 10 1/2 liniges Stiftankerwerk mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video, Makro-Aufnahme, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "brac_2003-mini.jpg"
image: "Brac_2003.jpg"
movementlistkey: "brac"
caliberkey: "2003"
manufacturers: ["brac"]
manufacturers_weight: 2003
categories: ["movements","movements_b","movements_b_brac"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das Werk kam lose, also ohne Gehäuse, und mit gebrochener Gesperrfeder in die Werkstatt, wurde komplett zerlegt, gereinigt und geölt und mit einer funktionierenden Gesperrfeder versehen.
usagegallery: 
  - image: "t/tegrov/Tegrov_HAU_Zifferblatt_Brac_2003.jpg"
    description: "Tegrov Herrenuhr"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Brac_2003_ZO.jpg
    ZU: Zeitwaage_Brac_2003_ZU.jpg
    3O: Zeitwaage_Brac_2003_3O.jpg
    6O: Zeitwaage_Brac_2003_6O.jpg
    9O: Zeitwaage_Brac_2003_9O.jpg
    12O: Zeitwaage_Brac_2003_12O.jpg
  values:
    ZO: "-7"
    ZU: "+5"
    3O: "-80"
    6O: "-50"
    9O: "+-0"
    12O: "-30"
---
Lorem Ipsum