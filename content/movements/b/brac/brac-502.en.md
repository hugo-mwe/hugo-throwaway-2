---
title: "Brac 502"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Brac 502","Brac","502","Swiss","1 Jewel","watch","watches","wristwatch","wristwatches","pin lever","Switzerland"]
description: "Brac 502"
abstract: ""
preview_image: "brac_502-mini.jpg"
image: "Brac_502.jpg"
movementlistkey: "brac"
caliberkey: "502"
manufacturers: ["brac"]
manufacturers_weight: 502
categories: ["movements","movements_b","movements_b_brac_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/orion/Orion_HAU.jpg"
    description: "Orion mens' watch"
---
Lorem Ipsum