---
title: "Brac 192"
date: 2009-06-21T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Brac 192","Brac","192","Swiss","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","Stiftanker","17 Steine"]
description: "Brac 192"
abstract: ""
preview_image: "brac_190-mini.jpg"
image: "Brac_192.jpg"
movementlistkey: "brac"
caliberkey: "192"
manufacturers: ["brac"]
manufacturers_weight: 192
categories: ["movements","movements_b","movements_b_brac"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Brac 192](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Brac_192)
usagegallery: 
  - image: "r/rimarex/Rimarex_HAU.jpg"
    description: "Rimarex Herrenuhr"
---
Lorem Ipsum