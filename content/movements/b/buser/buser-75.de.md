---
title: "Buser 75"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Buser 75","Buser","75","4 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","4 Steine","Handaufzug","Schweiz","Zylinderhemmung"]
description: "Buser 75"
abstract: ""
preview_image: "buser_75-mini.jpg"
image: "Buser_75.jpg"
movementlistkey: "buser"
caliberkey: "75"
manufacturers: ["buser"]
manufacturers_weight: 75
categories: ["movements","movements_b","movements_b_buser"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tiptop/TipTop_DAU.jpg"
    description: "Tiptop Damenuhr"
---
Lorem Ipsum