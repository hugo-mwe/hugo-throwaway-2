---
title: "Bulova 5 BD"
date: 2009-10-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bulova 5 BD","Bulova","5 BD","23 Jewels","Biel","New York","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","23 Steine","Formwerk","Tonneau"]
description: "Bulova 5 BD"
abstract: ""
preview_image: "bulova_5_bd-mini.jpg"
image: "Bulova_5_BD.jpg"
movementlistkey: "bulova"
caliberkey: "5 BD"
manufacturers: ["bulova"]
manufacturers_weight: 5
categories: ["movements","movements_b","movements_b_bulova"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum