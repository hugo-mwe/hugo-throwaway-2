---
title: "Bulova 5 BD"
date: 2009-10-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bulova 5 BD","Bulova","5 BD","23 Jewels","Biel","New York","watch","watches","wristwatch","wristwatches","caliber","swiss","switzerland","form movement","tonneau"]
description: "Bulova 5 BD"
abstract: ""
preview_image: "bulova_5_bd-mini.jpg"
image: "Bulova_5_BD.jpg"
movementlistkey: "bulova"
caliberkey: "5 BD"
manufacturers: ["bulova"]
manufacturers_weight: 5
categories: ["movements","movements_b","movements_b_bulova_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum