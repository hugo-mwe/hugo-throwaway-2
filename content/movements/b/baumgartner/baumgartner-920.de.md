---
title: "Baumgartner 920"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 920","BFG 920","Baumgartner","BFG","920","17 Jewels","mechanisch","Handaufzug","Stiftanker","Schweiz","Steine"]
description: "Baumgartner 920"
abstract: ""
preview_image: "baumgartner_920-mini.jpg"
image: "Baumgartner_920.jpg"
movementlistkey: "baumgartner"
caliberkey: "920"
manufacturers: ["baumgartner"]
manufacturers_weight: 920
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_17_Jewels.jpg"
    description: "Kienzle Damenuhr &quot;Made In Germany&quot;"
---
Lorem Ipsum