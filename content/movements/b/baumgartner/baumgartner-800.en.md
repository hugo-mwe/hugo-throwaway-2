---
title: "Baumgartner 800"
date: 2012-12-30T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 800","BFG 800","BFG","Grenchen","pin lever","Roskopf","indirect center second","5 jewels"]
description: "Baumgartner 800 - A well made Roskopf-type pin lever movement. With photos, macros, video and timegrapher output."
abstract: "A better pin lever movement with 5 jewels and center second"
preview_image: "baumgartner_800-mini.jpg"
image: "Baumgartner_800.jpg"
movementlistkey: "baumgartner"
caliberkey: "800"
manufacturers: ["baumgartner"]
manufacturers_weight: 800
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Homepage BFG-Baumgartner](http://www.bfg-baumgartner.de)
timegrapher_old: 
  description: |
    In the laying positions, the Baumgartner 800 performs very well, but in the hanging positions, it loses any precision, which indicates a now well balanced balance - probably the ruby ellipse is the cause here.
  images:
    ZO: Zeitwaage_Baumgartner_800_ZO.jpg
    ZU: Zeitwaage_Baumgartner_800_ZU.jpg
    3O: Zeitwaage_Baumgartner_800_3O.jpg
    6O: Zeitwaage_Baumgartner_800_6O.jpg
    9O: Zeitwaage_Baumgartner_800_9O.jpg
    12O: Zeitwaage_Baumgartner_800_12O.jpg
  values:
    ZO: "-8"
    ZU: "-+0"
    3O: "+240"
    6O: "+270"
    9O: "..."
    12O: ".."
usagegallery: 
  - image: "g/gisa/Gisa_HAU_Baumgartner_800.jpg"
    description: "Gisa mens' watch"
---
Lorem Ipsum