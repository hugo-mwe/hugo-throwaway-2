---
title: "Baumgartner 920"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 920","BFG 920","Baumgartner","BFG","920","17 Jewels","mechanical","balance","pin lever","swiss","Switzerland"]
description: "Baumgartner 920"
abstract: ""
preview_image: "baumgartner_920-mini.jpg"
image: "Baumgartner_920.jpg"
movementlistkey: "baumgartner"
caliberkey: "920"
manufacturers: ["baumgartner"]
manufacturers_weight: 920
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_17_Jewels.jpg"
    description: "Kienzle ladies' watch &quot;Made In Germany&quot;"
---
Lorem Ipsum