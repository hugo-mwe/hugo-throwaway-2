---
title: "Baumgartner"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: []
categories: ["movements","movements_b"]
movementlistkey: "baumgartner"
abstract: "(Baumgartner Frères, Grenchen, Switzerland)"
description: ""
---
(Baumgartner Frères, Grenchen, Switzerland)
{{< movementlist "baumgartner" >}}

{{< movementgallery "baumgartner" >}}