---
title: "Baumgartner 930"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 930","Baumgartner","930","BFG","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","pin lever","swiss","switzerland"]
description: "Baumgartner 930"
abstract: ""
preview_image: "baumgartner_930-mini.jpg"
image: "Baumgartner_930.jpg"
movementlistkey: "baumgartner"
caliberkey: "930"
manufacturers: ["baumgartner"]
manufacturers_weight: 930
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/buler/Buler_DAU_3.jpg"
    description: "Buler ladies' watch"
---
Lorem Ipsum