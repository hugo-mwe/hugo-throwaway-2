---
title: "Baumgartner 911"
date: 2018-01-18T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 911","Baumgartner","911","BFG 911","BFG","Swiss","5 Jewels","1 Jewel","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","Stiftanker","5 Steine","1 Stein"]
description: "Baumgartner 911 - Ein modernes Stiftankerwerk in Roskopf-Bauweise"
abstract: "Ein recht häufig anzutreffendes Stiftankerwerk aus der Schweiz"
preview_image: "baumgartner_911-mini.jpg"
image: "Baumgartner_911.jpg"
movementlistkey: "baumgartner"
caliberkey: "911"
manufacturers: ["baumgartner"]
manufacturers_weight: 911
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["true"]
links: |
  * [Ranfft Uhren: Baumgartner 911](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Baumgartner_911)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Version ohne Datum und Zentralsekunde und vielen Dank an [Günter G](/supporters/guenter-g/). für die Spende dieses Werks, ebenfalls ohne Datum und Zentralsekunde inklusive Richard Damenuhr."
usagegallery: 
  - image: "r/royal/Royal_Calendar_DAU.jpg"
    description: "Royal Calendar Damenuhr"
  - image: "g/geeta/Geeta_DAU_Zifferblatt.jpg"
    description: "Geeta Damenuhr  (nur Zifferblatt)"
  - image: "r/richard/Richard_De_Luxe_DU_Baumgartner_911.jpg"
    description: "Richard De Luxe Damenuhr"
---
Lorem Ipsum