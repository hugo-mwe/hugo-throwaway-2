---
title: "Baumgartner 34"
date: 2009-09-13T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 34","Baumgartner","34","BFG 34","1 Jewels","BFG","Swiss","watch","watches","wristwatch","wristwatches","caliber","switzerland","pin lever"]
description: "Baumgartner 34 "
abstract: ""
preview_image: "baumgartner_34-mini.jpg"
image: "Baumgartner_34.jpg"
movementlistkey: "baumgartner"
caliberkey: "34"
manufacturers: ["baumgartner"]
manufacturers_weight: 34
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/lamar/Lamar_de_Luxe_HAU.jpg"
    description: "Lamar de Luxe mens' watch"
  - image: "l/lugano/Lugano_DAU_Baumgartner_34.jpg"
    description: "Lugano Damenuhr"
links: |
  * [Ranfft Watches: Baumgartner 34](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Baumgartner_34)
---
Lorem Ipsum