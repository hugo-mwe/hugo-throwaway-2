---
title: "Baumgartner 866"
date: 2009-11-29T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 866","BFG 866","BFG","Baumgartner","866","1 Jewel","watch","watches","wristwatch","wristwatches","childrens` watch","movement","caliber","swiss","pin lever"]
description: "Baumgartner 866"
abstract: ""
preview_image: "baumgartner_866-mini.jpg"
image: "Baumgartner_866.jpg"
movementlistkey: "baumgartner"
caliberkey: "866"
manufacturers: ["baumgartner"]
manufacturers_weight: 866
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Cowboyuhr.jpg"
    description: "anonyme kids watch with Cowboy motive"
  - image: "b/buler/Buler_de_Luxe_HAU_Baumgartner_866.jpg"
    description: "Buler de Luxe mens' watch"
---
Lorem Ipsum