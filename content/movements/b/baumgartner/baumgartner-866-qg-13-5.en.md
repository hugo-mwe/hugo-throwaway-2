---
title: "Baumgartner 866 (QG, 13,5''')"
date: 2009-11-22T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","Baumgartner 866 QG","BFG 866 QG","BFG","Baumgartner","866","1 Jewel","watch","watches","wristwatch","wristwatches","childrens` watch","movement","caliber","swiss","pin lever"]
description: "Baumgartner 866 QG 13,5'''"
abstract: ""
preview_image: "baumgartner_866_qg_13_5-mini.jpg"
image: "Baumgartner_866_QG_13_5.jpg"
movementlistkey: "baumgartner"
caliberkey: "866 (QG, 13,5)"
manufacturers: ["baumgartner"]
manufacturers_weight: 866135
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Watches: Baumgartner 866 (QG)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Baumgartner_866_CLD&)
donation: "This movement was kindly donated by [Klaus Brunnemer](/supporters/en/). Thank you very much!"
---
Lorem Ipsum