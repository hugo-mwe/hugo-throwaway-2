---
title: "Baumgartner 582"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 582","Baumgartner","582","BFG 582","BFG","KIF Trior","Novodiac","2 Jewels","1 Jewel","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","2 Steine","1 Stein","Stiftankerhemmung","Stiftanker","Schweiz"]
description: "Baumgartner 582"
abstract: ""
preview_image: "baumgartner_582-mini.jpg"
image: "Baumgartner_582.jpg"
movementlistkey: "baumgartner"
caliberkey: "582"
manufacturers: ["baumgartner"]
manufacturers_weight: 582
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Baumgartner 582](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Baumgartner_582)
usagegallery: 
  - image: "a/astromaster/Astromaster_HAU.jpg"
    description: "Astromaster Herrenuhr"
  - image: "g/grovana/Grovana_HAU.jpg"
    description: "Grovana Herrenuhr"
---
Lorem Ipsum