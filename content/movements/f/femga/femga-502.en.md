---
title: "Femga 502"
date: 2014-02-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Femga 502","Femga","502","France","Annemasse","center second","17 Jewels"]
description: "Femga 502 - a french mens' watch movement with indirect center second indication"
abstract: "A french mens' watch movement with indirect center second from the early 1950ies"
preview_image: "femga_502-mini.jpg"
image: "Femga_502.jpg"
movementlistkey: "femga"
caliberkey: "502"
manufacturers: ["femga"]
manufacturers_weight: 502
categories: ["movements","movements_f","movements_f_femga_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen shown here was in good condition, but nevertheless got a full service with cleaning and oiling.
usagegallery: 
  - image: "a/anker/Anker_HAU_Femga_502.jpg"
    description: "Anker mens' watch"
timegrapher_old: 
  description: |
    In the horizontal positions, the Femga 502 showed extraordinary performance, on the vertical positions, it performed not really well with large deviation in total and big positional differences.
  images:
    ZO: Zeitwaage_Femga_502_ZO.jpg
    ZU: Zeitwaage_Femga_502_ZU.jpg
    3O: Zeitwaage_Femga_502_3O.jpg
    6O: Zeitwaage_Femga_502_6O.jpg
    9O: Zeitwaage_Femga_502_9O.jpg
    12O: Zeitwaage_Femga_502_12O.jpg
  values:
    ZO: "-1"
    ZU: "+-0"
    3O: "-30"
    6O: "-35"
    9O: "-60"
    12O: "-42"
---
Lorem Ipsum