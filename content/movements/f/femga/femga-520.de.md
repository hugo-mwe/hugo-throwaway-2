---
title: "Femga 520"
date: 2013-04-06T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FEMGA","Femga","FEMGA 520","Annemasse","Frankreich","France","Ankerwerk","Anker","Zentralsekunde"]
description: "FEMGA 520 - Ein französisches Ankerwerk mit direkter Zentralsekunde"
abstract: "Ein ganz typisches 50-er-Jahre Ankerwerk mit direkter Zentralsekunde, diesmal aus Frankreich"
preview_image: "femga_520-mini.jpg"
image: "Femga_520.jpg"
movementlistkey: "femga"
caliberkey: "520"
manufacturers: ["femga"]
manufacturers_weight: 520
categories: ["movements","movements_f","movements_f_femga"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/engo/Engo_HAU_Femga_520.jpg"
    description: "Engo Herrenuhr"
---
Lorem Ipsum