---
title: "Femga 67"
date: 2009-12-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Femga 67","Femga","67","17 Jewels","Annemasse","Frankreich","17 Steine","Formwerk"]
description: "Femga 67"
abstract: ""
preview_image: "femga_67-mini.jpg"
image: "Femga_67.jpg"
movementlistkey: "femga"
caliberkey: "67"
manufacturers: ["femga"]
manufacturers_weight: 67
categories: ["movements","movements_f","movements_f_femga"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Femga 67](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Femga_67)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende des Werks!</p>
usagegallery: 
  - image: "o/oridam/Oridam_DAU_Zifferblatt_Femga_67.jpg"
    description: "Oridam Damenuhr"
timegrapher_old: 
  description: |
    Dieses Werk wurde gereinigt und geölt.
  images:
    ZO: Zeitwaage_Femga_67_ZO.jpg
    ZU: Zeitwaage_Femga_67_ZU.jpg
    3O: Zeitwaage_Femga_67_3O.jpg
    6O: Zeitwaage_Femga_67_6O.jpg
    9O: Zeitwaage_Femga_67_9O.jpg
    12O: Zeitwaage_Femga_67_12O.jpg
---
Lorem Ipsum