---
title: "FEF 6764"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FEF 6764","FEF","6764","17 Jewels","Swiss","ladies` watch","caliber"]
description: "FEF 6764"
abstract: ""
preview_image: "fef_6764-mini.jpg"
image: "FEF_6764.jpg"
movementlistkey: "fef"
caliberkey: "6764"
manufacturers: ["fef"]
manufacturers_weight: 6764
categories: ["movements","movements_f","movements_f_fef_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mirexal/Mirexal_DAU.jpg"
    description: "Mirexal ladies' watch"
---
Lorem Ipsum