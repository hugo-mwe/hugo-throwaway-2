---
title: "Förster 106"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Foerster 106","Foerster","106","Förster","BF","Foresta","Bernhard Förster","Pforzheim","Deutschland","Zylinderhemmung","Zylinder"]
description: "Förster 106"
abstract: " "
preview_image: "foerster_106-mini.jpg"
image: "Foerster_106.jpg"
movementlistkey: "foerster"
caliberkey: "106"
manufacturers: ["foerster"]
manufacturers_weight: 106
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Anonym_Foerster_106.jpg"
    description: "Unmarkierte Damenuhr"
---
Lorem Ipsum