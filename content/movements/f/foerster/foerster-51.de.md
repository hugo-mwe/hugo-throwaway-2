---
title: "Förster 51"
date: 2017-11-29T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Förster 51","Förster","BF","Elastor","21 Jewels","21 Rubis","Zentralsekunde"]
description: "Förster 51 - Ein Handaufzugswerk mit hauseigener Stoßsicherung"
abstract: "Ein recht unspektakuläres Handaufzugswerk, das mit zahlreichen Decksteinen und einer hauseigenen Elastor-Förster-Stoßsicherung ausgestattet ist."
preview_image: "foerster_51-mini.jpg"
image: "Foerster_51.jpg"
movementlistkey: "foerster"
caliberkey: "51"
manufacturers: ["foerster"]
manufacturers_weight: 51
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum