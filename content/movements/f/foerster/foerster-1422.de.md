---
title: "Förster 1422"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Förster","Förster 1422","1422","422","Foerster","Forster","FB","BF","Damenuhr","21","Steine","21 Steine","21 Rubis","Automatik","Automatic","Deutschland"]
description: "Förster 1422"
abstract: " "
preview_image: "foerster_1422-mini.jpg"
image: "Foerster_1422.jpg"
movementlistkey: "foerster"
caliberkey: "1422"
manufacturers: ["foerster"]
manufacturers_weight: 1422
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_DAU_Automatic_2.jpg"
    description: "Meister Anker Automatic Damenuhr"
---
Lorem Ipsum