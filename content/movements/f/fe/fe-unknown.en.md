---
title: "FE ???"
date: 2014-12-25T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FE","France Ebauches","form movement","ladies' watch","17 Jewels","17 Rubis","pallet lever"]
description: "unknown FE movement"
abstract: ""
preview_image: "fe_unknown-mini.jpg"
image: "FE_Unknown.jpg"
movementlistkey: "fe"
caliberkey: "???"
manufacturers: ["fe"]
manufacturers_weight: 0
categories: ["movements","movements_f","movements_f_fe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "y/yema/Yema_DAU.jpg"
    description: "Yema ladies' watch"
---
Lorem Ipsum