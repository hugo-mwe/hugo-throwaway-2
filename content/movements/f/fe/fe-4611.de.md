---
title: "FE 4611"
date: 2014-12-24T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FE","FE 4611","4611","FE 46 11","46 11","France Ebauches","17","Steine","17 Steine","17 Rubis","Russland","Frankreich","Automatik","Automatic"]
description: "FE 4611"
abstract: ""
preview_image: "fe_4611-mini.jpg"
image: "FE_4611.jpg"
movementlistkey: "fe"
caliberkey: "4611"
manufacturers: ["fe"]
manufacturers_weight: 4611000
categories: ["movements","movements_f","movements_f_fe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum