---
title: "FE 140-1C"
date: 2014-12-25T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FE 140-1C","FE","140-1C","France Ebauces","France","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Frankreich","17 Steine"]
description: "FE 140-1C"
abstract: ""
preview_image: "fe_140-1c-mini.jpg"
image: "FE_140-1C.jpg"
movementlistkey: "fe"
caliberkey: "140-1C"
manufacturers: ["fe"]
manufacturers_weight: 140013
categories: ["movements","movements_f","movements_f_fe"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: FE 140-1](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&FE_140_1)
usagegallery: 
  - image: "p/praetina/Praetina_HAU_2.jpg"
    description: "Prätina Herrenuhr"
---
Lorem Ipsum