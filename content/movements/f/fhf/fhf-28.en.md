---
title: "FHF 28"
date: 2011-01-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","FHF 28","FHF","28","Fontainemelon","Fabrique d`Horlogerie de Fontainemelon","17 Jewels","Swiss","Switzerland"]
description: "FHF 28"
abstract: ""
preview_image: "fhf_28-mini.jpg"
image: "FHF_28.jpg"
movementlistkey: "fhf"
caliberkey: "28"
manufacturers: ["fhf"]
manufacturers_weight: 2800
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: FHF 28](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&FHF_28)
usagegallery: 
  - image: "p/pronto/Pronto_HAU.jpg"
    description: "Pronto gents watch"
  - image: "a/ancre/Ancre_HAU_FHF_28.jpg"
    description: "Ancre gents watch"
---
Lorem Ipsum