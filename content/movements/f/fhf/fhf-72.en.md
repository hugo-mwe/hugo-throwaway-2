---
title: "FHF 72"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 72","FHF","72","Fontainemelon","Fabrique d`Horlogerie de Fontainemelon","17 Jewels","Swiss","Switzerland"]
description: "FHF 72"
abstract: ""
preview_image: "fhf_72-mini.jpg"
image: "FHF_72.jpg"
movementlistkey: "fhf"
caliberkey: "72"
manufacturers: ["fhf"]
manufacturers_weight: 7200
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dubois/DuBois_HAU.jpg"
    description: "DuBois gents watch"
links: |
  * [Ranfft Uhren: FHF 72](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&FHF_72)
---
Lorem Ipsum