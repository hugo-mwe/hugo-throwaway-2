---
title: "FHF"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["FHF","Fontainemelon","Font","Schweiz"]
categories: ["movements","movements_f"]
movementlistkey: "fhf"
abstract: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
description: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
---
(Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz)
{{< movementlist "fhf" >}}

{{< movementgallery "fhf" >}}