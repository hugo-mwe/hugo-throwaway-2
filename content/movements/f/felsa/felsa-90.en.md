---
title: "Felsa 90"
date: 2009-11-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 90","handwinding","cylinder","10 jewels","form movement"]
description: "Felsa 90"
abstract: ""
preview_image: "felsa_90-mini.jpg"
image: "Felsa_90.jpg"
movementlistkey: "felsa"
caliberkey: "90"
manufacturers: ["felsa"]
manufacturers_weight: 900
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/ecly/Ecly.jpg"
    description: "Ecly ladies' watch"
  - image: "o/ormo/Ormo_DAU.jpg"
    description: "Ormo ladies' watch"
  - image: "k/kasta/Kasta_DAU_Zifferblatt.jpg"
    description: "Kasta ladies' watch  (dial only)"
  - image: "g/gam/Gam_DAU_Felsa_90.jpg"
    description: "Gam ladies' watch"
links: |
  * [Servicing a Felsa 90](http://inforeloj.forums.ylos.com/viewtopic.php?p=4499&sid=29278d6c7de8def2af8455d52d6129a5) (Spanisch article with lots of photos about servicing a Felsa 90)
---
Lorem Ipsum