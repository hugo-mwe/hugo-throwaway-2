---
title: "Felsa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["Felsa","Felsa AG","Schweiz"]
categories: ["movements","movements_f"]
movementlistkey: "felsa"
abstract: "Uhrwerke der Felsa AG aus Lengnau, Schweiz"
description: "Uhrwerke der Felsa AG aus Lengnau, Schweiz"
---
(Felsa AG, Lengnau, Schweiz)
{{< movementlist "felsa" >}}

{{< movementgallery "felsa" >}}