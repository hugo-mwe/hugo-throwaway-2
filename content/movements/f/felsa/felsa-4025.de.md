---
title: "Felsa 4025"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 4025","Felsa","4025","Lengau","Swiss","17 Jewels","Schweiz","17 Steine","Damenuhr","Handaufzug"]
description: "Felsa 4025"
abstract: ""
preview_image: "felsa_4025-mini.jpg"
image: "Felsa_4025.jpg"
movementlistkey: "felsa"
caliberkey: "4025"
manufacturers: ["felsa"]
manufacturers_weight: 40250
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/aero/Aero_DAU.jpg"
    description: "Aero Damenuhr"
---
Lorem Ipsum