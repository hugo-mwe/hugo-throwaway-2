---
title: "Felsa 690"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa","Bidynator","25","Rubis","Jewels","Switzerland","Swiss","Automatic"]
description: "Felsa 690"
abstract: ""
preview_image: "felsa_690-mini.jpg"
image: "Felsa_690.jpg"
movementlistkey: "felsa"
caliberkey: "690"
manufacturers: ["felsa"]
manufacturers_weight: 6900
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum