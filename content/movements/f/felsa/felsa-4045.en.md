---
title: "Felsa 4045"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 4045","Felsa","4045","17 Jewels","form movement","ladies` watch","swiss"]
description: "Felsa 4045"
abstract: ""
preview_image: "felsa_4045-mini.jpg"
image: "Felsa_4045.jpg"
movementlistkey: "felsa"
caliberkey: "4045"
manufacturers: ["felsa"]
manufacturers_weight: 40450
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ankra/Ankra_24_DAU.jpg"
    description: "Ankra 24 ladies' watch"
  - image: "a/ankra/Ankra_DAU.jpg"
    description: "Ankra ladies' watch"
---
Lorem Ipsum