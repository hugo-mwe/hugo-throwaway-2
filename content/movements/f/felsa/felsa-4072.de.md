---
title: "Felsa 4072"
date: 2009-10-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 4072","Felsa","4072","17 Jewels","Schweiz","17 Steine"]
description: "Felsa 4072"
abstract: ""
preview_image: "felsa_4072-mini.jpg"
image: "Felsa_4072.jpg"
movementlistkey: "felsa"
caliberkey: "4072"
manufacturers: ["felsa"]
manufacturers_weight: 40720
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Felsa 4072](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Felsa_4072)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende des Werks!</p>
usagegallery: 
  - image: "c/chopard/Chopard_DAU_2_Zifferblatt.jpg"
    description: "Chopard Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum