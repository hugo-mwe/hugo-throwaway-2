---
title: "Felsa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "en"
keywords: ["FHF","Fontainemelon","Font","Switzerland","Swiss"]
categories: ["movements","movements_f"]
movementlistkey: "felsa"
description: "Movement of the FHF, the Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Switzerland"
abstract: "Movement of the FHF, the Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Switzerland"
---
(Felsa AG, Lengnau, Switzerland)
{{< movementlist "felsa" >}}

{{< movementgallery "felsa" >}}