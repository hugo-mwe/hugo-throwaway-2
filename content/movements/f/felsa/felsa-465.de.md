---
title: "Felsa 465"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 465","Felsa","465","Incabloc","17 Jewels","Schweiz","17 Steine"]
description: "Felsa 465"
abstract: ""
preview_image: "felsa_465-mini.jpg"
image: "Felsa_465.jpg"
movementlistkey: "felsa"
caliberkey: "465"
manufacturers: ["felsa"]
manufacturers_weight: 4650
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dom/DomWatch_HAU.jpg"
    description: "Dom Watch Herrenuhr"
links: |
  * [Ranfft Uhren: Felsa 465](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Felsa_465)
---
Lorem Ipsum