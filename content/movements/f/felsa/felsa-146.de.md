---
title: "Felsa 146 ?"
date: 2009-11-04T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 146","15 Jewels","15 Rubis","Palettenanker","Schweiz"]
description: "Felsa 146"
abstract: ""
preview_image: "felsa_146-mini.jpg"
image: "Felsa_146.jpg"
movementlistkey: "felsa"
caliberkey: "146"
manufacturers: ["felsa"]
manufacturers_weight: 1460
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/supera/Supera_DAU_Zifferblatt.jpg"
    description: "Supera Damenuhr  (nur Zifferblatt)"
timegrapher_old: 
  description: |
    goldfarbige Ausführung Dieses Werk wurde gereinigt und geölt - die Zeitwaagenwerte für dieses über 60 Jahre alte Werk sind wahrlich beeindruckend!
  images:
    ZO: Zeitwaage_Felsa_146_ZO.jpg
    ZU: Zeitwaage_Felsa_146_ZU.jpg
    3O: Zeitwaage_Felsa_146_3O.jpg
    6O: Zeitwaage_Felsa_146_6O.jpg
    9O: Zeitwaage_Felsa_146_9O.jpg
    12O: Zeitwaage_Felsa_146_12O.jpg
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Version in der silberfarbigen, 15-steinigen Ausführung!"
---
Lorem Ipsum