---
title: "Felsa 4022"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 4022","Felsa","4022","Lengau","Swiss","17 Jewels","switzerland","ladies` watch"]
description: "Felsa 4022"
abstract: ""
preview_image: "felsa_4022-mini.jpg"
image: "Felsa_4022.jpg"
movementlistkey: "felsa"
caliberkey: "4022"
manufacturers: ["felsa"]
manufacturers_weight: 40220
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/blumus/Blumus_De_Luxe_DAU.jpg"
    description: "Blumus De Luxe ladies' watch"
---
Lorem Ipsum