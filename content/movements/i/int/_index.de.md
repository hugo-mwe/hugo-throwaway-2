---
title: "INT"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "de"
keywords: ["INT","AS","DuRoWe","FHF"]
categories: ["movements","movements_i"]
movementlistkey: "int"
description: "Uhrwerke von INT, einem Zusammenschluß deutscher und schweizer Hersteller wie AS, DuRoWe und FHF"
abstract: "Uhrwerke von INT, einem Zusammenschluß deutscher und schweizer Hersteller wie AS, DuRoWe und FHF"
---
(Ein Zusammenschluß verschiedener deutsch/schweizer Hersteller wie DuRoWe, FHF und AS)
{{< movementlist "int" >}}

{{< movementgallery "int" >}}