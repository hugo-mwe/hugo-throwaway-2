---
title: "INT"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "en"
keywords: ["INT","AS","FHF","DuRoWe"]
categories: ["movements","movements_i"]
movementlistkey: "int"
description: "Movements of INT, a federation between german and swiss manufacturers, such as AS, DuRoWe and FHF"
abstract: "Movements of INT, a federation between german and swiss manufacturers, such as AS, DuRoWe and FHF"
---
(a union of german and swiss manufacturers, such as AS, DuRoWe and FHF)
{{< movementlist "int" >}}

{{< movementgallery "int" >}}