---
title: "Intex 390"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Intex 390","Intex","390","Willi Friesinger","17 Jewels","17 Steine","Formwerk","Deutschland"]
description: "Intex 390"
abstract: ""
preview_image: "intex_390-mini.jpg"
image: "Intex_390.jpg"
movementlistkey: "intex"
caliberkey: "390"
manufacturers: ["intex"]
manufacturers_weight: 390
categories: ["movements","movements_i","movements_i_intex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_DAU_2.jpg"
    description: "Anker Damenuhr"
---
Lorem Ipsum