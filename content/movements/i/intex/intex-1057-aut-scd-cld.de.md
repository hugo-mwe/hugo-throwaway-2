---
title: "Intex 1057 AUT SCD CLD"
date: 2013-10-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Intex 1057","Intex","Willi Friesinger","Robot-Automatic","25 Jewels","Automatic","Handaufzug","Modul","Datum"]
description: "Intex 1057 - ein Automaticwerk aus Deutschland, das auf einem Handaufzugswerk basiert und mit dem \"Robot-Automatic\"-Modul zur echten Automatic komplettiert wurde"
abstract: "Ein deutsches Automaticwerk, das aus einem Handaufzugswerk mit raffiniert konstruierter Datumsindikation besteht, auf das das \"Robot-Automatic\"-Modul gesetzt wurde."
preview_image: "intex_1057_aut_scd_cld-mini.jpg"
image: "Intex_1057_AUT_SCD_CLD.jpg"
movementlistkey: "intex"
caliberkey: "1057 AUT SCD CLD"
manufacturers: ["intex"]
manufacturers_weight: 1057
categories: ["movements","movements_i","movements_i_intex"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>Das vorliegende Werk kam mit starken Gebrauchsspuren und gelöstem Automaticmodul und durch die offenbar länger andauernde Ablösung metallischen Abriebe ins Labor und wurde gereinigt und geölt. Lediglich auf eine Reinigung der Unruh wurde verzichtet, da es erfahrungsgemäß dabei zu nicht zu entfernender Magnetisierung der Spirale kommt.</p><p>Der Zentralsekundenzeiger fehlte; die Achse ist im vordersten Teil abgebrochen.</p>
timegrapher_old: 
  description: |
    Das Werk lief liegend optimal, hängend zeigte es jedoch starken Nachgang in drei und noch tragbaren Vorgang in einer Lage:
  images:
    ZO: Zeitwaage_Intex_1057_AUT_SCD_CLD_ZO.jpg
    ZU: Zeitwaage_Intex_1057_AUT_SCD_CLD_ZU.jpg
    3O: Zeitwaage_Intex_1057_AUT_SCD_CLD_3O.jpg
    6O: Zeitwaage_Intex_1057_AUT_SCD_CLD_6O.jpg
    9O: Zeitwaage_Intex_1057_AUT_SCD_CLD_9O.jpg
    12O: Zeitwaage_Intex_1057_AUT_SCD_CLD_12O.jpg
  values:
    ZO: "+-0"
    ZU: "+-0"
    3O: "-70"
    6O: "-170"
    9O: "-90"
    12O: "+15"
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1057.jpg"
    description: "Anker Automatic Herrenuhr"
---
Lorem Ipsum