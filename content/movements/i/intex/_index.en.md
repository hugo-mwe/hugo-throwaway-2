---
title: "Intex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "en"
keywords: ["FHF","Fontainemelon","Font","Schweiz"]
categories: ["movements","movements_i"]
movementlistkey: "intex"
abstract: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
description: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
---
(Intex-Uhrenfabriken GmbH Willi Friesinger, Pforzheim, Germany)
{{< movementlist "intex" >}}

{{< movementgallery "intex" >}}