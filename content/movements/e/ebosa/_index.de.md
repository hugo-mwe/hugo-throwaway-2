---
title: "Ebosa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "ebosa"
description: ""
abstract: "(Ebosa Trading AG, Grenchen, Schweiz)"
---
(Ebosa Trading AG, Grenchen, Schweiz)
{{< movementlist "ebosa" >}}

{{< movementgallery "ebosa" >}}