---
title: "Ebosa 94"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ebosa 94","Ebosa","caliber 94","pin lever","pier construction","center second","15 Jewels","15 Rubis","15 Steine","Grenchen"]
description: "Ebosa 94 - An early center second movement with pin lever escapement and 15 jewels."
abstract: "An early pin lever movement with center second and 15 jewels"
preview_image: "ebosa_94-mini.jpg"
image: "Ebosa_94.jpg"
movementlistkey: "ebosa"
caliberkey: "94"
manufacturers: ["ebosa"]
manufacturers_weight: 94
categories: ["movements","movements_e","movements_e_ebosa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_DAU_4_Ebosa_94.jpg"
    description: "Anker ladies' watch"
---
Lorem Ipsum