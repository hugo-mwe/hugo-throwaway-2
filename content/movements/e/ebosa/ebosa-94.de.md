---
title: "Ebosa 94"
date: 2012-12-29T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ebosa 94","Ebosa","Kaliber 94","Stiftanker","Pfeilerwerk","Pfeileraufbau","Zentralsekunde","15 Jewels","15 Rubis","15 Steine","Grenchen"]
description: "Ebosa 94 - Ein frühes Zentralsekundenwerk mit Stiftankerhemmung und 15 Steinen"
abstract: "Ein frühes Zentral-sekundenwerk mit Stiftanker-hemmung und 15 Steinen"
preview_image: "ebosa_94-mini.jpg"
image: "Ebosa_94.jpg"
movementlistkey: "ebosa"
caliberkey: "94"
manufacturers: ["ebosa"]
manufacturers_weight: 94
categories: ["movements","movements_e","movements_e_ebosa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_DAU_4_Ebosa_94.jpg"
    description: "Anker Damenuhr"
---
Lorem Ipsum