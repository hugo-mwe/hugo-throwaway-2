---
title: "Ebosa 23"
date: 2013-03-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ebosa 23","Ebosa","15 Jewels","15 Rubis","pin lever","Ebosa SA","KIF Protechoc","shock protection"]
description: "Ebosa 23: One of the nicest pin lever movements with odd construction details and a shock protection"
abstract: "One of the nicest an most unusual pin lever movement. It features a fake screw balance, decoration stripes, a KIF protechoc shock protection and a very unusual construction."
preview_image: "ebosa_23-mini.jpg"
image: "Ebosa_23.jpg"
movementlistkey: "ebosa"
caliberkey: "23"
manufacturers: ["ebosa"]
manufacturers_weight: 23
categories: ["movements","movements_e","movements_e_ebosa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen described here was more or less working, but needed (and got) a full service.
links: |
  * [Watch-Wiki: Ebosa S.A.](http://watch-wiki.org/index.php?title=Ebosa_S.A.) (german only)[](http://watch-wiki.org/index.php?title=Ebosa_S.A.)
usagegallery: 
  - image: "z/zurex/Zurex_Ebosa_23.jpg"
    description: "Zurex mens' watch"
timegrapher_old: 
  description: |
    On the timegrapher, the results in horizontal positions were really good, but on the vertical positions it showed very poor results and even got such a low amplitude, that it was no longer possible to measure it. If you look as Position "3 up", you can see, how the change of the position changed not only beating frequency, but also the amplitiude.
  images:
    ZO: Zeitwaage_Ebosa_23_ZO.jpg
    ZU: Zeitwaage_Ebosa_23_ZU.jpg
    3O: Zeitwaage_Ebosa_23_3O.jpg
    6O: Zeitwaage_Ebosa_23_6O.jpg
    9O: Zeitwaage_Ebosa_23_9O.jpg
    12O: Zeitwaage_Ebosa_23_12O.jpg
  values:
    ZO: "+70"
    ZU: "+40"
    3O: "-"
    6O: "-"
    9O: "+240"
    12O: "-"
---
Lorem Ipsum