---
title: "Ebosa 23"
date: 2013-03-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ebosa 23","Ebosa","15 Jewels","15 Rubis","15 Steine","Stiftanker","Ebosa SA","KIF Protechoc","Stoßsicherung"]
description: "Ebosa 23: Eines der optisch ansprechendsten Stiftankerwerke, mit sehr ungewöhnlichem Werksaufbau und  Stoßsicherung"
abstract: "Eines der schönsten und ungewöhnlichsten Stiftankerwerke. Von der Pseudo-Schraubenunruh über Zierschliffe, der KIF Protechoc-Stoßsicherung bis hin zu einem höchst ungewöhnlichen Werksaufbau, zieht dieses Werk alle Register!"
preview_image: "ebosa_23-mini.jpg"
image: "Ebosa_23.jpg"
movementlistkey: "ebosa"
caliberkey: "23"
manufacturers: ["ebosa"]
manufacturers_weight: 23
categories: ["movements","movements_e","movements_e_ebosa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Watch-Wiki: Ebosa S.A.](http://watch-wiki.org/index.php?title=Ebosa_S.A.)[](http://watch-wiki.org/index.php?title=Ebosa_S.A.)
labor: |
  Das vorliegende Werk war mehr oder weniger lauffähig und bekam deswegen einen Vollservice.
usagegallery: 
  - image: "z/zurex/Zurex_Ebosa_23.jpg"
    description: "Zurex Herrenuhr"
timegrapher_old: 
  description: |
    Der Eindruck auf der Zeitwaage war leider weitaus schlechter, als es der Eindruck, den es im liegenden Zustand versprach. Während es in den horizontalen Positionen einen geradezu mustergültigen, schnurgeraden Ausdruck bot (der Vorgang ist beabsichtigt, um den Nachgang in den vertikalen Positionen zu kompensieren), waren die Werte in den vertikalen Positionen teilweise jenseits von gut und böse, sowohl was den Nachgang betraf, als auch die Gangstabilität. Auch die Amplitude brach in den hängenden Position massiv ein - besonders schön zu beobachten in Position "3 oben", wo am Ende keine Messung mehr möglich war.
  images:
    ZO: Zeitwaage_Ebosa_23_ZO.jpg
    ZU: Zeitwaage_Ebosa_23_ZU.jpg
    3O: Zeitwaage_Ebosa_23_3O.jpg
    6O: Zeitwaage_Ebosa_23_6O.jpg
    9O: Zeitwaage_Ebosa_23_9O.jpg
    12O: Zeitwaage_Ebosa_23_12O.jpg
  values:
    ZO: "+70"
    ZU: "+40"
    3O: "-"
    6O: "-"
    9O: "+240"
    12O: "-"
---
Lorem Ipsum