---
title: "Elgin 524"
date: 2010-09-25T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Chatons","Regulierschrauben","Breguet-Spirale","Elgin 524","Elgin","524","17 Jewels","Illinois","USA","8/0","17 Steine"]
description: "Elgin 524 - Ein sehr hochwertiges Großserienwerk aus den USA mit Chatons und Breguetspirale. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: "Ein sehr hochwertiges Großserienwerk aus den USA mit Chatons und Breguetspirale."
preview_image: "elgin_524-mini.jpg"
image: "Elgin_524.jpg"
movementlistkey: "elgin"
caliberkey: "524"
manufacturers: ["elgin"]
manufacturers_weight: 524
categories: ["movements","movements_e","movements_e_elgin"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Elgin 524](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&a0&2uswk&Elgin_524&)
labor: |
  Auf der Zeitwaage verweigerte sich das vorliegende Exemplar leider - Schuld ist eine defekte Unruhspirale, die immer wieder in Kontakt mit den Armen der Unruh gerät und diese dadurch anhält.
---
Lorem Ipsum