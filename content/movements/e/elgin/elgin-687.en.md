---
title: "Elgin 687"
date: 2015-03-30T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Elgin 687","Elgin","manual wind","17 Jewels","Shockmaster","Incabloc"]
description: "Elgin 687 - a common american manual wind movement"
abstract: "A common american manual wind movement from the 1950ies"
preview_image: "elgin_687-mini.jpg"
image: "Elgin_687.jpg"
movementlistkey: "elgin"
caliberkey: "687"
manufacturers: ["elgin"]
manufacturers_weight: 687
categories: ["movements","movements_e","movements_e_elgin_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/elgin/Elgin_Shockmaster_Elgin_687.jpg"
    description: "Elgin Shockmaster mens' watch"
---
Lorem Ipsum