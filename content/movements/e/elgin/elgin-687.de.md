---
title: "Elgin 687"
date: 2015-03-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Elgin 687","Elgin","Handaufzug","17 Jewels","Shockmaster","Incabloc"]
description: "Elgin 687 - ein verbreitetes amerikanisches Handaufzugswerk"
abstract: "Ein verbreitetes amerikanisches Handaufzugswerk aus den 1950er Jahren."
preview_image: "elgin_687-mini.jpg"
image: "Elgin_687.jpg"
movementlistkey: "elgin"
caliberkey: "687"
manufacturers: ["elgin"]
manufacturers_weight: 687
categories: ["movements","movements_e","movements_e_elgin"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/elgin/Elgin_Shockmaster_Elgin_687.jpg"
    description: "Elgin Shockmaster Herrenuhr"
---
Lorem Ipsum