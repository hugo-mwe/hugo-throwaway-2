---
title: "Elgin 524"
date: 2010-09-25T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["chatons","regulating screws","breguet-type hairspring","Elgin 524","Elgin","524","17 Jewels","Illinois","USA","8/0"]
description: "Elgin 524 - A very fine mass produced US movement with chatons and breguet-type hairspring. Detailed description with photos, video and data sheet."
abstract: "A very fine mass produced US movement with chatons and breguet-type hairspring."
preview_image: "elgin_524-mini.jpg"
image: "Elgin_524.jpg"
movementlistkey: "elgin"
caliberkey: "524"
manufacturers: ["elgin"]
manufacturers_weight: 524
categories: ["movements","movements_e","movements_e_elgin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  It was not possible to test the specimen shown here on the timegrapher, since the hairspring is a bit broken and often gets into contact with the arms of the balance wheel, bringing it to halt.
links: |
  * [Ranfft Uhren: Elgin 524](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&a0&2uswk&Elgin_524&)
---
Lorem Ipsum