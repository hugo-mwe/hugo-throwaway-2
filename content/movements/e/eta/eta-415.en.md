---
title: "ETA 415"
date: 2016-02-14T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 415","cylinder movement","cylinder","ladies' watch","ellipse","elliptical","swiss","Switzerland"]
description: "ETA 415 - A very old, elliptically shaped cylinder movement"
abstract: "A very old, elliptically shaped cylinder movement from ETA"
preview_image: "eta_415-mini.jpg"
image: "ETA_415.jpg"
movementlistkey: "eta"
caliberkey: "415"
manufacturers: ["eta"]
manufacturers_weight: 415
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here was lacking a few parts of the winding and time setting mechanism, and since it's a cylinder movement, it could not be measured on a modern timegrapher.
---
Lorem Ipsum