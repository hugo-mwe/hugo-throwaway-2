---
title: "ETA 355"
date: 2009-10-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 355","ETA","355","8 Jewels","swiss","switzerland","cylinder escapement"]
description: "ETA 355"
abstract: ""
preview_image: "eta_355-mini.jpg"
image: "ETA_355.jpg"
movementlistkey: "eta"
caliberkey: "355"
manufacturers: ["eta"]
manufacturers_weight: 355
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "g/gala/Gala_DAU_Zifferblatt.jpg"
    description: "Gala ladies' watch  (only dial)"
---
Lorem Ipsum