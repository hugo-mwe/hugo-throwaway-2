---
title: "ETA"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "eta"
description: ""
abstract: "(ETA SA Manufacture Horlogère Suisse, Grenchen, Switzerland)"
---
(ETA SA Manufacture Horlogère Suisse, Grenchen, Switzerland)
{{< movementlist "eta" >}}

{{< movementgallery "eta" >}}