---
title: "ETA 2442"
date: 2009-09-25T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA 2442","ETA","2442","17 Jewels","Grenchen","Schweiz","17 Steine"]
description: "ETA 2442"
abstract: ""
preview_image: "eta_2442-mini.jpg"
image: "ETA_2442.jpg"
movementlistkey: "eta"
caliberkey: "2442"
manufacturers: ["eta"]
manufacturers_weight: 2442
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Uhrmacher Knops: ETA 2442](http://www.uhrmacher-knops.de/seiten/ArchivReparaturen/2008/0108/xx08.asp) (Bildbeschreibung der Reparatur eines ETA 2442)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks!</p>
timegrapher_old: 
  images:
    ZU: Zeitwaage_ETA_2442_ZU.jpg
usagegallery: 
  - image: "a/amado/Amado_DAU.jpg"
    description: "Amado Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum