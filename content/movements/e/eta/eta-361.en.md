---
title: "ETA 361"
date: 2018-03-24T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 361","ETA","361","15 Jewels","Grenchen","swiss","switzerland"]
description: "ETA 361: An early ladies' watch pallet lever movement"
abstract: "An early ladies' watch movement from a time, when ladies' wrist watches slowly became popular"
preview_image: "eta_361-mini.jpg"
image: "ETA_361.jpg"
movementlistkey: "eta"
caliberkey: "361"
manufacturers: ["eta"]
manufacturers_weight: 361
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/Unknown_DAU_ETA_361.jpg"
    description: "anonymous ladies' watch"
  - image: "anonym/DU_Arcadia_ETA_361.jpg"
    description: "unmarked silver ladies' watch"
---
Lorem Ipsum