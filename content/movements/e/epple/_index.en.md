---
title: "Epple"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: ["Epple","Julius Epple K.G.","Pforzheim","Germany","caliber","movement","wristwatch"]
categories: ["movements","movements_e"]
movementlistkey: "epple"
description: "Movements of Julius Epple K.G. of Pforzheim, Germany"
abstract: "Movements of Julius Epple K.G. of Pforzheim, Germany"
---
(Julius Epple K.G., Pforzheim, Germany)
{{< movementlist "epple" >}}

{{< movementgallery "epple" >}}