---
title: "EB 8461"
date: 2014-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["EB 8461","EB","Ebauches Bettlach","Stiftanker","1 Stein","Roskopf","Handaufzug","Digital"]
description: "EB 8461 - Ein konstruktiv etwas ungewöhnlich gebautes Stiftankerwerk, das auch mit einer digitalen Zeitanzeige-Indikation versehen war"
abstract: "Ein konstruktiv etwas ungewöhnlich gebautes Stiftankerwerk, das auch mit einer digitalen Zeitanzeige-Indikation versehen war."
preview_image: "eb_8461-mini.jpg"
image: "EB_8461.jpg"
movementlistkey: "eb"
caliberkey: "8461"
manufacturers: ["eb"]
manufacturers_weight: 8461
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk war praktisch neu, aber vollkommen verharzt und durch einen Reparaturversuch mit verölter/verdreckter und verbogener Unruhspirale versehen. Durch einen Komplettservice konnte es wieder zum Leben erweckt werden, allerdings verhindert die beschädigte Unruhspirale ein sauberes Ablesen der Gangwerte auf der Zeitwaage. Dennoch konnte es auf einen Vorgang von unter einer Minute pro Tag reguliert werden, was für ein Stiftankerwerk mit beschädigter Spirale ein durchaus gutes Ergebnis ist.
usagegallery: 
  - image: "e/eloga/Eloga_DAU_EB_8461_dig.jpg"
    description: "Eloga Damenuhr"
---
Lorem Ipsum