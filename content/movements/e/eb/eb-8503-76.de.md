---
title: "EB 8503-76"
date: 2010-02-18T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Stiftankerwerk","direkt angetriebene Zentralsekunde","indirekt angetriebenes Minutenrad","EB 8503-76","EB","8503-76","17 Jewels","Ebauches Bettlach","Schweiz","17 Steine"]
description: "EB 8503-76 - Ein 17-steiniges Einweg-Werk aus der Schweiz. Detaillierte Beschreibung mit Bildern, Film, Makro-Aufnahmen, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "eb_8503-76-mini.jpg"
image: "EB_8503-76.jpg"
movementlistkey: "eb"
caliberkey: "8503-76"
manufacturers: ["eb"]
manufacturers_weight: 850376
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [US Patent 3,896,614](https://books.google.com/patents/US3896614) (Patentschrift der Unruhbrücke)
  * [US Patent 4,291,401](https://books.google.com/patents/US4291401) (Patentschrift der Zifferblattbefestigung dieses Werks)
labor: |
  Das vorliegende Exemplar, das fast ladenneu wirkte, kam komplett verharzt in die Werkstatt. Da es sich nicht auseinandernehmen läßt, mußte es komplett gereinigt werden, was leider der Aufdruck des Datumsrings nicht überlebte.<br />
  Das Werk selber lief nach der Reinigung und anschließenden Ölung, gab aber bereits nach wenigen Tagen wieder den Geist auf.
usagegallery: 
  - image: "t/trice/Trice_HAU_Zifferblatt_EB_8503-76.jpg"
    description: "Trice Herrenuhr  (ohne Gehäuse)"
timegrapher_old: 
  description: |
    Wie zu erwarten war, zeigen die Zeitwaagenausdrucke sehr gleichmäßige Linien, was auf eine geringe Abnutzung des Werks hindeutet. Besonders auffällig ist auch der verschwindend kleine Abfallfehler - hier konnte das Werk wohl schon von der Roboter-Technik profitieren. Die Abweichungen waren zwar absolut gesehen recht groß, hätten sich aber mit einer Mittelwertregulierung gut in den Griff bekommen lassen; die einzelnen Lagenabweichungen sind für ein Werk dieser unteren Klasse durchaus vorzeigbar.
  images:
    ZO: Zeitwaage_EB_8503-76_ZO.jpg
    ZU: Zeitwaage_EB_8503-76_ZU.jpg
    3O: Zeitwaage_EB_8503-76_3O.jpg
    6O: Zeitwaage_EB_8503-76_6O.jpg
    9O: Zeitwaage_EB_8503-76_9O.jpg
    12O: Zeitwaage_EB_8503-76_12O.jpg
  values:
    ZO: "-32"
    ZU: "-10"
    3O: "-90"
    6O: "-60"
    9O: "-5"
    12O: "-60"
---
Lorem Ipsum