---
title: "EB 1297"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["EB 1297","EB","1297","Ebauches Bettlach","Bettlach","pin lever","screw balance","Swiss","Switzerland","15 Jewels","15","Rubis"]
description: "EB 1297"
abstract: ""
preview_image: "eb_1297-mini.jpg"
image: "EB_1297.jpg"
movementlistkey: "eb"
caliberkey: "1297"
manufacturers: ["eb"]
manufacturers_weight: 1297
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum