---
title: "EB 1575N"
date: 2015-09-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["EB 1575","EB","Ebauches Bettlach","Stiftanker","1 Jewels","KIF","Automatic","Roskopf"]
description: "EB 1575N - Ein seltenes Stiftanker-Automaticwerk in Roskopf-Bauweise"
abstract: "Das selten zu findende erste Stiftanker-Automaticwerk der schweizer Ebauches Bettlach"
preview_image: "eb_1575n-mini.jpg"
image: "EB_1575N.jpg"
movementlistkey: "eb"
caliberkey: "1575N"
manufacturers: ["eb"]
manufacturers_weight: 1575
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "c/continental/Continental_Automatic_HAU_EB_1575N.jpg"
    description: "Continental Automatic Herrenuhr"
---
Lorem Ipsum