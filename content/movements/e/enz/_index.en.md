---
title: "Enz"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: ["Enz","Uhrenfabrik Enz","Pforzheim","Franz Schnurr"]
categories: ["movements","movements_e"]
movementlistkey: "enz"
abstract: "Movements from the german manufacturer Enz (Uhrenfabrik Enz) in Pforzheim"
description: "Movements from the german manufacturer Enz (Uhrenfabrik Enz) in Pforzheim"
---
(Uhrenfabrik Enz, Franz Schnurr, Pforzheim, Germany)
{{< movementlist "enz" >}}

{{< movementgallery "enz" >}}