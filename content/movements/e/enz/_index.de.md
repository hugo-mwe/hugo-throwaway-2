---
title: "Enz"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: ["Enz","Uhrenfabrik Enz","Franz Schnurr","Pforzheim","Deutschland"]
categories: ["movements","movements_e"]
movementlistkey: "enz"
description: "Uhrwerke der Uhrenfabrik Enz aus Pforzheim"
abstract: "Uhrwerke der Uhrenfabrik Enz aus Pforzheim"
---
(Uhrenfabrik Enz, Franz Schnurr, Pforzheim, Deutschland)
{{< movementlist "enz" >}}

{{< movementgallery "enz" >}}