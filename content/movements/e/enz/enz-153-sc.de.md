---
title: "Enz 153 SC"
date: 2017-04-21T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Enz 153 SC","Franz Schnurr","Pforzheim","17 Jewels","17 Rubis","17 Steine","Handaufzug","Zentralsekunde","Roskopf"]
description: "Enz 153 SC - ein sehr einfaches Palettenankerwerk in Roskopf-Bauweise"
abstract: "Ein sehr einfach ausgeführes Palettenankerwerk, das seine Verwandschaft mit Roskopf-Stiftankerwerken nicht verleugnen kann."
preview_image: "enz_153_sc-mini.jpg"
image: "Enz_153_SC.jpg"
movementlistkey: "enz"
caliberkey: "153 SC"
manufacturers: ["enz"]
manufacturers_weight: 153
categories: ["movements","movements_e","movements_e_enz"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum