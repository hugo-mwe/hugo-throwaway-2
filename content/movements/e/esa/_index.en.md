---
title: "ESA"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "esa"
abstract: "Uhrwerke des schweizer Herstellers ESA aus Neuchatel"
description: "Uhrwerke des schweizer Herstellers ESA aus Neuchatel"
---
(Ebauches S.A., Neuchatel, Switzerland)
{{< movementlist "esa" >}}

{{< movementgallery "esa" >}}