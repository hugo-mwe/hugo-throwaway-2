---
title: "Villeret"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["v"]
language: "en"
keywords: ["Villeret","Aurore Villeret","Aurore","Switzerland","Swiss"]
categories: ["movements","movements_v"]
movementlistkey: "villeret"
abstract: "Movements of the swiss manufacturer Villeret (Aurore Villeret)"
description: "Movements of the swiss manufacturer Villeret (Aurore Villeret)"
---
(Aurore Villeret, Fabrique d'ébauches Bernoises S.A., Villeret, Switzerland)
{{< movementlist "villeret" >}}

{{< movementgallery "villeret" >}}