---
title: "Venus 54"
date: 2011-03-19T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["schweizer Ankerhemmung","Venus 54","Venus","54","15 Jewels","Moutier","Münster","Schweiz","15 Steine","Formwerk","Ellipse"]
description: "Venus 54 - Ein ellipsenförmiges Formwerk mit 15 Steinen. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: ""
preview_image: "venus_54-mini.jpg"
image: "Venus_54.jpg"
movementlistkey: "venus"
caliberkey: "54"
manufacturers: ["venus"]
manufacturers_weight: 54
categories: ["movements","movements_v","movements_v_venus"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Zifferblatt_Venus_54.jpg"
    description: "anonyme Damenuhr"
---
Lorem Ipsum