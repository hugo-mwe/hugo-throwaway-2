---
title: "Dugena"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["Dugena","Deutschland"]
categories: ["movements","movements_d"]
movementlistkey: "dugena"
abstract: "fremde Uhrwerke, die Dugena, versehen mit eigener Kalibernummer, verwendete"
description: "fremde Uhrwerke, die Dugena, versehen mit eigener Kalibernummer, verwendete"
---
(Deutsche Uhrengenossenschaft Alpina, Eisenach/Berlin/Darmstadt, Deutschland)

Die Dugena stellte keine eigenen Werke her, sondern verwendete Uhrwerke von Fremdherstellern, gab ihnen aber eigene Kaliberbezeichnungen. Aus diesem Grund, und weil zudem sämtliche Dugena-Werke auch eigens in den Flume-Werksuchern gelistet sind, werden an dieser Stelle ausnahmsweise auch Fremdwerke aufgelistet.
{{< movementlist "dugena" >}}

{{< movementgallery "dugena" >}}