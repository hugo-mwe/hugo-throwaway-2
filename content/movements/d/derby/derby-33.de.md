---
title: "Derby 33"
date: 2009-05-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Derby 33","Derby","D33","33","15 Jewels","mechanisch","Handaufzug","Schweiz","Steine"]
description: "Derby 33"
abstract: ""
preview_image: "derby_33-mini.jpg"
image: "Derby_33.jpg"
movementlistkey: "derby"
caliberkey: "33"
manufacturers: ["derby"]
manufacturers_weight: 33
categories: ["movements","movements_d","movements_d_derby"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mobilia/Mobilia_Preserval_Incassable.jpg"
    description: "Mobilia Preserval Incassable Herrenuhr"
---
Lorem Ipsum