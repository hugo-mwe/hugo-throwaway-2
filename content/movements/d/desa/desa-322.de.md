---
title: "Desa 322"
date: 2016-02-09T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Desa 322","Desa","Ebauches Desa","Grenchen","Schweiz","Swiss","Stiftanker","1 Jewel","1 Rubis","1 Stein","Roskopf"]
description: "Desa 322 - Ein einfaches, dreizehnliniges Stiftankerwerk in Roskopf-Bauweise aus der Schweiz"
abstract: "Ein dreizehnliniges Stiftankerwerk in Roskopf-Bauweise aus der Schweiz"
preview_image: "desa_322-mini.jpg"
image: "Desa_322.jpg"
movementlistkey: "desa"
caliberkey: "322"
manufacturers: ["desa"]
manufacturers_weight: 322
categories: ["movements","movements_d","movements_d_desa"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam in gutem Zustand und bekam nur einen minimalen Service, sowie eine leichte Justierung, da es anfangs fast vier Minuten pro Tag zu schnell lief.
donation: "Dieses Werk wurde von [Günter G](/supporters/guenter-g/). gespendet. Vielen Dank für die Unterstützung des Uhrwerkearchivs!"
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "+34"
    3O: "-40"
    6O: "-19"
    9O: "-16"
    12O: "-9"
  amplitudes:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: "0.6"
    3O: "0.8"
    6O: "0.8"
    9O: ""
    12O: "1.7"
usagegallery: 
  - image: "l/lucerne/Lucerne_Umhaengeuhr.jpg"
    description: "Lucerne Umhängeuhr"
---
Lorem Ipsum