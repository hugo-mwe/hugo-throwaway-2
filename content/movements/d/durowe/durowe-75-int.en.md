---
title: "DuRoWe 75 (INT)"
date: 2010-01-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ebauches S.A.","INT","Duro-Shock","pallet lever","yoke winding system","Durowe 75","Durowe","75","17 Jewels","Pforzheim","Hummel","Laco","Lacher","germany","form movement"]
description: "Durowe 75 - A typical ladies` watch form movement."
abstract: ""
preview_image: "durowe_75-mini.jpg"
image: "Durowe_75.jpg"
movementlistkey: "durowe"
caliberkey: "75 (INT)"
manufacturers: ["durowe"]
manufacturers_weight: 75
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    The specimen shown here came completely gummed into the studio, was disassembled completely, cleaned, lubricated and re-assembled afterwards. On the timegrapher, it shows good results, and if there were not the two runaways on the positions "3 up" and "dial down", it would even run within the chronometer specs. For a form movement of that small size, that's a great performance!
  images:
    ZO: Zeitwaage_Durowe_75_ZO.jpg
    ZU: Zeitwaage_Durowe_75_ZU.jpg
    3O: Zeitwaage_Durowe_75_3O.jpg
    6O: Zeitwaage_Durowe_75_6O.jpg
    9O: Zeitwaage_Durowe_75_9O.jpg
    12O: Zeitwaage_Durowe_75_12O.jpg
links: |
  <p>* [Ranfft Uhren: Durowe 75](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Durowe_75) (in the photos, the AS 1977 version is shown)</p><p>This movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum