---
title: "DuRoWe 270"
date: 2017-01-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 270","Deutsche UhrenRohwerke","Automatic","Schraubenunruh","Zentralsekunde","winzig"]
description: "DuRoWe 270 - das einzige Damenautomatic-Werk der DuRoWe ist gleichzeitig eines der kleinsten Damenautomatic-Werke überhaupt"
abstract: " Das einzige Damenautomatic-Werk von DuRoWe und gleichzeitig eines der kleinsten Damenautomatic-Werke auf dem Markt."
preview_image: "durowe_270-mini.jpg"
image: "Durowe_270.jpg"
movementlistkey: "durowe"
caliberkey: "270"
manufacturers: ["durowe"]
manufacturers_weight: 270
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    Die gemessenen Werte zeigen die Performance dieses Werks in unrevidiertem Zustand, nach vielen Jahren Einsatz und nach vermutlich längerer Lagerzeit:
  rates:
    ZO: "+45"
    ZU: "0"
    3O: "+30"
    6O: "-64"
    9O: "+15"
    12O: "+90"
  amplitudes:
    ZO: "254"
    ZU: "160"
    3O: "143"
    6O: "143"
    9O: "143"
    12O: "140"
  beaterrors:
    ZO: "1.1"
    ZU: "0.9"
    3O: "0.9"
    6O: "1.2"
    9O: "0.9"
    12O: "0.6"
usagegallery: 
  - image: "k/kh/KH_Automatic_DAU_Durowe_270.jpg"
    description: "KH Automatic Damenuhr"
  - image: "r/roxy/Roxy_DAU_Zifferblatt_Durowe_270.jpg"
    description: "Roxy Damenuhr  (Gehäuse fehlt)"
labor: |
  Das hier vorgestellte Exemplar kam mit deutlichen Gebrauchsspuren, aber in lauffähigem Zustand ins Labor, so daß auf einen Service verzichtet wurde.
---
Lorem Ipsum