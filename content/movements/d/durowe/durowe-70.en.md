---
title: "DuRoWe 70"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Durowe 70","form movement","pallet lever","17 Jewels","17 Rubis","germany"]
description: "Durowe 70"
abstract: ""
preview_image: "durowe_70-mini.jpg"
image: "Durowe_70.jpg"
movementlistkey: "durowe"
caliberkey: "70"
manufacturers: ["durowe"]
manufacturers_weight: 70
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum