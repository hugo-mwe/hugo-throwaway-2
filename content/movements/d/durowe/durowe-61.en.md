---
title: "DuRoWe 61 / Kienzle 070/27"
date: 2010-09-19T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["screw balance","hairspring key","pallet lever","DuRoWe 61","DuRoWe","61","17 Jewels","Pforzheim","Germany","german","manual wind","form movement","Kienzle 070/27"]
description: "DuRoWe 61 - A form movement for ladies' watches with 17 jewels. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "durowe_61-mini.jpg"
image: "Durowe_61.jpg"
movementlistkey: "durowe"
caliberkey: "61"
manufacturers: ["durowe"]
manufacturers_weight: 61
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    The specimen shown here came in good shape and strongly running into the lab. Due to its small size, and the difficult cleaning process, it was not cleaned at all. You can get a good impression of the performance of a movement, which was not serviced for years.   On tiny movements, good timing results are hard to achieve. When, additionally, the last service was long ago, it's quite impossible, and that is exactly, what you can see here: In all positions, the movement shows large deviation numbers, but in the mean (e.g. when being worn), they are reduced to fair amounts.
  images:
    ZO: Zeitwaage_Durowe_61_ZO.jpg
    ZU: Zeitwaage_Durowe_61_ZU.jpg
    3O: Zeitwaage_Durowe_61_3O.jpg
    6O: Zeitwaage_Durowe_61_6O.jpg
    9O: Zeitwaage_Durowe_61_9O.jpg
    12O: Zeitwaage_Durowe_61_12O.jpg
usagegallery: 
  - image: "z/zentra/Zentra_DAU_Durowe_61.jpg"
    description: "ZentRa ladies' watch"
---
Lorem Ipsum