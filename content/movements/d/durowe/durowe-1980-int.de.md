---
title: "DuRoWe 1980 (INT)"
date: 2018-03-02T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Durowe 1980","INT 1980","AS 1977-5","17 Jewels","17 Rubis","17 Steine","Handaufzug","Palettenanker","Formwerk"]
description: "DuRoWe 1980 (INT) - Eines der letzten Handaufzugswerke von DuRoWe"
abstract: "Eines der letzten DuRoWe-Handaufzugswerke in 5 1/2 Linien."
preview_image: "durowe_1980_int-mini.jpg"
image: "Durowe_1980_INT.jpg"
movementlistkey: "durowe"
caliberkey: "1980 (INT)"
manufacturers: ["durowe"]
manufacturers_weight: 1980
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam gut gebraucht, aber noch in hinreichend gutem Zustand ins Labor und bekam daher nur eine einfache Reinigung und Ölung verpaßt.
usagegallery: 
  - image: "o/osco/Osco_DU_INT_1980.jpg"
    description: "Osco Damenuhr"
timegrapher_old: 
  rates:
    ZO: "+2"
    ZU: "0"
    3O: "-3"
    6O: "0"
    9O: "+28"
    12O: "+29"
  amplitudes:
    ZO: "235"
    ZU: "256"
    3O: "217"
    6O: "214"
    9O: "209"
    12O: "213"
  beaterrors:
    ZO: "0.2"
    ZU: "0.1"
    3O: "0.2"
    6O: "0.0"
    9O: "0.5"
    12O: "0.1"
donation: "Ein weiteres Exemplar dieses Werks wurde als Ersatzteilträger von [Klaus Brunnemer](/supporters/klaus-brunnemer/) gespendet. Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs."
---
Lorem Ipsum