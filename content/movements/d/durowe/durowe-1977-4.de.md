---
title: "DuRoWe 1977-4 (INT)"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 1977-4","DuRoWe","Deutsche UhrenRohwerke","INT","INT 1977-4","INT 1977","1977-4","Durowe","Uhr","Uhren","Armbanduhr","Armbanduhren","Damenuhr","Kaliber","Werk","17","Steine","Jewels","Handaufzug"]
description: "DuRoWe 1977-4 (INT)"
abstract: ""
preview_image: "durowe_1977_4-mini.jpg"
image: "Durowe_1977-4.jpg"
movementlistkey: "durowe"
caliberkey: "1977-4"
manufacturers: ["durowe"]
manufacturers_weight: 19774
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum