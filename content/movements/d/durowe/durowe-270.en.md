---
title: "DuRoWe 270"
date: 2017-02-03T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["DuRoWe 270","Deutsche UhrenRohwerke","selfwinding","screw balance","center second","small"]
description: "DuRoWe 270 - the only selfwinding movement from DuRoWe for ladies' watches is at the same time, one of the smallest selfwindig movements ever."
abstract: " The only selfwinding movement from DuRoWe for ladies' watches and also one of the smallest selfwinding movements ever."
preview_image: "durowe_270-mini.jpg"
image: "Durowe_270.jpg"
movementlistkey: "durowe"
caliberkey: "270"
manufacturers: ["durowe"]
manufacturers_weight: 270
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "k/kh/KH_Automatic_DAU_Durowe_270.jpg"
    description: "KH Automatic ladies' watch"
  - image: "r/roxy/Roxy_DAU_Zifferblatt_Durowe_270.jpg"
    description: "Roxy ladies' watch  (case missing)"
timegrapher_old: 
  description: |
    The values show the performance of this movement in unserviced condition, after many years of use and probably even longer storage time:
  rates:
    ZO: "+45"
    ZU: "0"
    3O: "+30"
    6O: "-64"
    9O: "+15"
    12O: "+90"
  amplitudes:
    ZO: "254"
    ZU: "160"
    3O: "143"
    6O: "143"
    9O: "143"
    12O: "140"
  beaterrors:
    ZO: "1.1"
    ZU: "0.9"
    3O: "0.9"
    6O: "1.2"
    9O: "0.9"
    12O: "0.6"
labor: |
  The specimen shown here was well used, but still working, when it came into the lab, so no service was made.
---
Lorem Ipsum