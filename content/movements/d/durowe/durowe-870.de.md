---
title: "DuRoWe 870 / Timex M84"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Laco 870","Timex 84","Laco","Timex Electric","Timex M84","M84","Durowe","Electric","7","Steine","Jewels","elektromechanisch","Batterie"]
description: "Durowe 870 / Timex M84"
abstract: ""
preview_image: "durowe_870-mini.jpg"
image: "Durowe_870.jpg"
movementlistkey: "durowe"
caliberkey: "870"
manufacturers: ["durowe"]
manufacturers_weight: 870
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum