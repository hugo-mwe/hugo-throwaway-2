---
title: "DuRoWe 89"
date: 2009-11-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 89","DuRoWe","89","Deutsche Uhren Rohwerke","17 Jewels","Deutschland","17 Steine","Formwerk"]
description: "DuRoWe 89"
abstract: ""
preview_image: "durowe_89-mini.jpg"
image: "Durowe_89.jpg"
movementlistkey: "durowe"
caliberkey: "89"
manufacturers: ["durowe"]
manufacturers_weight: 89
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum