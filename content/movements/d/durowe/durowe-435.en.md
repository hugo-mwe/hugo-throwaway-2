---
title: "DuRoWe 435"
date: 2015-01-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["DuRoWe 435","DuRoWe","manual wind","date","center second","17 Jewels","17 Rubis","screw balance","Duroswing"]
description: "DuRoWe 435 - A rather rate manual wind movement with date indication, made by the Deutsche Uhren-Rohwerke in Germany in the mid 1950ies."
abstract: "This nice german manual wind movement with it's unique shape of the bridges dates from the mid 1950ies."
preview_image: "durowe_435-mini.jpg"
image: "Durowe_435.jpg"
movementlistkey: "durowe"
caliberkey: "435"
manufacturers: ["durowe"]
manufacturers_weight: 435
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here came in good condition in the lab, but for a best possible presentation, it was serviced, cleaned and regulated on the timegrapher.</p><p>On the timegrapher, it shows quite well figures, but they are very much position depending. Probably the balance wheel is aged.</p><p>Take the amplitude numbers with a grain of salt, since the correct lift angle is unknown and the usual default of 52° was used.</p><p>\{zeitwaage\}ZO=+20|2.4|210,ZU=+8|2.2|208,12O=-4|3.0|175,3O=-30|3.0|179,6O=-185|2.7|171,9O=-170|2.8|162\{/zeitwaage\}</p>
usagegallery: 
  - image: "a/adora/Adora_HAU_Durowe_435.jpg"
    description: "Adora mens' watch"
donation: "This movement, in combination with the Adora watch is a kind donation of [Gundula Stein](/supporters/of-gundula-stein/). Thank you very much!"
---
Lorem Ipsum