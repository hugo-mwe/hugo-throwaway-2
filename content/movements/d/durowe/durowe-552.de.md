---
title: "DuRoWe 552"
date: 2015-11-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Durowe 552","552","Automatic","Wippenwechsler","25 Jewels","25 Rubis","25 Steine","Rotor","Duromat"]
description: "Durowe 552 - Das erste Automaticwerk der legendären \"Duromat-Serie\" der deutschen UhrenRohwerke aus Pforzheim"
abstract: "Der Vater und die erste Ausführung aller \"Duromat\"-Werke aus Pforzheim, mit dem 1952 eine lang anhaltende Erfolgsgeschichte begann. "
preview_image: "durowe_552-mini.jpg"
image: "Durowe_552.jpg"
movementlistkey: "durowe"
caliberkey: "552"
manufacturers: ["durowe"]
manufacturers_weight: 552
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Exemplar kam in einem stark verdreckten Gehäuse, sowie mit blockierter Automatic und bescheidenen Gangwerten in die Werkstatt. Nach erfolgter Reinigung läuft das Werk wieder passabel, nur der Zahnschaden im Automatic-Aufzug konnte nicht mehr behoben werden.
timegrapher_old: 
  rates:
    ZO: "+5"
    ZU: "+12"
    3O: "+47"
    6O: "+36"
    9O: "+29"
    12O: "-30"
  amplitudes:
    ZO: "223"
    ZU: "226"
    3O: "175"
    6O: "170"
    9O: "176"
    12O: "180"
  beaterrors:
    ZO: "3.0"
    ZU: "3.3"
    3O: "4.6"
    6O: "4.8"
    9O: "3.8"
    12O: "4.3"
usagegallery: 
  - image: "e/Ebs/EBS_HAU_Durowe_552.jpg"
    description: "EBS Herrenuhr"
  - image: "l/licht/Licht_Automatic_HAU_Durowe_552.jpg"
    description: "Licht Automatic Herrenuhr"
---
Lorem Ipsum