---
title: "DuRoWe 61 / Kienzle 070/27"
date: 2010-09-19T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Schraubenunruh","Spiralschlüssel","Palettenankers","DuRoWe 61","DuRoWe","61","17 Jewels","Pforzheim","Deutschland","Handaufzug","Formwerk","Kienzle 070/27"]
description: "DuRoWe 61 - Ein Formwerk für Damenuhren mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "durowe_61-mini.jpg"
image: "Durowe_61.jpg"
movementlistkey: "durowe"
caliberkey: "61"
manufacturers: ["durowe"]
manufacturers_weight: 61
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das hier vorliegende Werk kam in äußerlich einwandfreiem Zustand ins Labor. Wegen seiner geringen Größe und des dadurch diffizilen Reinigungsprozesses wurde auf eine Reinigung verzichtet. Auf diese Weise bekommt man ein gutes Bild davon, wie Gangwerte aussehen können, wenn ein Werk jahrelang ununterbrochen im Einsatz ist.
usagegallery: 
  - image: "z/zentra/Zentra_DAU_Durowe_61.jpg"
    description: "ZentRa Damenuhr"
timegrapher_old: 
  description: |
    Gute Gangwerte sind bei kleinen Uhrwerken eher schwierig zu erreichen; wenn das Werk dann auch noch jahrelang (jahrzehntelang?) ohne Service läuft, dann ist es fast unmöglich, ein sauberes Gangbild zu erhalten. Nichts anderes zeigt das hier vorliegende Werk: Mit diesen Werten, die in alle Richtungen streuen, ist kein Staat zu machen, dennoch dürfte das Werk im Mittel(!) am Handgelenk relativ genau laufen, da sich die unterschiedlichen Lagenunterschiede im Tragen mehr oder weniger stark ausgleichen.
  images:
    ZO: Zeitwaage_Durowe_61_ZO.jpg
    ZU: Zeitwaage_Durowe_61_ZU.jpg
    3O: Zeitwaage_Durowe_61_3O.jpg
    6O: Zeitwaage_Durowe_61_6O.jpg
    9O: Zeitwaage_Durowe_61_9O.jpg
    12O: Zeitwaage_Durowe_61_12O.jpg
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum