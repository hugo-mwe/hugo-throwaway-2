---
title: "Diehl 620"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Diehl 620","Diehl","620","7 Jewels","Junghans","flat","germany"]
description: "Diehl 620"
abstract: ""
preview_image: "diehl_620-mini.jpg"
image: "Diehl_620.jpg"
movementlistkey: "diehl"
caliberkey: "620"
manufacturers: ["diehl"]
manufacturers_weight: 620
categories: ["movements","movements_d","movements_d_diehl_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/diehl/Diehl_compact_2.jpg"
    description: "Diehl gents watch"
---
Lorem Ipsum