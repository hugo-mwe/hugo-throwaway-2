---
title: "Diehl 674"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Diehl","674","Diehl 674","17 Rubis","17 Jewels","Germany","Junghans"]
description: "Diehl 674"
abstract: ""
preview_image: "diehl_674-mini.jpg"
image: "Diehl_674.jpg"
movementlistkey: "diehl"
caliberkey: "674"
manufacturers: ["diehl"]
manufacturers_weight: 674
categories: ["movements","movements_d","movements_d_diehl_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum