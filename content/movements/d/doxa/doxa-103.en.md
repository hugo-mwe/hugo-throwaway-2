---
title: "Doxa 103"
date: 2009-04-04T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 2391","Glucydur screw balance","Nivarox-I","Doxa 103","Doxa","103","17 Jewels","Neuchatel","swiss","switzerland"]
description: "Doxa 103 - A high quality swiss movement with 11 1/2 linges. Detailed description with photos, macro image, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "doxa_103-mini.jpg"
image: "Doxa_103.jpg"
movementlistkey: "doxa"
caliberkey: "103"
manufacturers: ["doxa"]
manufacturers_weight: 103
categories: ["movements","movements_d","movements_d_doxa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    The movement came completely gummed into the lab, sealed in a watch wreck (see below) and hence got a full service. Nevertheles, the movement has got a tiny problem: Since the gears are so near to each other and tend to "tumble" a bit, they tend to touch from time to time, slowing down the movement a tiny little bit. It's likely possible, that this problem originates from the bad treatment, it got over the years.   On the timegrapher, which printed here in double precision, the movement performed not bad, but still far beyond its possibilities. While on the horizontal positions, the deviation is pretty low (at max. +15 seconds per day), three of the four vertical position show a bad performance, down to -45 seconds per day. It's quite possible, that this is due to the slightly "tumbling" gears. In the vertical positions, any gear will start tumbling, if no cap jewels are used. And if the gears are not 100% straight, you will see these problems. Unfortunately, the Doxa 103 lacks cap jewels, except one on the escapement wheel.
  images:
    ZO: Zeitwaage_Doxa_103_ZO.jpg
    ZU: Zeitwaage_Doxa_103_ZU.jpg
    3O: Zeitwaage_Doxa_103_3O.jpg
    6O: Zeitwaage_Doxa_103_6O.jpg
    9O: Zeitwaage_Doxa_103_9O.jpg
    12O: Zeitwaage_Doxa_103_12O.jpg
usagegallery: 
  - image: "d/doxa/Doxa_HAU_Doxa_103.jpg"
    description: "Doxa gents watch"
links: |
  * [Ranfft Uhren: ETA 2391](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&ETA_2391) (the base ebauche, ETA 2391)
---
Lorem Ipsum