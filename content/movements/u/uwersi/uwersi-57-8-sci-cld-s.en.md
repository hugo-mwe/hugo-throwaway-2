---
title: "Uwersi 57/8 (SCI CLD pin lever)"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Uwersi 57/8","Uhrenwerke Ersingen","Helmut Epperlein","Vereinigte Uhrenfabriken","Vereinigte Uhrenfabriken Ersingen","VUFE","V.U.F.E","19 Rubis","19 Jewels","pin lever","Germany"]
description: "Uwersi 57/8 with indirect center second and date"
abstract: ""
preview_image: "uwersi_57_8_stiftanker_sci_cld-mini.jpg"
image: "Uwersi_57_8_Stiftanker_SCI_CLD.jpg"
movementlistkey: "uwersi"
caliberkey: "57/8 SCI CLD s"
manufacturers: ["uwersi"]
manufacturers_weight: 570821
categories: ["movements","movements_u","movements_u_uwersi_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Uwersi 57/8](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Uwersi_57_8P)</p><p>This movement was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum