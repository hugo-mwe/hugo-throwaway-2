---
title: "Uwersi"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "de"
keywords: ["Uwersi","V.U.F.E.","VUFE","Ersingen","Helmut Epperlein","Epperlein"]
categories: ["movements","movements_u"]
movementlistkey: "uwersi"
abstract: "Uhrwerke der Uwersi, des Uhren-Werks Ersingen, auch bekannt als Vereinigte Uhrenfabriken Ersingen (V.U.F.E.)"
description: "Uhrwerke der Uwersi, des Uhren-Werks Ersingen, auch bekannt als Vereinigte Uhrenfabriken Ersingen (V.U.F.E.)"
---
(Uhren-Werk Ersingen / Vereinigte Uhrenfabriken Ersingen, Ersingen, Deutschland)
{{< movementlist "uwersi" >}}

{{< movementgallery "uwersi" >}}