---
title: "Uwersi 57/10 old"
date: 2016-01-30T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Uwersi 57/10 alt","Uwersi","57/10","VUFE","Uhrenwerk Ersingen","Vereinigte Uhrenfabriken Ersingen","Zentralsekunde","21 Jewels","21 Rubis","pallet lever","center second"]
description: "Uwersi 57/10 alt (old) - One of the best movements from Uwersi (VUFE)"
abstract: "One of the best movements by Uwersi with pallet lever escapement and lots of cap jewels."
preview_image: "uwersi_57_10_alt-mini.jpg"
image: "Uwersi_57_10_alt.jpg"
movementlistkey: "uwersi"
caliberkey: "57/10 alt"
manufacturers: ["uwersi"]
manufacturers_weight: 571000
categories: ["movements","movements_u","movements_u_uwersi_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here was resinified and had a broken winding mechanism. Although a service was made, the missing spring of the winding mechanism could not be restored and so it kept broken. Because of this, no timegrapher measurements were made.
usagegallery: 
  - image: "a/anker/Anker_HAU_Uwersi_57_10.jpg"
    description: "Anker gents watch"
---
Lorem Ipsum