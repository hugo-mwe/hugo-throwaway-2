---
title: "Uwersi"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "en"
keywords: ["Uwersi","V.U.F.E.","VUFE","Ersingen","Helmut Epperlein","Epperlein"]
categories: ["movements","movements_u"]
movementlistkey: "uwersi"
abstract: "Movements of Uwersi, the Uhren-Werk Ersingen in Germany, also known as Vereinigte Uhrenfabriken Ersingen (V.U.F.E.)"
description: "Movements of Uwersi, the Uhren-Werk Ersingen in Germany, also known as Vereinigte Uhrenfabriken Ersingen (V.U.F.E.)"
---
(Uhren-Werk Ersingen / Vereinigte Uhrenfabriken Ersingen, Ersingen, Germany)
{{< movementlist "uwersi" >}}

{{< movementgallery "uwersi" >}}