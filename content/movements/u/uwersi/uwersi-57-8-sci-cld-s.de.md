---
title: "Uwersi 57/8 (SCI CLD Stiftanker)"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uwersi 57/8","Uhrenwerke Ersingen","Helmut Epperlein","Vereinigte Uhrenfabriken","Vereinigte Uhrenfabriken Ersingen","VUFE","V.U.F.E","19 Rubis","19 Jewels","19 Steine","Stiftanker","Deutschland"]
description: "Uwersi 57/8 mit indirekter Zentralsekunde, Kalender und Stiftanker mit Stahlstiften"
abstract: ""
preview_image: "uwersi_57_8_stiftanker_sci_cld-mini.jpg"
image: "Uwersi_57_8_Stiftanker_SCI_CLD.jpg"
movementlistkey: "uwersi"
caliberkey: "57/8 SCI CLD s"
manufacturers: ["uwersi"]
manufacturers_weight: 570821
categories: ["movements","movements_u","movements_u_uwersi"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
links: |
  * [Ranfft Uhren: Uwersi 57/8](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Uwersi_57_8P)
---
Lorem Ipsum