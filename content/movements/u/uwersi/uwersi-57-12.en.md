---
title: "Uwersi 57/12"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Uwersi 57/12","25 Rubis","25 Jewels","germany","form movement"]
description: "Uwersi 57/12"
abstract: ""
preview_image: "uwersi_57_12-mini.jpg"
image: "Uwersi_57_12.jpg"
movementlistkey: "uwersi"
caliberkey: "57/12"
manufacturers: ["uwersi"]
manufacturers_weight: 571200
categories: ["movements","movements_u","movements_u_uwersi_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_HAU_25_Rubis.jpg"
    description: "Anker gents watch"
---
Lorem Ipsum