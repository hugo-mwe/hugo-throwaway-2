---
title: "UMF 13-33"
date: 2018-03-10T01:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 13-33","UMF 13","Caliber 13","7 Jewels","7 Rubis","Quarz","Quartz"]
description: "UMF 13-33: One of the last inhouse quartz movement from Ruhla, Germany"
abstract: "One of the last inhouse quartz movements from Ruhla, Germany"
preview_image: "umf_13-33-mini.jpg"
image: "UMF_13-33.jpg"
movementlistkey: "umf"
caliberkey: "13-33"
manufacturers: ["umf"]
manufacturers_weight: 1333
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement is a kind donation from [Mario](/supporters/mario/). Thank you very much for the support of the movement archive!"
usagegallery: 
  - image: "r/ruhla/Ruhla_Eurochron_HQU_UMF_13-11.jpg"
    description: "Ruhla Eurochron gents watch model 13-33-34-24"
---
Lorem Ipsum