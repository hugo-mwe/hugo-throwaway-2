---
title: "UMF 23-42"
date: 2017-11-09T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 23-42","UMF 23","pin lever","rocking bar","center second","5 Jewels","5 Rubis","Champion"]
description: "UMF 23-42 - a rare pin lever movement from the 1960ies, made by Ruhla in (Easy) Germany"
abstract: "A rare, only two years produced pin lever movement from the GDR, the predecessor of the world famous UMF 24."
preview_image: "umf_23-42-mini.jpg"
image: "UMF_23-42.jpg"
movementlistkey: "umf"
caliberkey: "23-42"
manufacturers: ["umf"]
manufacturers_weight: 2342
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum