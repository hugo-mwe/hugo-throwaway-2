---
title: "UMF Diva (M 3)"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["UMF 3","UMF M3","Diva","M3","UMF Ruhla","Ruhla","DDR","GDR","15","Steine","Jewels","Handaufzug","Damenuhr","Schraubenunruh"]
description: "UMF 3"
abstract: ""
preview_image: "umf_3-mini.jpg"
image: "UMF_3.jpg"
movementlistkey: "umf"
caliberkey: "Diva"
manufacturers: ["umf"]
manufacturers_weight: 31
categories: ["movements","movements_u","movements_u_umf"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum