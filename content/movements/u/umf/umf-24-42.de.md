---
title: "UMF 24-42"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["UMF","Ruhla","DDR","M24","24-42","2 Steine","2 Rubis","2 Jewels","UMF 24","Zentralsekunde","Zapfenlager"]
description: "DAS Stiftankerwerk aus der ehemaligen DDR, hier in der seltenen, alten Ausführung mit zwei Steinen"
abstract: "DAS Stiftankerwerk aus der ehemaligen DDR, hier in der seltenen, alten Ausführung mit zwei Steinen"
preview_image: "umf_24-42-mini.jpg"
image: "UMF_24_42.jpg"
movementlistkey: "umf"
caliberkey: "24-42"
manufacturers: ["umf"]
manufacturers_weight: 2442
categories: ["movements","movements_u","movements_u_umf"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/ruhla/Ruhla_HAU_UMF_24-42.jpg"
    description: "Ruhla HAU"
timegrapher_old: 
  images:
    ZO: Zeitwaage_UMF_24-42_ZO.jpg
    ZU: Zeitwaage_UMF_24-42_ZU.jpg
    3O: Zeitwaage_UMF_24-42_3O.jpg
    6O: Zeitwaage_UMF_24-42_6O.jpg
    9O: Zeitwaage_UMF_24-42_9O.jpg
    12O: Zeitwaage_UMF_24-42_12O.jpg
  values:
    ZO: "+60"
    ZU: "+180"
    3O: "-130"
    6O: "-20"
    9O: "+30"
    12O: "+10"
labor: |
  Das vorliegende Werk kam ungereinigt und mit vermutlich durch einen Stoß beschädigter Unruh in die Werkstatt und wurde dort grundgereinigt und geölt. Die stark taumelnde Unruh wurde durch Einschrauben des zifferblattseitigen Lagersteins wieder in Lage gebracht, und erstaunlicherweise zeigte sich auch eine hohe Amplitude. Dennoch zeigten die Gangergebnisse auf der Zeitwaage ein recht uneinheitliches Bild, von starkem Vorgang bis zu nicht minder starkem Vorgang war alles vorhanden, im Mittel aber, also am Handgelenk, dürfte diese Uhr dennoch mit -+ einer Minute pro Tag laufen. Mehr wurde seinerzeit auch nicht versprochen, insofern ist hier "alles im Lot".
links: |
  * [1] [Watch-Wiki: UMF 24-42](http://watch-wiki.org/index.php?title=UMF_24-42)
  * [2] [Watch-Wiki: Kaliberfamilie UMF M24](http://watch-wiki.org/index.php?title=Werkfamilie_M_24)
  * [3] [Ranfft Uhren: UMF 24-42](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&a153&2uswk&UMF_24_42)
---
Lorem Ipsum