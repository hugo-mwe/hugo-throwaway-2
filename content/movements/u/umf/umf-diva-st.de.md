---
title: "UMF Diva ST (M3)"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["UMF M3","UMF M3","Diva ST","Diva","M3","UMF Ruhla","Ruhla","DDR","GDR","15","Steine","Jewels","Handaufzug","Damenuhr","Schraubenunruh"]
description: "UMF Diva ST (M3)"
abstract: ""
preview_image: "umf_diva-st_mini.jpg"
image: "UMF_3_stossgesichert.jpg"
movementlistkey: "umf"
caliberkey: "Diva ST"
manufacturers: ["umf"]
manufacturers_weight: 32
categories: ["movements","movements_u","movements_u_umf"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "u/umf/UMF_Ruhla_DAU_15_Rubis.jpg"
    description: "UMF Ruhla Damenuhr"
---
Lorem Ipsum