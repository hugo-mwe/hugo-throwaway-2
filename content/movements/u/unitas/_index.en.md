---
title: "Unitas"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "en"
keywords: ["Unitas","Tramelan","Switzerland","Swiss"]
categories: ["movements","movements_u"]
movementlistkey: "unitas"
abstract: "Movements of the swiss manufacturer Unitas from Tramelan."
description: "Movements of the swiss manufacturer Unitas from Tramelan."
---
(Fabrique d'Ebauches Unitas S.A., Tramelan, Switzerland)
{{< movementlist "unitas" >}}

{{< movementgallery "unitas" >}}