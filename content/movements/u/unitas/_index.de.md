---
title: "Unitas"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "de"
keywords: ["Unitas","Tramelan","Schweiz"]
categories: ["movements","movements_u"]
movementlistkey: "unitas"
abstract: "Uhrwerke des schweizer Herstellers Unitas aus Tramelan."
description: "Uhrwerke des schweizer Herstellers Unitas aus Tramelan."
---
(Fabrique d'Ebauches Unitas S.A., Tramelan, Schweiz)
{{< movementlist "unitas" >}}

{{< movementgallery "unitas" >}}