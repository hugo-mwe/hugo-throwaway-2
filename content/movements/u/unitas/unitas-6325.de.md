---
title: "Unitas 6325"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Unitas 6325","Unitas","6325","21 Jewels","21 Steine","Handaufzug","Schweiz"]
description: "Unitas 6325"
abstract: ""
preview_image: "unitas_6325-mini.jpg"
image: "Unitas_6325.jpg"
movementlistkey: "unitas"
caliberkey: "6325"
manufacturers: ["unitas"]
manufacturers_weight: 63250
categories: ["movements","movements_u","movements_u_unitas"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Unitas 6325](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Unitas_6325)
usagegallery: 
  - image: "o/omikron/Omikron.jpg"
    description: "Omikron Herrenuhr"
  - image: "a/ankra/Ankra_74.jpg"
    description: "Ankra 74 Herrenuhr"
---
Lorem Ipsum