---
title: "Unitas 6360"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Unitas 6360","Unitas","6360","17 Jewels","17 Steine","Schweiz"]
description: "Unitas 6360"
abstract: ""
preview_image: "unitas_6360-mini.jpg"
image: "Unitas_6360.jpg"
movementlistkey: "unitas"
caliberkey: "6360"
manufacturers: ["unitas"]
manufacturers_weight: 63600
categories: ["movements","movements_u","movements_u_unitas"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/ornata/Ornata_DAU.jpg"
    description: "Ornata Damenuhr"
---
Lorem Ipsum