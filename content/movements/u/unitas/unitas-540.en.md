---
title: "Unitas 540"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Unitas 540","Unitas","540","17 Jewels","form movement","caliber","switzerland"]
description: "Unitas 540"
abstract: ""
preview_image: "unitas_540-mini.jpg"
image: "Unitas_540.jpg"
movementlistkey: "unitas"
caliberkey: "540"
manufacturers: ["unitas"]
manufacturers_weight: 5400
categories: ["movements","movements_u","movements_u_unitas_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bancor/Bancor_DAU.jpg"
    description: "Bancor ladies' watch"
---
Lorem Ipsum