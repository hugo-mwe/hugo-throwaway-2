---
title: "Urofa 522"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Urofa 522","Urofa","522","Dugena 659","Dugena","659","Deutsche Uhrenrohwerke Fabrik","15 Jewels","germany"]
description: "Urofa 522"
abstract: ""
preview_image: "urofa_522-mini.jpg"
image: "Urofa_522.jpg"
movementlistkey: "urofa"
caliberkey: "522"
manufacturers: ["urofa"]
manufacturers_weight: 522
categories: ["movements","movements_u","movements_u_urofa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_Anonym.jpg"
    description: "Dugena ladies' watch"
---
Lorem Ipsum