---
title: "China R16-1"
date: 2016-02-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["China R16-1","Automatic","Gyrotron","Großdatum","kleine Sekunde","kleine Minute","Tag/Nacht-Anzeige","24h-Anzeige"]
description: "China R16-1 - Ein Automaticwerk mit Großdatum und diversen unnützen Anzeigen"
abstract: "Ein chinesisches Automaticwerk mit Großdatum und einer Menge unnützer Anzeigen auf Hilfszifferblättern"
preview_image: "china_r16-1-mini.jpg"
image: "China_R16-1.jpg"
movementlistkey: "china"
caliberkey: "R16-1"
manufacturers: ["china"]
manufacturers_weight: 161
categories: ["movements","movements_c","movements_c_china"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Uhr wurde von [Günter G](/supporters/guenter-g/). gespendet. Vielen Dank für die Unterstützung des Uhrwerkearchivs!"
usagegallery: 
  - image: "anonym/Fake_HAU_China_R16-1.jpg"
    description: "Fälschung aus China"
labor: |
  <p>Das vorliegende Exemplar wurde einige Zeit getragen (erkennbar an einer völlig angelaufenen Krone), mußte wegen schlechter Gangeigenschaften und Kontaminierung mit Metallspänen gereinigt werden. Die Gangwerte sind dennoch eher bescheiden. Die Datumsanzeige funktioniert nur sporadisch, aufgrund der schon oben aufgelisteten Verarbeitungsprobleme.</p><p>Einer Regulierung entzog sich dieses Werk, ebenfalls aufgrund der schlechten Verarbeitung: Nach einer recht passablen, aber langwierigen Justierung geriet das Werk, verursacht durch einen kleinen Stoß, wieder soweit außer Takt, daß eine komplette Neujustierung notwendig gewesen wäre. Aufgrund der Fragilität der Justierung wurde darauf verzichtet</p>
---
Lorem Ipsum