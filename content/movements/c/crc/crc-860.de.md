---
title: "CRC 860"
date: 2014-12-07T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["CRC 860","France","pin lever","17 jewels"]
description: "CRC 860 - ein französisches Stiftankerwerk mit 17 Steinen, darunter zwei Blindsteine"
abstract: " "
preview_image: "crc_860-mini.jpg"
image: "CRC_860.jpg"
movementlistkey: "crc"
caliberkey: "860"
manufacturers: ["crc"]
manufacturers_weight: 860
categories: ["movements","movements_c","movements_c_crc"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum