---
title: "Chaika 1601A"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Chaika 1601A","Chaika","1601A","1601","Sekonda","17 Jewels","watch","watches","wristwatch","wristwatches","ladies","movement","caliber","17","jewels","USSR","Russia","Sowjet Union"]
description: "Chaika 1601A"
abstract: ""
preview_image: "chaika_1601a-mini.jpg"
image: "Chaika_1601A.jpg"
movementlistkey: "chaika"
caliberkey: "1601A"
manufacturers: ["chaika"]
manufacturers_weight: 1601
categories: ["movements","movements_c","movements_c_chaika_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/chaika/Chaika_DAU.jpg"
    description: "Chaika ladies' watch"
  - image: "m/meister-anker/Meister_Anker_17_Steine.jpg"
    description: "Meister Anker ladies' watch"
  - image: "s/sekonda/Sekonda_DAU.jpg"
    description: "Sekonda ladies' watch"
---
Lorem Ipsum