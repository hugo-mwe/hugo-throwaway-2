---
title: "Chaika 1601A"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Chaika 1601A","Chaika","1601A","1601","Sekonda","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Damenuhr","Kaliber","Werk","17","Steine","Jewels","Rußland","UdSSR","Sowjetunion"]
description: "Chaika 1601A"
abstract: ""
preview_image: "chaika_1601a-mini.jpg"
image: "Chaika_1601A.jpg"
movementlistkey: "chaika"
caliberkey: "1601A"
manufacturers: ["chaika"]
manufacturers_weight: 1601
categories: ["movements","movements_c","movements_c_chaika"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/chaika/Chaika_DAU.jpg"
    description: "Chaika Damenuhr"
  - image: "m/meister-anker/Meister_Anker_17_Steine.jpg"
    description: "Meister Anker Damenuhr"
  - image: "s/sekonda/Sekonda_DAU.jpg"
    description: "Sekonda Damenuhr"
---
Lorem Ipsum