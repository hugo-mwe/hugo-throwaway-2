---
title: "Consul"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: []
categories: ["movements","movements_c"]
movementlistkey: "consul"
description: ""
abstract: "(Switzerland)"
---
(Switzerland)
{{< movementlist "consul" >}}

{{< movementgallery "consul" >}}