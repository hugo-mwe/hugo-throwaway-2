---
title: "Citizen 5330A"
date: 2009-05-21T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 5330A","5330","Citizen","Miyota","5330A","17 jewels","17","Steine","Jewels","Handaufzug","Damenuhr"]
description: "Citizen 5330A"
abstract: ""
preview_image: "citizen_5330a-mini.jpg"
image: "Citizen_5330A.jpg"
movementlistkey: "citizen"
caliberkey: "5330A"
manufacturers: ["citizen"]
manufacturers_weight: 5330
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_HA_4.jpg"
    description: "Citizen Damenuhr"
---
Lorem Ipsum