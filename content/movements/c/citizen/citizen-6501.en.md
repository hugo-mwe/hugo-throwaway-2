---
title: "Citizen 6501"
date: 2009-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 6501","Citizen","6501","6000","21 Jewels","Japan","Automatic","selfwinding"]
description: "Citizen 6501"
abstract: ""
preview_image: "citizen_6501-mini.jpg"
image: "Citizen_6501.jpg"
movementlistkey: "citizen"
caliberkey: "6501"
manufacturers: ["citizen"]
manufacturers_weight: 6501
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Citizen 6501](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Citizen_6501&)
usagegallery: 
  - image: "c/citizen/Citizen_6501_652786.jpg"
    description: "Citizen Automatic gents watch"
---
Lorem Ipsum