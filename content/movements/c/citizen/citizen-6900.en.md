---
title: "Citizen 6900"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 6900","Citizen","6900","21","Rubis","Automatic","selfwinding","Jewels","28800"]
description: "Citizen 6900"
abstract: ""
preview_image: "citizen_6900-mini.jpg"
image: "Citizen_6900.jpg"
movementlistkey: "citizen"
caliberkey: "6900"
manufacturers: ["citizen"]
manufacturers_weight: 6900
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_Automatic_1.jpg"
    description: "Citizen Automatic ladies' watch"
  - image: "c/citizen/Citizen_DAU_Automatic_2.jpg"
    description: "Citizen Automatic ladies' watch"
  - image: "c/citizen/Citizen_Cosmostarv2.jpg"
    description: "Citizen Cosmostar ladies' watch"
---
Lorem Ipsum