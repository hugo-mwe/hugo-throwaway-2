---
title: "Citizen 7290"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 7290","Citizen","7290","Automatic","23","jewels","selfwinding","hacking","hack"]
description: "Citizen 7290"
abstract: ""
preview_image: "citizen_7290-mini.jpg"
image: "Citizen_7290.jpg"
movementlistkey: "citizen"
caliberkey: "7290"
manufacturers: ["citizen"]
manufacturers_weight: 7290
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum