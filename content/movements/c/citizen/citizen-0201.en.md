---
title: "Citizen 0201"
date: 2009-04-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 0201","Citizen","0201","17 Jewels","Japan","watch","watches","wristwatch","wristwatches","caliber","handwind","handwound","HMT"]
description: "Citizen 0201"
abstract: ""
preview_image: "citizen_0201-mini.jpg"
image: "Citizen_0201.jpg"
movementlistkey: "citizen"
caliberkey: "0201"
manufacturers: ["citizen"]
manufacturers_weight: 201
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_HAU.jpg"
    description: "Citizen mens' watch"
---
Lorem Ipsum