---
title: "Citizen 7806A"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 7806A","Citizen","7806A","8 Jewels","Cosmotron","Electric","Electronic","Electromagnetic","8 Steine","Japan","elektrisch","elektromagnetisch"]
description: "Citizen 7806A"
abstract: ""
preview_image: "citizen_7806a-mini.jpg"
image: "Citizen_7806A.jpg"
movementlistkey: "citizen"
caliberkey: "7806A"
manufacturers: ["citizen"]
manufacturers_weight: 7806
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_Cosmotron_HAU_2.jpg"
    description: "Citizen Cosmotron Herrenuhr"
---
Lorem Ipsum