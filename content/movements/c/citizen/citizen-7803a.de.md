---
title: "Citizen 7803A"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 7803A","Citizen","7803A","8 Jewels","Cosmotron","Electric","Electronic","Electromagnetic","8 Steine","Japan","elektrisch","elektromagnetisch"]
description: "Citizen 7803A"
abstract: ""
preview_image: "citizen_7803a-mini.jpg"
image: "Citizen_7803A.jpg"
movementlistkey: "citizen"
caliberkey: "7803A"
manufacturers: ["citizen"]
manufacturers_weight: 7803
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_Cosmotron_HAU.jpg"
    description: "Citizen Cosmotron Herrenuhr"
---
Lorem Ipsum