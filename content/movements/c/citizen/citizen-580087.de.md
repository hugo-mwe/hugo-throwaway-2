---
title: "Citizen 580087"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 580087","Citizen","580087","Cosmotron","electric","Elektrik","elektromechanisch","Kaliber","Werk","17","Steine","Jewels","28800","Batterie","Knopfzelle"]
description: "Citizen 580087"
abstract: ""
preview_image: "citizen_580087-mini.jpg"
image: "Citizen_580087.jpg"
movementlistkey: "citizen"
caliberkey: "580087"
manufacturers: ["citizen"]
manufacturers_weight: 580087
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum