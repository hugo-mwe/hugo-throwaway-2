---
title: "Citizen 8203A"
date: 2014-10-02T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 8203A","Miyota 8205","Citizen","Miyota","Automatic","25 Jewels","Tag","Datum"]
description: "Citizen 8203A - der Urahn des Miyota 8205, eines der inzwischen am häufigsten verbauten japanischen Automaticwerke mit Vollausstattung."
abstract: "Der Urahn des heute mit am häufigsten verbauten japananischen Automaticwerks, dem Miyota 8205."
preview_image: "citizen_8203a-mini.jpg"
image: "Citizen_8203A.jpg"
movementlistkey: "citizen"
caliberkey: "8203A"
manufacturers: ["citizen"]
manufacturers_weight: 8203
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>Bei dem vorliegenden Werk mußte sowohl die zu niedrige Reibung des Minutenrohres korrigiert, als auch das Werk selber gereinigt werden. Leider verhindert eine offenbar hartnäckig verschmutzte Unruhspirale, daß sich ein stabiler und gleichmäßiger Gang einstellt, so daß dieses Werk, respektive die Unruhpartie, nochmal irgendwann nachhaltig gereinigt werden muß.</p><p>Auf der Zeitwaage ist im aktuellen Zustand kein sinnvolles Gangbild zu ermitteln, andere, vergleichbare Exemplare liefern aber sehr gute Werte mit leichten Lageschwankungen ab.</p>
usagegallery: 
  - image: "c/citizen/Citizen_HAU_Citizen_8203A.jpg"
    description: "Citizen Herrenuhr Modell GN-4W-S"
---
Lorem Ipsum