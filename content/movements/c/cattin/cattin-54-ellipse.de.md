---
title: "Cattin 54 (Ellipse)"
date: 2016-12-27T15:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Cattin 54","Cattin","Morteau","Frankreich","Stiftanker","Zentralsekunde"]
description: "Cattin 54 (Ellipse) - Ein sehr seltenes, französisches Stiftankerwerk"
abstract: ""
preview_image: "cattin_54-mini.jpg"
image: "Cattin_54.jpg"
movementlistkey: "cattin"
caliberkey: "54 (Ellipse)"
manufacturers: ["cattin"]
manufacturers_weight: 540
categories: ["movements","movements_c","movements_c_cattin"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "m/mortima/Mortima_HU_Cattin_54.jpg"
    description: "Mortima Herrenuhr"
---
Lorem Ipsum