---
title: "Certina"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: []
categories: ["movements","movements_c"]
movementlistkey: "certina"
description: ""
abstract: "(Kurth Frères S.A., Grenchen, Switzerland)"
---
(Kurth Frères S.A., Grenchen, Switzerland)
{{< movementlist "certina" >}}

{{< movementgallery "certina" >}}