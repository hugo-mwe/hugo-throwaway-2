---
title: "Certina 251"
date: 2015-12-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Certina 251","K.F. 251","Kurth Freres 251","Grana 251","manual wind","15 Jewels","15 Rubis","form movement","decentral second","screw balance","Grenchen","Switzerland","Swiss"]
description: "Certina 251 / K.F. 251 / Grana 251"
abstract: "An early form movement from Certina with decentral seconds indication and screw balance."
preview_image: "certina_251-mini.jpg"
image: "Certina_251.jpg"
movementlistkey: "certina"
caliberkey: "251"
manufacturers: ["certina"]
manufacturers_weight: 251
categories: ["movements","movements_c","movements_c_certina_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement and watch was donated by [Erwin](/supporters/erwin/). Thank you very much!"
timegrapher_old: 
  rates:
    ZO: "+20"
    ZU: "+34"
    3O: "+7"
    6O: "-16"
    9O: "-41"
    12O: "-15"
  amplitudes:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
usagegallery: 
  - image: "c/certina/Certina_HAU_Certina_251.jpg"
    description: "Certina gents watch"
labor: |
  The specimen shown here came resinified into the lab and for the first time, it wasn't cleaned in fluids, but with cleaning wood sticks. The result was quite ok.
---
Lorem Ipsum