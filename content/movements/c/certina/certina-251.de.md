---
title: "Certina 251"
date: 2015-12-27T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Certina 251","K.F. 251","Kurth Freres 251","Grana 251","Handaufzug","15 Jewels","15 Rubis","15 Steine","Formwerk","kleine Sekunde","Schraubenunruh","Grenchen","Schweiz","Swiss"]
description: "Certina 251 / K.F. 251 / Grana 251"
abstract: "Ein frühes Formwerk von Certina mit dezentraler Sekundenanzeige und Schraubenunruh."
preview_image: "certina_251-mini.jpg"
image: "Certina_251.jpg"
movementlistkey: "certina"
caliberkey: "251"
manufacturers: ["certina"]
manufacturers_weight: 251
categories: ["movements","movements_c","movements_c_certina"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Werk kam verharzt ins Labor und wurde erstmals nicht in Reinigungsbädern vom Harz befreit, sondern mit Putzhölzern. Das Ergebnis ist ziemlich zufriedenstellend.
usagegallery: 
  - image: "c/certina/Certina_HAU_Certina_251.jpg"
    description: "Certina Herrenuhr"
timegrapher_old: 
  rates:
    ZO: "+20"
    ZU: "+34"
    3O: "+7"
    6O: "-16"
    9O: "-41"
    12O: "-15"
  amplitudes:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
donation: "Dieses Werk samt Uhr ist eine Spende von [Erwin](/supporters/erwin/). Ganz herzlichen Dank dafür!"
---
Lorem Ipsum