---
title: "Certina 13-21"
date: 2013-12-27T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Certina 13-21","Certina","Grana","Kurth Freres","Handaufzug","17 Jewels","Feinregelage","Exzenter"]
description: "Certina 13-21 - ein sehr kleines, hochwertiges Damenuhrenwerk mit Feinregelage"
abstract: "Ein sehr kleines und sehr hochwertiges schweizer Damenuhrenwerk mit Exzenter-Feinregelage"
preview_image: "certina_13-21-mini.jpg"
image: "Certina_13-21.jpg"
movementlistkey: "certina"
caliberkey: "13-21"
manufacturers: ["certina"]
manufacturers_weight: 1321
categories: ["movements","movements_c","movements_c_certina"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk befand sich in sehr gutem Zustand, so daß auf einen Service verzichtet werden konnte. Die Gangwerte zeigen somit den Originalzustand dieses Werks, das beim Versand, eingewickelt in Zeitungspapier, nicht geschont wurde. Die Bilder vom Räderwerk wurden von einem zweiten, defekten, Werk angefertigt.
timegrapher_old: 
  description: |
    Für ein Werk im gebrauchten Originalzustand, noch dazu in dieser winzigen Größe sind die Gangwerte sehr ordentlich! Certina stand seinerzeit nicht umsonst für gesicherte Qualität. In einigen Lagen ist zwar ein stärkerer Abfallfehler meßbar, in der Praxis wirkt er sich hier aber nicht aus. Lediglich in der Position "Krone oben" scheint es so, als wäre die Unruh nicht optimal ausgewuchtet, vielleicht war aber auch nur ein Staubkörnchen im Spiel, wie gesagt, das Werk ist ungereinigt.  
  images:
    ZO: Zeitwaage_Certina_13-21_ZO.jpg
    ZU: Zeitwaage_Certina_13-21_ZU.jpg
    3O: Zeitwaage_Certina_13-21_3O.jpg
    6O: Zeitwaage_Certina_13-21_6O.jpg
    9O: Zeitwaage_Certina_13-21_9O.jpg
    12O: Zeitwaage_Certina_13-21_12O.jpg
  values:
    ZO: "+15"
    ZU: "+-0"
    3O: "-25"
    6O: "+-0"
    9O: "+10"
    12O: "+7"
usagegallery: 
  - image: "c/certina/Certina_DAU_0806_433_Certina_13-21.jpg"
    description: "Certina Damenuhr Modell 0806 433"
  - image: "c/certina/Certina_DAU_Zifferblatt_Certina_13-21.jpg"
    description: "Certina Damenuhr (nur Zifferblatt)"
---
Lorem Ipsum