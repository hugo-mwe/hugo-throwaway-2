---
title: "Certina 230"
date: 2010-05-16T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Grana 230","K.F.230","screw balance","Incabloc","yoke winding system","Certina 230","Certina","230","Grana 230","K.F. 230","Kurth Freres","17 Jewels","Grenchen","swiss","switzerland"]
description: "Certina 230 - A high quality 5 1/4''' form movement with 17 jewels. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "certina_230-mini.jpg"
image: "Certina_230.jpg"
movementlistkey: "certina"
caliberkey: "230"
manufacturers: ["certina"]
manufacturers_weight: 230
categories: ["movements","movements_c","movements_c_certina_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    On the timegrapher, which was set into double precision mode, it showed a good performance, especially, when you take into account, that this movement is about 60 years old. It shows a very low bear error (indicatable by very small lines on the printout). The assemblers at the Certina factory did an outstanding job there! The deviations are really OK, the two big numbers at "3 up" and "9 up" will neutralize themselves on wearing the watch. For a movement of that tiny size, the timegrapher results are really good and prove the high quality of the Certina 230.
  images:
    ZO: Zeitwaage_Certina_230_ZO.jpg
    ZU: Zeitwaage_Certina_230_ZU.jpg
    3O: Zeitwaage_Certina_230_3O.jpg
    6O: Zeitwaage_Certina_230_6O.jpg
    9O: Zeitwaage_Certina_230_9O.jpg
    12O: Zeitwaage_Certina_230_12O.jpg
  values:
    ZO: "-8"
    ZU: "+13"
    3O: "-20"
    6O: "+1"
    9O: "+30"
    12O: "+5"
usagegallery: 
  - image: "c/certina/Certina_DAU_Certina_230.jpg"
    description: "Certina ladies' watch"
labor: |
  The movement came in fair condition into the laboratory, but since the watch built around it was in a bad condition (crystal missing, dial dirty), it got a full service nevertheless.
---
Lorem Ipsum