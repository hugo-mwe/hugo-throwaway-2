---
title: "Cortébert 665"
date: 2015-09-26T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Cortebert 665","Cortebert","17 Jewels","17 Steine","Palettenanker","Chatons","dezentrale Sekunde"]
description: "Cortebert 665 - ein hochwertiges schweizer Ankerwerk mit 17 Steinen, teilweise in gesteckten Chatons"
abstract: "Ein sehr ansprechendes schweizer Werk mit 17 Steinen, die teilweise in gesteckten Chatons gelagert sind."
preview_image: "cortebert_665-mini.jpg"
image: "Cortebert_665.jpg"
movementlistkey: "cortebert"
caliberkey: "665"
manufacturers: ["cortebert"]
manufacturers_weight: 665
categories: ["movements","movements_c","movements_c_cortebert"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "c/cortebert/Cortebert_HAU_Cortebert_665.jpg"
    description: "Cortebert Herrenuhr (Exportversion)"
labor: |
  <p>Das hier vorgestellte Werk kam in einem Gehäuse mit fehlendem Glas, dessen Boden nicht mehr hielt. Es war zusammen mit dem Zifferblatt eingeklebt(!), der Kleber ḱonnte nur mit Mühe vom Werk entfernt werden, leider nicht restlos. So bleibt der Rücker weiterhin verklebt und auch die Unruhspirale ist durch Kleberreste beschädigt.</p><p>Da das Werk zudem noch verharzt war, wurde es gereinigt und geölt, so daß es jetzt zumindest wieder kleine Lebenszeichen von sich gibt. Mit +190 Sekunden pro Tag in der Lage "Zifferblatt unten" läuft es in Anbetracht des Kleber-Schadens noch überraschend gut. Hier machen sich dann doch die guten Lagersteine bemerkbar.</p>
---
Lorem Ipsum