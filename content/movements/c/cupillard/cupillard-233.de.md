---
title: "Cupillard 233"
date: 2009-04-18T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Cupillard 233","Cupillard","233","Ebauches","Villers-le-lac","France","15 Jewels","Frankreich","15 Steine"]
description: "Cupillard 233"
abstract: ""
preview_image: "cupillard_233-mini.jpg"
image: "Cupillard_233.jpg"
movementlistkey: "cupillard"
caliberkey: "233"
manufacturers: ["cupillard"]
manufacturers_weight: 233
categories: ["movements","movements_c","movements_c_cupillard"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Anonym_Cupillard_233.jpg"
    description: "anonyme Herrenuhr"
  - image: "c/clyda/Clyda_HAU.jpg"
    description: "Clyda Herrenuhr"
  - image: "a/ancre/Ancre_HAU.jpg"
    description: "Ancre Herrenuhr"
links: |
  * [Ranfft Uhren: Cupillard 233](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Cupillard_233)
---
Lorem Ipsum