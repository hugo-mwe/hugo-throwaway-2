---
title: "Cupillard 55-A 21"
date: 2009-10-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Cupillard 55-A 21","Cupillard","55-A 21","Ebauches","Villers-le-lac","France","17 Jewels","France","french","form movement"]
description: "Cupillard 55-A 21"
abstract: ""
preview_image: "cupillard_55-a_21-mini.jpg"
image: "Cupillard_55-A_21.jpg"
movementlistkey: "cupillard"
caliberkey: "55-A 21"
manufacturers: ["cupillard"]
manufacturers_weight: 5521
categories: ["movements","movements_c","movements_c_cupillard_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/ka/KA_Zifferblatt.jpg"
    description: "dial of a KA ladies' watch"
donation: "This movement was donated by [Harald Hoeber](/supporters/en/). Thank you very much!"
---
Lorem Ipsum