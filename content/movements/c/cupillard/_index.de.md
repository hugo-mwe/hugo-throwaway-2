---
title: "Cupillard"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: ["Cupillard","Frankreich","Villers-le-lac"]
categories: ["movements","movements_c"]
movementlistkey: "cupillard"
abstract: "Uhrwerke des französischen Rohwerkeherstellers Cupillard"
description: "Uhrwerke des französischen Rohwerkeherstellers Cupillard"
---
(Ebauches Cupillard, Villers-le-lac, Frankreich)
{{< movementlist "cupillard" >}}

{{< movementgallery "cupillard" >}}