---
title: "Cupillard"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: ["Cupillard","France","Villers-le-lac"]
categories: ["movements","movements_c"]
movementlistkey: "cupillard"
abstract: "movements of the french manufacturer Cupillard"
description: "movements of the french manufacturer Cupillard"
---
(Ebauches Cupillard, Villers-le-lac, France)
{{< movementlist "cupillard" >}}

{{< movementgallery "cupillard" >}}