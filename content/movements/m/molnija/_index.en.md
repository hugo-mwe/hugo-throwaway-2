---
title: "Molnija"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "en"
keywords: ["Molnija","Tscheljabinsk","Russia","USSR"]
categories: ["movements","movements_m"]
movementlistkey: "molnija"
abstract: "Movements of the former manufacturer Molnija of Tscheljabinsk, Russia."
description: "Movements of the former manufacturer Molnija of Tscheljabinsk, Russia."
---
(Molnija, Tscheljabinsk, Russia)
{{< movementlist "molnija" >}}

{{< movementgallery "molnija" >}}