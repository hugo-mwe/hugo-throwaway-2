---
title: "Molnija 3602"
date: 2016-01-30T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Molnija 3602","Molnija","Molnia","3602","Taschenuhr","18 Jewels","18 Rubis","dezentrale Sekunde","kleine Sekunde"]
description: "Molnija 3602 - Ein sehr beliebtes, fast 50 Jahre lang produziertes Taschenuhrwerk aus Russland"
abstract: "Das wohl am häufigsten gebaute Taschenuhrenwerk aus Russland, hier in seiner letzten Ausführung"
preview_image: "molnija_3602-mini.jpg"
image: "Molnija_3602.jpg"
movementlistkey: "molnija"
caliberkey: "3602"
manufacturers: ["molnija"]
manufacturers_weight: 3602
categories: ["movements","movements_m","movements_m_molnija"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Ein großes Werk, eine schwere Schraubenunruh und keine Stoßsicherung - das vorliegende Exemplar kam aus nachvollziehbaren Gründen mit einer gebrochenen Unruhwelle ins Labor und wurde daher keinem Service und auch keinem Ganggenauigkeitstest unterzogen.
donation: "Dieses Werk samt Taschenuhr ist eine Spende von [Erwin](/supporters/erwin/) an das Uhrenarchiv. Ganz herzlichen Dank dafür!"
usagegallery: 
  - image: "c/corsar/Corsar_TU_Molnija_3602.jpg"
    description: "Corsar Taschenuhr"
---
Lorem Ipsum