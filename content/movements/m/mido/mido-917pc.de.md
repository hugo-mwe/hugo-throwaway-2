---
title: "Mido 917PC"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Mido 917PC","Mido 917","Mido","AS","Powerwind","17 Jewels","Incastar","Schweiz"]
description: "Mido 917PC"
abstract: ""
preview_image: "mido_917pc-mini.jpg"
image: "Mido_917PC.jpg"
movementlistkey: "mido"
caliberkey: "917PC"
manufacturers: ["mido"]
manufacturers_weight: 9172
categories: ["movements","movements_m","movements_m_mido"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mido/Mido_Multifort_Powerwind.jpg"
    description: "Mido Multifort Superautomatic Powerwind Herrenuhr"
links: |
  * [Ranfft Uhrwerkearchiv: Mido 917PC](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&uswk&Mido_917PC) (Informationen über das Werk und ausführliche Beschreibung des Incastar-Systems)
---
Lorem Ipsum